SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

    ALTER TABLE users     CHANGE id      id      INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;
    ALTER TABLE addresses CHANGE user_id user_id INT(11) UNSIGNED NOT NULL;

    DELETE FROM addresses WHERE country_id = 0;
    ALTER TABLE addresses ADD CONSTRAINT FOREIGN KEY(country_id) REFERENCES countries(id);
    DELETE FROM addresses WHERE user_id  NOT IN  (SELECT id FROM users);
    ALTER TABLE addresses ADD CONSTRAINT FOREIGN KEY(user_id)    REFERENCES users(id);
    ALTER TABLE states ADD CONSTRAINT FOREIGN KEY(country_id) REFERENCES countries(id);

    /**
     * DB structure for widgets
     */

    DROP TABLE IF EXISTS widget_masters;
    CREATE TABLE widget_masters (
        id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
        , name        varchar(128) NOT NULL DEFAULT ''
        , description varchar(256) NOT NULL DEFAULT ''
        , height_min tinyint UNSIGNED NOT NULL DEFAULT 1
        , height_max tinyint UNSIGNED NOT NULL DEFAULT 3
        , height_def tinyint UNSIGNED NOT NULL DEFAULT 1
        , width_min  tinyint UNSIGNED NOT NULL DEFAULT 1
        , width_max  tinyint UNSIGNED NOT NULL DEFAULT 4
        , width_def  tinyint UNSIGNED NOT NULL DEFAULT 1
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    INSERT INTO widget_masters VALUES                             -- hmin, hmax, hdef, wmin, wmax, wdef
      (null, 'weather',    'Show the weather in your local area.',      1,    2,    1,    1,    2,   1)
    , (null, 'editor',     'Rich text editor for your custom content.', 1,    6,    1,    1,    4,   1)
    , (null, 'logo',       'Company''s logo.',                          1,    2,    1,    1,    4,   1)
    , (null, 'news',       'Reuters News.',                             1,    6,    1,    1,    4,   1)
    , (null, 'slideshow',  'Pictures slideshow.',                       2,    6,    2,    1,    4,   1)
    , (null, 'team',       'Team members.',                             2,    2,    2,    2,    2,   2)
    , (null, 'event',      'Events.',                                   2,    2,    2,    2,    2,   2)
    , (null, 'promo',      'Promotions & sales.',                       2,    2,    2,    2,    2,   2)
    , (null, 'clock',      'Local time clock.',                         1,    1,    1,    1,    4,   1)
    , (null, 'ticker',     'Scroll content ticker.',                    1,    1,    1,    4,    4,   4)
    , (null, 'restaurant', 'Restaurant category widget.',               1,    6,    1,    1,    4,   1)
    ;

    DROP TABLE IF EXISTS widget_instances;
    CREATE TABLE widget_instances (
        id         INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
        , name     varchar(128) NOT NULL DEFAULT ''
        , user_id  INT UNSIGNED NOT NULL
        , FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
        , widget_master_id  INT UNSIGNED NOT NULL
        , FOREIGN KEY (widget_master_id) REFERENCES widget_masters(id) ON DELETE CASCADE ON UPDATE CASCADE
        , col        tinyint UNSIGNED NOT NULL DEFAULT 0
        , row        tinyint UNSIGNED NOT NULL DEFAULT 0
        , size_x     tinyint UNSIGNED NOT NULL DEFAULT 1
        , size_y     tinyint UNSIGNED NOT NULL DEFAULT 1
        , show_title bool             NOT NULL DEFAULT false
        , opacity    tinyint UNSIGNED NOT NULL DEFAULT 30
        , created    datetime         NOT NULL
        , updated    datetime         NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    DROP TABLE IF EXISTS widget_instance_editors;
    CREATE TABLE widget_instance_editors (
        id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
        , widget_instance_id  INT UNSIGNED NOT NULL
        , FOREIGN KEY (widget_instance_id) REFERENCES widget_instances(id) ON DELETE CASCADE ON UPDATE CASCADE
        , content text NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    DROP TABLE IF EXISTS widget_instance_weathers;
    CREATE TABLE widget_instance_weathers (
        id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
        , widget_instance_id  INT UNSIGNED NOT NULL
        , FOREIGN KEY (widget_instance_id) REFERENCES widget_instances(id) ON DELETE CASCADE ON UPDATE CASCADE
        , location varchar(128) NOT NULL
        , geocode  varchar(128) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    DROP TABLE IF EXISTS widget_instance_clocks;
    CREATE TABLE widget_instance_clocks (
        id                   INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
        , widget_instance_id INT UNSIGNED NOT NULL
        , FOREIGN KEY (widget_instance_id) REFERENCES widget_instances(id) ON DELETE CASCADE ON UPDATE CASCADE
        , datetime_format    varchar(128) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    -- / Restaurant module

        DROP TABLE IF EXISTS restaurant_periods;
        CREATE TABLE restaurant_periods (
            id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
            , user_id     INT UNSIGNED NOT NULL
            , FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
            , name        varchar(128) NOT NULL DEFAULT ''
            , starts      tinyint UNSIGNED NOT NULL
            , ends        tinyint UNSIGNED NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        DROP TABLE IF EXISTS restaurant_categories;
        CREATE TABLE restaurant_categories (
            id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
            , user_id     INT UNSIGNED NOT NULL
            , FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
            , name        varchar(128) NOT NULL DEFAULT ''
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        DROP TABLE IF EXISTS restaurant_category_period;
        CREATE TABLE restaurant_category_period (
            id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
            ,  restaurant_category_id     INT UNSIGNED NOT NULL
            , FOREIGN KEY (restaurant_category_id) REFERENCES restaurant_categories(id) ON DELETE CASCADE ON UPDATE CASCADE
            , restaurant_period_id     INT UNSIGNED NOT NULL
            , FOREIGN KEY (restaurant_period_id) REFERENCES restaurant_periods(id) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        DROP TABLE IF EXISTS restaurant_menu_items;
        CREATE TABLE restaurant_menu_items (
            id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
            , restaurant_category_id     INT UNSIGNED NOT NULL
            , FOREIGN KEY (restaurant_category_id) REFERENCES restaurant_categories(id) ON DELETE CASCADE ON UPDATE CASCADE
            , name        varchar(128) NOT NULL DEFAULT ''
            , description varchar(256) NOT NULL DEFAULT ''
            , picture     varchar(128) DEFAULT NULL
            , sort_order  tinyint UNSIGNED NOT NULL DEFAULT 0
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        DROP TABLE IF EXISTS restaurant_menu_item_prices;
        CREATE TABLE         restaurant_menu_item_prices (
            id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
            , restaurant_menu_item_id     INT UNSIGNED NOT NULL
            , FOREIGN KEY (restaurant_menu_item_id) REFERENCES restaurant_menu_items(id) ON DELETE CASCADE ON UPDATE CASCADE
            , name        varchar(128) NOT NULL DEFAULT 'default'
            , price       DECIMAL(8,2) UNSIGNED NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        DROP TABLE IF EXISTS widget_instance_restaurants;
        CREATE TABLE         widget_instance_restaurants (
            id                       INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
            , widget_instance_id INT UNSIGNED NOT NULL
            , FOREIGN KEY (widget_instance_id) REFERENCES widget_instances(id) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


        DROP TABLE IF EXISTS widget_instance_restaurant_categories;
        CREATE TABLE         widget_instance_restaurant_categories (
            id                       INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
            , widget_instance_restaurant_id INT UNSIGNED NOT NULL
            , FOREIGN KEY (widget_instance_restaurant_id) REFERENCES widget_instance_restaurants(id) ON DELETE CASCADE ON UPDATE CASCADE
            , restaurant_category_id INT UNSIGNED NOT NULL
            , FOREIGN KEY (restaurant_category_id) REFERENCES restaurant_categories(id) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


    -- Restaurant module /

    DROP TABLE IF EXISTS background_videos;
    CREATE TABLE background_videos (
        id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
        , name        varchar(128) NOT NULL DEFAULT ''
        , screenshot  varchar(128) NOT NULL DEFAULT ''
        , video       varchar(128) NOT NULL DEFAULT ''
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    INSERT INTO background_videos (`id`, `name`, `screenshot`, `video`)  VALUES
        (NULL, 'checkeered-360p', 'checkeered-360p.jpg', 'checkeered-360p.mp4'),
        (NULL, 'dark-blue-glow-360p', 'dark-blue-glow-360p.jpg', 'dark-blue-glow-360p.mp4');

    ALTER TABLE `team_members` ADD `title` VARCHAR(128) NOT NULL DEFAULT '' AFTER `first_name`;
    ALTER TABLE news ADD name varchar(254) NOT NULL DEFAULT '' AFTER id;
    TRUNCATE news;
    ALTER TABLE `news` ADD UNIQUE(`name`);
    UPDATE news_feeds SET last_updated = '2015-01-01';

    UPDATE menuitems SET `order` = `order`+1 WHERE `order` > 15;
    INSERT INTO menuitems VALUES (null, 1, 'Background Videos', 'background_videos', 'admin_index', 'admin/background_videos', 1, 105, now(), now(), 16);

    DROP TABLE weather_panels;
    DROP TABLE weather_undergrounds_weather_panels;
    DROP TABLE weather_undergrounds;
    DROP TABLE forecasts;
    DROP TABLE event_types;
    DROP TABLE event_repeats;
    DROP TABLE events_forcalender;
    ALTER TABLE `events`      DROP `recurring_event`,   DROP `event_type_id`,   DROP `event_repeat_id`;
    ALTER TABLE `events`      DROP `status`, DROP `active`;
    ALTER TABLE `events`      DROP `display_begining`,  DROP `display_ending`,  DROP `modified`, DROP `created`;
    ALTER TABLE `promotions`  DROP `start_displaying`,  DROP `stop_displaying`, DROP `modified`, DROP `created`;

    CREATE TABLE IF NOT EXISTS `recurrences` (
        id                INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `repeat_start`    date NOT NULL,
        `repeat_interval` varchar(255) NOT NULL,
        `repeat_year`     varchar(255) NOT NULL,
        `repeat_month`    varchar(255) NOT NULL,
        `repeat_day`      varchar(255) NOT NULL,
        `repeat_week`     varchar(255) NOT NULL,
        `repeat_weekday`  varchar(255) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ALTER TABLE events ADD recurrence_id INT UNSIGNED DEFAULT NULL;
    ALTER TABLE events ADD FOREIGN KEY(recurrence_id) REFERENCES recurrences(id) ON DELETE SET NULL ON UPDATE RESTRICT;

    ALTER TABLE promotions ADD recurrence_id INT UNSIGNED DEFAULT NULL;
    ALTER TABLE promotions ADD FOREIGN KEY(recurrence_id) REFERENCES recurrences(id) ON DELETE SET NULL ON UPDATE RESTRICT;

COMMIT;
