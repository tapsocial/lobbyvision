TapSocial / Lobbyvision
=======================

Technologies used
------------------

- CakePHP v2.3.8 as framework
- (Twitter's Bootstrap v2)[http://getbootstrap.com/2.3.1/] as admin CSS framework
- Image uploading for different models by deprecated (MeioUpload)[https://github.com/jrbasso/MeioUpload] cake plugin
- widget animation done with the bxslider library http://bxslider.com/


Deploy
-------

- chmod a+w -Rvf app/webroot/uploads
