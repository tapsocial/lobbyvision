<?
/* @var $this View */
echo $this->Html->script('jquery.textareaCounter.plugin');
echo $this->Html->script('datepicker/zebra_datepicker.js');
echo $this->Html->css('datepicker/zebra_datepicker.css');
?>
<script type="text/javascript">
$(document).ready(function(){
    $('#PromotionAllDay').change(function(){
        var isChecked = $('#PromotionAllDay').attr('checked')?true:false;
        if(isChecked){
            $('#PromotionStartHour').val('12');
            $('#PromotionStartMin').val('00');
            $('#PromotionStartMeridian').val('am');

            $('#PromotionEndMonth').val($('#PromotionStartMonth').val());
            $('#PromotionEndDay').val($('#PromotionStartDay').val());
            $('#PromotionEndYear').val($('#PromotionStartYear').val());

            $('#PromotionEndHour').val('11');
            $('#PromotionEndMin').val('59');
            $('#PromotionEndMeridian').val('pm');

            $('#PromotionStartHour,#PromotionStartMin,#PromotionEndHour,#PromotionStartMeridian, #PromotionEndMin,#PromotionEndMeridian')
                    .attr('disabled','disabled')
                    .addClass('hide');
        } else {
            $('#PromotionStartHour,#PromotionStartMin,#PromotionEndHour,#PromotionStartMeridian, #PromotionEndMin,#PromotionEndMeridian,#PromotionEndMonth,#PromotionEndDay,#PromotionEndYear')
                    .removeAttr('disabled hide')
                    .removeClass('hide');
             $('.enddate').show();
        }
    }).change();
    var options3 = {
        'maxCharacterSize': 880,
        'originalStyle': 'originalTextareaInfo',
        'warningStyle' : 'warningTextareaInfo',
        'warningNumber': 40,
        'displayFormat' : '#left Characters Left / #max'
    };
    $('#PromotionDescription').textareaCount(options3, function(data){
        $('#showData').html(data.input + " characters input. <br />" + data.left + " characters left. <br />" + data.max + " max characters. <br />" + data.words + " words input.");
    });
    $('.datePicker').Zebra_DatePicker();
    $('#RecurrenceRepeatType').change(function() {
        if ($(this).val() == 'never') {
            $('#RecurrenceRepeatStart').parents('div.input').hide();
            $('#RecurrenceRepeatStart').removeAttr('required');
        } else {
            $('#RecurrenceRepeatStart').attr('required', 'required');
            $('#RecurrenceRepeatStart').attr('placeholder', 'YYYY-MM-DD');
            $('#RecurrenceRepeatStart').removeAttr('readonly');
            $('#RecurrenceRepeatStart').parents('div.input').show();
        }
    }).change();
});
</script>
<style>
    .mrgin-left, .charleft {float: none; margin-left: 0;}
</style>
<h1><?php echo __('Add Promotion'); ?></h1>
<?php echo $this->Form->create('Promotion',array('type' => 'file')); ?>
    <fieldset>
    <?php
        echo $this->Form->input('name');
        echo $this->Form->input('description',array('maxlength'=>'880'));
        echo $this->Form->input('all_day', ['checked' => 'checked']);
        echo $this->Form->input('start');
        echo $this->Form->input('end');
        echo $this->Form->input('image',array('type' => 'file'));
        echo $this->Form->input('Recurrence.repeat_type',  [
            'label'   => 'Repeat Frequency',
            'options' => $this->viewVars['recurrences'],
            'value'   => $this->viewVars['recurrence']
        ]);
        echo $this->Form->input('Recurrence.repeat_start', [
            'type'  => 'text',
            'class' => 'datePicker',
            'label' =>'Repeat Beginning On Date',
        ]);
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
