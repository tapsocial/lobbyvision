<h2><?php echo __('Promotions'); ?></h2>
<table cellpadding="0" cellspacing="0">
<tr>

        <th>ID<?php //echo $this->Paginator->sort('id'); ?></th>
        <th><?php echo $this->Paginator->sort('start'); ?></th>
        <th><?php echo $this->Paginator->sort('end'); ?></th>
        <th><?php echo $this->Paginator->sort('name'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
</tr>
<?php
$num=($this->request->params['paging']['Promotion']['page']-1)*$this->request->params['paging']['Promotion']['limit'];

foreach ($promotions as $promotion): $num++;?>
<tr>
    <td><?php echo $num;//h($promotion['Promotion']['id']); ?>&nbsp;</td>
    <td><?php echo h($promotion['Promotion']['start']); ?>&nbsp;</td>
    <td><?php echo h($promotion['Promotion']['end']); ?>&nbsp;</td>
    <td><?php echo h($promotion['Promotion']['name']); ?>&nbsp;</td>
    <td class="actions">
        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $promotion['Promotion']['id'])); ?>
        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $promotion['Promotion']['id']), null, __('Are you sure you want to delete # %s?', $promotion['Promotion']['id'])); ?>
    </td>
</tr>
<?php endforeach; ?>
</table>
<p>
<?php
echo $this->Paginator->counter(array(
'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
));
?>	</p>

<div class="paging">
<?php
    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
?>
</div>

<div class="actions">
    <ul>
        <li><?php echo $this->Html->link(__('Add new'), array('action' => 'add')); ?></li>

    </ul>
</div>

