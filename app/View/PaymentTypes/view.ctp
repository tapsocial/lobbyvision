<div class="paymentTypes view">
<h2><?php echo __('Payment Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($paymentType['PaymentType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($paymentType['PaymentType']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Payment Type'), array('action' => 'edit', $paymentType['PaymentType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Payment Type'), array('action' => 'delete', $paymentType['PaymentType']['id']), null, __('Are you sure you want to delete # %s?', $paymentType['PaymentType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Payment Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Payment Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bookings'), array('controller' => 'bookings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Booking'), array('controller' => 'bookings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bookings Extra Services'), array('controller' => 'bookings_extra_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bookings Extra Service'), array('controller' => 'bookings_extra_services', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Bookings'); ?></h3>
	<?php if (!empty($paymentType['Booking'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Villa Id'); ?></th>
		<th><?php echo __('Contact Person Name'); ?></th>
		<th><?php echo __('Visitor Name'); ?></th>
		<th><?php echo __('Address 1'); ?></th>
		<th><?php echo __('Address 2'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('State Id'); ?></th>
		<th><?php echo __('Post Code'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Available Time'); ?></th>
		<th><?php echo __('Checkin Date'); ?></th>
		<th><?php echo __('Checkout Date'); ?></th>
		<th><?php echo __('Book Status Id'); ?></th>
		<th><?php echo __('Payment Status Id'); ?></th>
		<th><?php echo __('Payment Type Id'); ?></th>
		<th><?php echo __('Enabled'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($paymentType['Booking'] as $booking): ?>
		<tr>
			<td><?php echo $booking['id']; ?></td>
			<td><?php echo $booking['villa_id']; ?></td>
			<td><?php echo $booking['contact_person_name']; ?></td>
			<td><?php echo $booking['visitor_name']; ?></td>
			<td><?php echo $booking['address_1']; ?></td>
			<td><?php echo $booking['address_2']; ?></td>
			<td><?php echo $booking['city']; ?></td>
			<td><?php echo $booking['country_id']; ?></td>
			<td><?php echo $booking['state_id']; ?></td>
			<td><?php echo $booking['post_code']; ?></td>
			<td><?php echo $booking['phone']; ?></td>
			<td><?php echo $booking['email']; ?></td>
			<td><?php echo $booking['available_time']; ?></td>
			<td><?php echo $booking['checkin_date']; ?></td>
			<td><?php echo $booking['checkout_date']; ?></td>
			<td><?php echo $booking['book_status_id']; ?></td>
			<td><?php echo $booking['payment_status_id']; ?></td>
			<td><?php echo $booking['payment_type_id']; ?></td>
			<td><?php echo $booking['enabled']; ?></td>
			<td><?php echo $booking['modified']; ?></td>
			<td><?php echo $booking['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'bookings', 'action' => 'view', $booking['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'bookings', 'action' => 'edit', $booking['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'bookings', 'action' => 'delete', $booking['id']), null, __('Are you sure you want to delete # %s?', $booking['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Booking'), array('controller' => 'bookings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Bookings Extra Services'); ?></h3>
	<?php if (!empty($paymentType['BookingsExtraService'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Booking Id'); ?></th>
		<th><?php echo __('Payment Type Id'); ?></th>
		<th><?php echo __('Payment Status Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($paymentType['BookingsExtraService'] as $bookingsExtraService): ?>
		<tr>
			<td><?php echo $bookingsExtraService['id']; ?></td>
			<td><?php echo $bookingsExtraService['booking_id']; ?></td>
			<td><?php echo $bookingsExtraService['payment_type_id']; ?></td>
			<td><?php echo $bookingsExtraService['payment_status_id']; ?></td>
			<td><?php echo $bookingsExtraService['modified']; ?></td>
			<td><?php echo $bookingsExtraService['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'bookings_extra_services', 'action' => 'view', $bookingsExtraService['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'bookings_extra_services', 'action' => 'edit', $bookingsExtraService['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'bookings_extra_services', 'action' => 'delete', $bookingsExtraService['id']), null, __('Are you sure you want to delete # %s?', $bookingsExtraService['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Bookings Extra Service'), array('controller' => 'bookings_extra_services', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
