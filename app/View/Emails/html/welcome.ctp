<p>Welcome!  You are only minutes away from providing your customers a dynamic display in your business.  TapSocial Digital Signage is quick and easy to configure.  Simply log in at www.TapSocial.net/digital and begin the process.</p>
<p>We have an extensive Help section once you are logged in.  If at any time you need personal support, please contact us at feedback@tapsocial.net and we will respond to you promptly.</p>

<p>Again, thank you for registering for TapSocial Digital Signage! </p>

<p>Regards,</p>

<p>The TapSocial Team</p>
