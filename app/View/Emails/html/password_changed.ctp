<p>Dear TapSocial Digital Signage Customer,</p>
<p>Our records indicate that you have recently changed your password.  If you feel that this is an error, please contact us immediately at <a href="mailto:feedback@tapsocial.net">feedback@tapsocial.net.</a></p>


<p>Thanks for being a customer of TapSocial Digital Signage.<br>

Regards,

The TapSocial Team
</p>