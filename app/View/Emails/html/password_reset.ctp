<p>Dear TapSocial Digital Signage Customer,<br>

You recently requested to reset your password.  Please follow <a href="<?php echo Router::url('/', true) ?>/users/newpassword/<?php echo $otp;?>">this link</a> to securely change your password.  </p>

<p>If at any time you need personal support, please contact us at feedback@tapsocial.net and we will respond to you promptly.
</p>

<p>Regards,</p>

<p>The TapSocial Team</p>
