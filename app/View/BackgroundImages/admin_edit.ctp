<div class="backgroundImages form">
<?php echo $this->Form->create('BackgroundImage'); ?>
	<fieldset>
		<legend><?php echo __('Edit Background Image'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		if(isset($this->request->data['BackgroundImage']['src'])){
			echo '<div class=""><label>&nbsp;</label>';
			echo $this->Html->image('../uploads/background_image/src/thumb/small/'.$this->request->data['BackgroundImage']['src'],array('class'=>'thumbnail','style'=>'display:inline;max-width:200px;height:120px'));
			echo '</div><br>';
		}
		

		echo $this->Form->input('src',array('type'=>'file','after'=>'<small class="padding-left">Upload new picture to replace current one.</small>'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('BackgroundImage.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('BackgroundImage.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Background Images'), array('action' => 'index')); ?></li>
	</ul>
</div>
