<div class="backgroundImages form">
<?php echo $this->Form->create('BackgroundImage',array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Add Background Image'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('src',array('type'=>'file'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Background Images'), array('action' => 'index')); ?></li>
	</ul>
</div>
