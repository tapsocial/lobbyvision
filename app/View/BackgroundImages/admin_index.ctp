<div class="backgroundImages index">
	<h2><?php echo __('Background Images'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('src'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($backgroundImages as $backgroundImage): ?>
	<tr>
		<td><?php echo h($backgroundImage['BackgroundImage']['id']); ?>&nbsp;</td>
		<td><?php echo h($backgroundImage['BackgroundImage']['name']); ?>&nbsp;</td>
		<td>
			<?php 	echo $this->Html->image('../uploads/background_image/src/thumb/small/'.$backgroundImage['BackgroundImage']['src'],array('style'=>'max-height:100px'));?>

	&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $backgroundImage['BackgroundImage']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $backgroundImage['BackgroundImage']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $backgroundImage['BackgroundImage']['id']), null, __('Are you sure you want to delete # %s?', $backgroundImage['BackgroundImage']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Background Image'), array('action' => 'add')); ?></li>
	</ul>
</div>
