<div class="backgroundImages view">
<h2><?php echo __('Background Image'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($backgroundImage['BackgroundImage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($backgroundImage['BackgroundImage']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Src'); ?></dt>
		<dd>
			<?php echo h($backgroundImage['BackgroundImage']['src']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Background Image'), array('action' => 'edit', $backgroundImage['BackgroundImage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Background Image'), array('action' => 'delete', $backgroundImage['BackgroundImage']['id']), null, __('Are you sure you want to delete # %s?', $backgroundImage['BackgroundImage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Background Images'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Background Image'), array('action' => 'add')); ?> </li>
	</ul>
</div>
