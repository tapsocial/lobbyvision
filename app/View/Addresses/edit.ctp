<div class="addresses form">
<?php echo $this->Form->create('Address'); ?>
	<fieldset>
		<legend>Edit <?php echo h($this->request->data['Address']['address_type']==1?'Mailing':'Billing'); ?> Address</legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('address_type',array('type'=>'hidden'));
		echo $this->Form->input('address_line_1');
		echo $this->Form->input('address_line_2');
		echo $this->Form->input('city');
		echo $this->Form->input('state/province');
		echo $this->Form->input('country_id');
		echo $this->Form->input('zip');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Address.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Address.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Addresses'), array('action' => 'index')); ?></li>
		
	</ul>
</div>
