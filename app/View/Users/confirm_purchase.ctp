<div class="users form two-column">
    <?php echo $this->Form->create('User'); ?>
    <section class="">
       <fieldset>
           <section class="span10"><h3>You have selected...</h3>
                <h4 class="text-info">
                    <?php echo $subscriptionPackage['SubscriptionPackage']['months'].'mo &nbsp;&nbsp;&nbsp; $'.$subscriptionPackage['SubscriptionPackage']['price_per_month'].'/mo&nbsp;&nbsp;&nbsp;'.$subscriptionPackage['SubscriptionPackage']['name'];?>
                </h4>
            </section>
            <section class="span10">
                <legend><?php echo __('Contact and Billing Information'); ?></legend>
            </section>
            <section class="span5">
            <?php
                //echo $this->Form->input('user_id');
                echo $this->Form->input('Contact.company_name',array('disabled'=>'disabled'));
                echo $this->Form->input('Contact.contact_name',array('disabled'=>'disabled'));
                echo $this->Form->input('Contact.email',array('disabled'=>'disabled'));
            ?>
            </section>
            <section class="span5">
            <?php
                //echo $this->Form->select('timezone', array('Please select your timezone', 'Europe/London');

                echo $this->Form->input('Contact.timezone',array('options'=>Configure::read('timezone'),'label'=>'Please select your timezone','default'=>'America/New_York','disabled'=>'disabled'));
                echo $this->Form->input('Contact.phone',array('disabled'=>'disabled'));
                echo $this->Form->input('Contact.contact_preference',array('disabled'=>'disabled'));
            ?>
        </fieldset>
    </section>

    <fieldset>
        <section class="span10">
            <legend><?php echo __('Account Access'); ?></legend>
        </section>
        <section class="span5">
        <?php
            echo $this->Form->input('group_id',array('type'=>'hidden','value'=>2));
            echo $this->Form->input('username',array('after'=>'<span class="help-block" id="avail" />','disabled'=>'disabled'));
            echo $this->Form->input('email');

            if($this->Session->check('Register.subscription_package_id')){
                echo $this->Form->input('subscription_package_id',array('type'=>'hidden','value'=>$this->Session->read('Register.subscription_package_id')));
            } else {
                echo $this->Form->input('subscription_package_id');
            }
        ?>
        </section>
    </fieldset>

    <section class="span5">
       <fieldset>
            <legend><?php echo __('Mailing Address'); ?></legend>
        <?php
        echo $this->Form->input('MailingAddress.address_type',array('type'=>'hidden','value'=>1));
            echo $this->Form->input('MailingAddress.address_line_1',array('disabled'=>'disabled'));
            echo $this->Form->input('MailingAddress.address_line_2',array('disabled'=>'disabled'));
            echo $this->Form->input('MailingAddress.city',array('disabled'=>'disabled'));
            echo $this->Form->input('MailingAddress.state/province',array('id'=>'MailingAddressState','disabled'=>'disabled',array('disabled'=>'disabled')));
            echo $this->Form->input('MailingAddress.country_id',array('default'=>250,'disabled'=>'disabled'));
            echo $this->Form->input('MailingAddress.zip',array('disabled'=>'disabled'));

        ?>
        </fieldset>
    </section>

    <section class="span5">
       <fieldset>
            <legend><?php echo __('Billing Address'); ?>
                <small>
                    <?=$this->Form->checkbox('sameasmailingaddress',array('label'=>'Same as Mailing Address','style'=>'float:none'));?>
                    <!-- Same as Mailing Address -->
                </small>
            </legend>
        <?php
            echo $this->Form->input('BillingAddress.address_type',array('type'=>'hidden','value'=>1));
            echo $this->Form->input('BillingAddress.address_line_1',array('disabled'=>'disabled'));
            echo $this->Form->input('BillingAddress.address_line_2',array('disabled'=>'disabled'));
            echo $this->Form->input('BillingAddress.city',array('disabled'=>'disabled'));
            echo $this->Form->input('BillingAddress.state/province',array('id'=>'BillingAddressState','disabled'=>'disabled'));
            echo $this->Form->input('BillingAddress.country_id',array('default'=>250,'disabled'=>'disabled'));
            echo $this->Form->input('BillingAddress.zip',array('disabled'=>'disabled'));
        ?>
        </fieldset>
    </section>
    <?php echo $this->Form->end(); ?>

    <section class="span10">
        <h4 class="text-info">
        <?php //echo $subscriptionPackage['SubscriptionPackage']['months'].'mo &nbsp;&nbsp;&nbsp; $'.$subscriptionPackage['SubscriptionPackage']['price_per_month'].'/mo&nbsp;&nbsp;&nbsp;'.$subscriptionPackage['SubscriptionPackage']['name'];?>
    </h4>

    </section>
    <section class="span10">
        <p class="muted">
            TapSocial utilizes PayPal for subscription purchases.  PayPal is committed to ensuring that all transaction information is safe and secure.  For more information about PayPal, please <a href="https://www.paypal-media.com/about" target="_blank">click here</a>.
        </p>
        <p class="muted">
            Upon selecting Purchase below, you will be routed to PayPal to enter your payment information.
        </p>
    </section>
    <?=$this->Paypal->button('Purchase', array(
        'type'       => 'subscribe',
        'item_name'  => $subscriptionPackage['SubscriptionPackage']['name'],
        'custom'     => $userId,
        'amount'     => ( $subscriptionPackage['SubscriptionPackage']['price_per_month']
                            * $subscriptionPackage['SubscriptionPackage']['months']
                        ),
        'term'       => 'month',
        'period'     => $subscriptionPackage['SubscriptionPackage']['months'],
        'notify_url' => Router::url('/', true).'paypal_ipn/process')
    );?>
</div>