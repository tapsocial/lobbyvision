<div class="users form two-column">
<?php //debug($this->request->data); ?>
<?php echo $this->Form->create('User'); ?>
<section class="">

   <fieldset>

<section class="span8">
		<legend><?php echo __('Account Access'); ?></legend>
		</section>
		<section class="span8">
	<?php
		echo $this->Form->input('group_id',array('type'=>'hidden','value'=>2));
		echo $this->Form->input('username',array('after'=>'<span class="help-block" id="avail" />'));
		echo $this->Form->input('email');
		echo $this->Form->input('password');
		
	?>
	</section>
  
</fieldset>

   <section class="span8">
<?php echo $this->Form->end(__('Update')); ?>
</section>
</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
