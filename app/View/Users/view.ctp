<div class="users view">
<h2><?php  echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		
		
		<dt><?php echo __('Subscription'); ?></dt>
		<dd>
			<?php echo $user['SubscriptionPackage']['months'].'mo &nbsp;&nbsp;&nbsp; $'.$user['SubscriptionPackage']['price_per_month'].'/mo&nbsp;&nbsp;&nbsp;'.$user['SubscriptionPackage']['name'];?>

			<?php //echo h($user['SubscriptionPackage']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit My Account'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		
	</ul>
</div>
