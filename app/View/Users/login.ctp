<?=$this->Session->flash('auth'); ?>
<style>
form div {clear: none;}
#UserLoginForm .submit {text-align: left;}
</style>
<h2>Login</h2>
<div class="row-fluid">
    <section class="span6 well">
        <h4 class="" style="color:#5A5A5A">Existing Customers</h4>
        <?=$this->Form->create('User', array('url' => '#', 'inputDefaults' => array('label' => false), 'class' => 'form form-horizontal'));?>
        <div class="control-group">
            <?=$this->Form->label('username', 'Username', array('class' => 'control-label'));?>
            <div class="controls">
                <?=$this->Form->input('username', array('class' => 'span12')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->
        <div class="control-group">
            <?=$this->Form->label('password', 'Password', array('class' => 'control-label'));?>
            <div class="controls">
                <?=$this->Form->input('password', array('class' => 'span12')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->
        <div class="offset5">
        <?php echo $this->Html->link('Forgot password',array('action'=>'forgotpassword')); ?>
        </div>
          <div class="control-group">
                <?php echo $this->Form->submit('Login', array('btn')); ?>
          </div>
        <?=$this->Form->end() ?>
    </section>
    <section class="span6 well">
        <h4 class="" style="color:#5A5A5A">New Customers</h4>
        <p><?php echo $this->Html->link('Create New account',array('action'=>'register'),array('class'=>'btn-link')); ?></p>
        <p><a href="http://tapsocial.net/tapsocial-digital-signage/" >Learn more about TapSocial Digital Signage</a></p>
    </section>
</div>