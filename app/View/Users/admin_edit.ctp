<div class="users form two-column">
<?php //debug($this->request->data); ?>
<?php echo $this->Form->create('User'); ?>
<section class="">

   <fieldset>
   
<!--    <section class="span7"><h3>Package...</h3>
			<h4 class="text-info"><?php //echo $subscriptionPackage['SubscriptionPacekage']['months'].'mo &nbsp;&nbsp;&nbsp; $'.$subscriptionPackage['SubscriptionPackage']['price_per_month'].'/mo&nbsp;&nbsp;&nbsp;'.$subscriptionPackage['SubscriptionPackage']['name'];?>
			</h4>
</section> -->
<section class="span8">
		<legend><?php echo __('Account Access'); ?></legend>
		</section>
		<section class="span8">
	<?php
	echo $this->Form->input('id');
		echo $this->Form->input('group_id',array('type'=>'hidden','value'=>2));
		echo $this->Form->input('username',array('after'=>'<span class="help-block" id="avail" />'));
		echo $this->Form->input('email');
		echo $this->Form->input('active');
		echo $this->Form->input('password',array('value'=>'','required'=>false));
		//echo $this->Form->input('password2',array('type'=>'password','label'=>'Confirm Password'));
		echo $this->Form->input('subscription_package_id',array('options'=>$subscription_packages));
	?>
	</section>
  
</fieldset>
</section>
	<fieldset>
	<section class="span8">
		<legend><?php echo __('Contact and Billing Information'); ?></legend>
	</section>
		<section class="span4">
	<?php
		//echo $this->Form->input('user_id');
	echo $this->Form->input('Contact.id');
		echo $this->Form->input('Contact.company_name');
		echo $this->Form->input('Contact.contact_name');
		echo $this->Form->input('Contact.email');
		?>
		</section>
		<section class="span4">
		<?php 
		//echo $this->Form->select('timezone', array('Please select your timezone', 'Europe/London');

		echo $this->Form->input('Contact.timezone',array('options'=>Configure::read('timezone'),'label'=>'Please select your timezone','default'=>'America/New_York'));
		echo $this->Form->input('Contact.phone');
		echo $this->Form->input('Contact.contact_preference',array('options'=>array('Phone', 'Email')));
	?>
	
    </section>
	</fieldset>

<section class="span4">
   <fieldset>
		<legend><?php echo __('Mailing Address'); ?></legend>
	<?php
	echo $this->Form->input('MailingAddress.id');
	echo $this->Form->input('MailingAddress.address_type',array('type'=>'hidden','value'=>1));
		echo $this->Form->input('MailingAddress.address_line_1');
		echo $this->Form->input('MailingAddress.address_line_2');
		echo $this->Form->input('MailingAddress.city');
		echo $this->Form->input('MailingAddress.state/province',array('id'=>'MailingAddressState'));
		echo $this->Form->input('MailingAddress.country_id',array('default'=>250));
		echo $this->Form->input('MailingAddress.zip');
		
	?>
	</fieldset>
</section>

<section class="span4">
   <fieldset>
		<legend><?php echo __('Billing Address'); ?>
			<small>
<?php 
echo $this->Form->checkbox('sameasmailingaddress',array('label'=>'Same as Mailing Address','style'=>'float:none'));?> Same as Mailing Address
</small>
		</legend>
	<?php
	echo $this->Form->input('BillingAddress.id');
		echo $this->Form->input('BillingAddress.address_type',array('type'=>'hidden','value'=>2));
		echo $this->Form->input('BillingAddress.address_line_1');
		echo $this->Form->input('BillingAddress.address_line_2');
		echo $this->Form->input('BillingAddress.city');
		echo $this->Form->input('BillingAddress.state/province',array('id'=>'BillingAddressState'));
		echo $this->Form->input('BillingAddress.country_id',array('default'=>250));
		echo $this->Form->input('BillingAddress.zip');
	?>
	</fieldset>
</section>
   <section class="span8">
<?php echo $this->Form->end(__('Update')); ?>
</section>
</div>
<script type="text/javascript">
$(document).ready(function(){
	
		$("#UserUsername").change(function(){ 

    jQuery.ajax({
        type: "GET", 
        url: "<?php echo Router::url('/', true);?>users/CheckUnique/"+jQuery(this).val(), 
        success: function(data){ 
            jQuery('#avail').html(data);
            if(data=="This Username is already taken")
            	jQuery('#UserUsername').focus();
        } 
    }); 
});
jQuery("#UserSameasmailingaddress").change(function(){
	if(this.checked){
		jQuery('#BillingAddressAddressLine1').val(jQuery('#MailingAddressAddressLine1').val());
		jQuery('#BillingAddressAddressLine2').val(jQuery('#MailingAddressAddressLine2').val());
		jQuery('#BillingAddressCity').val(jQuery('#MailingAddressCity').val());
		jQuery('#BillingAddressState').val(jQuery('#MailingAddressState').val());
		jQuery('#BillingAddressCountryId').val(jQuery('#MailingAddressCountryId').val());
		jQuery('#BillingAddressZip').val(jQuery('#MailingAddressZip').val());
	}else{
		jQuery('#BillingAddressAddressLine1').val('');
		jQuery('#BillingAddressAddressLine2').val('');
		jQuery('#BillingAddressCountryId').val('');
		jQuery('#BillingAddressCity').val('');
		jQuery('#BillingAddressZip').val('');
		

	}
})
jQuery('#Contact0Email').change(function(){
	if(jQuery('#UserEmail').val()==''){
		jQuery('#UserEmail').val(jQuery('#Contact0Email').val())
	}
});

})
</script>


<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
