<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Edit Account Settings'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('username');
		echo $this->Form->input('email');
		echo $this->Form->input('password',array('value'=>'','required'=>false,'after'=>'<br><span class="mrgin-left">Leave password blank if dont want to change.</span>'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		
	</ul>
</div>

<script>
jQuery(document).ready(function($) {
  
});
</script>
