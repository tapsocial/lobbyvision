<div class="agents form">

<?php echo $this->Form->create('User', array('action' => 'changepassword')); ?>
<fieldset>
	<?php 
 echo $this->Form->inputs(array(
    'legend' => __('Change Password'),
    'current_password' => array('type'=>'password'),
    'new_password' => array('type'=>'password'),
    'confirm_password' => array('type'=>'password')

));
?>
 </fieldset>

<?php echo $this->Form->end('Submit'); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Change Password'), array('controller' => 'users', 'action' => 'admin_changepassword')); ?> </li>
		<li><?php echo $this->Html->link(__('List Villas'), array('controller' => 'villas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Villa'), array('controller' => 'villas', 'action' => 'add')); ?> </li>
	</ul>
</div>
