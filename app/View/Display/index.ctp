<?php
/**
 * There is a lot of improvements to the responsive display
 *
 * @see https://css-tricks.com/viewport-sized-typography/
 *  - vw = (viewport width)/100
 *  - vh = (viewport height)/100
 *  - vmin
 * @see http://maloweb.com/snippets/responsivestuff.html
 *
 * @todo fullscreen on load
 */

/* @var $this View */

$this->layout                = 'bootstrap3';
$this->viewVars['htmlTitle'] = 'LobbyVision';
$design                      = ['Design' => $this->viewVars['authUser']['Design']];
$background                  = null;
switch ($design['Design']['background_type']) {
    case 1:
        $background = "background-image: url('/uploads/background_image/src/{$design['Design']['background']}');  background-size: 100% 100%; ";
        break;
    case 2:
        $background = "background-color: {$design['Design']['background']};";
        break;
}
?>
<script>
/**
 * Allow to call the document ready queue again
 */
(function($, undefined) {
    var isFired = false;
    var oldReady = jQuery.fn.ready;
    $(function() {
        isFired = true;
        $(document).ready();
    });
    jQuery.fn.ready = function(fn) {
        if(fn === undefined) {
            $(document).trigger('_is_ready');
            return;
        }
        if(isFired) {
            window.setTimeout(fn, 1);
        }
        $(document).bind('_is_ready', fn);
    };
})(jQuery);


</script>
<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="/js/gridstack.js/dist/gridstack.css" />
<link rel="stylesheet" href="/js/gridstack.js/dist/gridstack-extra.css"/>

<link rel="stylesheet" href="/js/bxslider-4/dist/jquery.bxslider.min.css" />
<script src="/js/bxslider-4/dist/jquery.bxslider.min.js"></script>

<link rel="stylesheet" href="/js/jquery-simplyscroll/jquery.simplyscroll.css" />
<script src="/js/jquery-simplyscroll/jquery.simplyscroll.min.js"></script>

<link rel="stylesheet" href="/css/display/index.css"/>
<link rel="stylesheet" href="/css/display/theme-<?=$design['Design']['shape']?>.css"/>

<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
<script src="/js/gridstack.js/dist/gridstack.js"></script>
<script src="/js/jquery.jclock.js"></script>

<script>
function rgba(rgb) {
    rgb = rgb.match(/rgba?\((\d+),\s*(\d+),\s*(\d+)/);
    rgb.shift();
    return rgb;
}

function startGrid() {
    var grid      = $('.grid-stack').gridstack({
        cellHeight: '14.8vh',
        height:     6,
        width:      4,
        disableDrag: true,
        disableResize: true,
    }).data('gridstack');
    grid.disable();

    /**
     * Sets the background opacity of each widget
     */
    $('.grid-stack-item-content').each(function(index, element) {
        var color   = $(element).css('background-color');
        if (color == 'rgba(0, 0, 0, 0)' || color == 'transparent')  {
            return false;
        }
        color       = rgba(color);
        var opacity = $(element).parent().data('background-opacity');
        if (opacity != undefined) {
            opacity = opacity/100;
            color = 'rgba('+ color[0]+ ', '+ color[1]+ ', '+ color[2]+ ', ' + opacity + ')';
            $(element).css('background-color', color);
        }
    });
}

$(function(){
    startGrid();
});

/**
 * Reload the content every  30 minuts
 *
 * Let's calculate how much time is needed to finish the current 30 minutes.
 * After reaching the :00 or :30, reload every 30 minutes. Put the content in a
 * container and once we have the content, unbind everything, remove the display
 * and replace the content and relaunch all documen.ready hooked functions.
 */
var startIn = (60 - new Date().getMinutes()) %30 ;
setTimeout (setInterval(function(){
    $('#ajaxContainer').load('/display #display', function() {
        $('body > #display *').unbind();
        $('body > #display').unbind().remove();
        $('body').append($('#ajaxContainer #display').detach());
        startGrid();
        $(document).ready();
    });
}, 30 * 60 * 1000), startIn * 60 *1000);
</script>

<? if ($background) :?>
    <style>
        body {<?=$background ?>}
    </style>
<? endif; ?>

<? if ($design['Design']['background_type'] == 3) :?>
    <video  autoplay loop id="backgroundVideo">
        <source src="/uploads/background_video/video/<?=$design['Design']['background']?>" type="video/mp4">
    </video>
<? endif ?>
<div id="ajaxContainer"></div>
<div id="display">
    <div class="grid-stack grid-stack-4">
        <?php foreach($this->viewVars['widgets'] as $widget) :?>
        <?
        $widgetType  = $widget['WidgetMaster']['name'];
        $widgetClass = 'WidgetInstance'.ucfirst($widgetType);
        App::uses($widgetClass, 'Model');

        $widgetObject  = new $widgetClass();
        $widgetContent = $widgetObject->getWidgetContent($widget);
        ?>
        <div class="grid-stack-item block-widget <?=$widget['WidgetMaster']['name']?>-block"
            id="widget-id-<?=$widget['WidgetInstance']['id'] ?>"
            data-gs-x="<?=$widget['WidgetInstance']['col'] ?>"
            data-gs-y="<?=$widget['WidgetInstance']['row'] ?>"
            data-gs-width="<?=$widget['WidgetInstance']['size_x'] ?>"
            data-gs-height="<?=$widget['WidgetInstance']['size_y'] ?>"
            data-background-opacity="<?=$widget['WidgetInstance']['opacity'] ?>"
        >
            <div class="grid-stack-item-content widget_container  <?=(!$widget['WidgetInstance']['show_title']) ? 'noTitle' : ''?>">
                <? if ($widget['WidgetInstance']['show_title']) :?>
                <h2 class="block-title"><?=$widget['WidgetInstance']['name'] ?></h2>
                <? endif ?>
                <?=$this->element("widgets/{$widget['WidgetMaster']['name']}_display", compact('design', 'widget', 'widgetContent'))?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
