<?
/**
 * @var $this View
 */

// $html = $this->loadHelper('html');
/* @var $html HtmlHelper */
$form = $this->loadHelper('form');
/* @var $form FormHelper */
?>
<script>
function addWidget(grid, type, widget) {
    var content = '';
    var willItfit = grid.willItFit(widget.col, widget.row, widget.size_x, widget.size_y, true);
    if (!willItfit) {
        alert('Not enough free space to place the widget');
        return false;
    }
    <? if (!Configure::read('debug')) : ?>
    var $button = $('[data-widget-type='+ type+ ']');
    if ($button.data('widget-singleton')) {
        $button.attr('disabled', 'disabled');
        $button.addClass('disabled');
    }
    <? endif ?>


    if (widget.id) {
        dataId  = ' data-id="' + widget.id + '" ';
        content = ' \
        <div ' + dataId +'data-type="' + type + '" class="grid-stack-item"> \
            <div class="grid-stack-item-content"> \
                <i class="fa fa-times action close"></i> \
                <a href="' + widget.editPath + '"> \
                <i class="icon fa ' + widget.icon + ' "></i> <br /> \
            ' + widget.name + '</a></div> \
        </div>';
    } else {
        content = ' \
        <div data-type="' + type + '" class="grid-stack-item"> \
            <div class="grid-stack-item-content"> \
                <i class="fa fa-times action close"></i> \
                <i class="icon fa ' + widget.icon + ' "></i> <br /> \
            ' + widget.name + '</div> \
        </div>';
    }
    var element = grid.addWidget($(content), widget.col, widget.row, widget.size_x, widget.size_y, !widget.id);
    grid.maxHeight(element, widget.height_max);
    grid.minHeight(element, widget.height_min);
    grid.maxWidth (element, widget.width_max);
    grid.minWidth (element, widget.width_min);
    if ((widget.height_max == widget.height_min) && (widget.width_max == widget.width_min)) {
        grid.resizable(element, false);
    }
}

function loadWidgets(grid) {
    grid.batchUpdate();
    <?php foreach($this->viewVars['widgets'] as $widget) :?>
    addWidget(grid, '<?=$widget['WidgetMaster']['name'] ?>', <?=json_encode($widget['WidgetInstance'] + $widget['WidgetMaster']) ?>);
    <?php endforeach; ?>
    grid.commit();
}

$(function(){
    var grid = $('.grid-stack').gridstack({
        animate:    true,
        cellHeight: 125,
        height:     6,
        width:      4,
    }).data('gridstack');
    loadWidgets(grid);

    $('#widgetList .btn').on('click', function() {
        var type = $(this).data('widget-type');
        addWidget(grid, type, {
             name:       'New ' + type + ' widget',
             icon:       'fa-plus-circle',
             size_x:     $(this).data('widget-width_def'),
             size_y:     $(this).data('widget-height_def'),
             height_min: $(this).data('widget-height_min'),
             height_max: $(this).data('widget-height_max'),
             width_min:  $(this).data('widget-width_min'),
             width_max:  $(this).data('widget-width_max'),
         });
    });

    $(document).on('click', '.grid-stack .action.close' ,function() {

        var $widget = $(this).parents('.grid-stack-item');
        var type    = $widget.data('type');

        var $button = $('[data-widget-type='+ type+ ']');
        if ($button.data('widget-singleton')) {
            $button.removeAttr('disabled');
            $button.removeClass('disabled');
        }
        grid.removeWidget($widget);

    });

    $(document).on('submit', '#DisplayEditForm', function() {
        var widgetData = _.map($('.grid-stack .grid-stack-item:visible'), function (el) {
            el = $(el);
            var node = el.data('_gridstack_node');
            return {
                id:     el.data('id'),
                type:   el.data('type'),
                col:    node.x,
                row:    node.y,
                size_x: node.width,
                size_y: node.height,
            };
        });
        widgetData     = JSON.stringify(widgetData);
        $('#DisplaySerialized').val(widgetData);
        return true;
    });
});
</script>
<style>
#widgetList {padding: 10px; white-space: nowrap; overflow-x: auto;}
#content.container       { overflow: initial;}
.display                 { background: #004756; height:  850px; padding: 10px 0; margin: 1em 0;}
.grid-stack-item-content { background: white; text-align: center; padding: 15px;}
.grid-stack-item-content .icon { font-size: 4em; margin: 4px; }

.grid-stack .action  {
    position: absolute;
    z-index: 1;
}
.grid-stack .action.close {
    right:  4px;
    top:    2px;
    cursor: not-allowed;
}
#DisplayEditForm .form-actions {padding: 0;}
</style>
<h2>Display editor</h2>
<p>
    Add widgets to be shown in your display. Drag and drop them to change
    where they are placed. Resize them using the bottom right corner of each
    widget. To configure each widget, double click on it.
</p>
    <div class="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Alert!</strong> Press the 'Save' button after adding and sizing your content modules.
    </div>
<div id="widgetList">
    <? foreach ($this->viewVars['masters'] as $master) :?>
        <button class="btn btn-info"
            data-widget-type="<?=$master['WidgetMaster']['name']?>"
            data-widget-height_min="<?=$master['WidgetMaster']['height_min']?>"
            data-widget-height_max="<?=$master['WidgetMaster']['height_max']?>"
            data-widget-height_def="<?=$master['WidgetMaster']['height_def']?>"
            data-widget-width_min="<?=$master['WidgetMaster']['width_min']?>"
            data-widget-width_max="<?=$master['WidgetMaster']['width_max']?>"
            data-widget-width_def="<?=$master['WidgetMaster']['width_def']?>"
            data-widget-singleton="<?=($master['WidgetMaster']['singleton'])? 'true' : 'false'?>"
            data-toggle="tooltip"
            title="<?=$master['WidgetMaster']['description']?>"
            >
            <i class="fa <?=$master['WidgetMaster']['icon']?>"></i> <?=$master['WidgetMaster']['name']?>
        </button>
    <? endforeach ?>
</div>
<br />
<?=$this->Form->create('Display')?>
    <div class="text-center">
        <button type="submit" class="btn btn-large btn-primary">save</button>
    </div>
    <div class="display"><div class="grid-stack grid-stack-4"></div></div>
    <?=$this->Form->input('serialized', ['type' => 'hidden'])?>
    <div class="text-center">
        <button type="submit" class="btn btn-large btn-primary">save</button>
    </div>
    <?$this->Form->end()?>
<link rel="stylesheet" href="/js/gridstack.js/dist/gridstack.css"/>
<link rel="stylesheet" href="/js/gridstack.js/dist/gridstack-extra.css"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
<script src="/js/gridstack.js/dist/gridstack.js"></script>