<div class="paymentStatuses form">
<?php echo $this->Form->create('PaymentStatus'); ?>
	<fieldset>
		<legend><?php echo __('Edit Payment Status'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('PaymentStatus.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('PaymentStatus.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Payment Statuses'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Bookings'), array('controller' => 'bookings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Booking'), array('controller' => 'bookings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bookings Extra Services'), array('controller' => 'bookings_extra_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bookings Extra Service'), array('controller' => 'bookings_extra_services', 'action' => 'add')); ?> </li>
	</ul>
</div>
