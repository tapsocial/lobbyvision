<?php  
//debug($this->request->params['requested']);
$class="";
if(!$villa_id)
$class="disabled";

	$tabs[]=array('title'=>'Info','url'=> array('controller'=>'villas','action' => 'edit',$villa_id));
	$tabs[]=array('title'=>'Image','url'=> array('controller'=>'villas_images','action' => 'edit',$villa_id),'options'=>array('class'=>$class));
	$tabs[]=array('title'=>'Features','url'=> array('controller'=>'villas_features','action' => 'edit',$villa_id)); 
	$tabs[]=array('title'=>'Seasons Price','url'=> array('controller'=>'villa_seasons_prices','action' => 'edit',$villa_id));


if(isset($tabs)):?>
<ul class="nav nav-tabs">
<?php

	foreach($tabs as $tab){ //debug($tab);
		$active="";
	if($this->request->params['controller']==$tab['url']['controller'])
		$active="active";
$url = null;
$options = array();
$confirmMessage = false;
	extract($tab);
	?>
    <li class="<?php echo $active;?>">
		<?php if(!$villa_id)
			echo $this->Html->link($title, '#', $options, $confirmMessage);
		else
			echo $this->Html->link($title, $url, $options, $confirmMessage);?>
    </li>
	<?php
	}?>
	</ul>
	<?php
	endif;
 ?>