<?php 
if(isset($tabs)):?>
<ul class="nav nav-tabs">
<?php

	foreach($tabs as $tab){ //debug($tab);
		$active="";
	if($this->request->params['controller']==$tab['url']['controller'])
		$active="active";
$url = null;
$options = array();
$confirmMessage = false;
	extract($tab);
	?>
    <li class="<?php echo $active;?>">
		<?php echo $this->Html->link($title, $url, $options, $confirmMessage);?>
    </li>
	<?php
	}
	endif;
 ?>