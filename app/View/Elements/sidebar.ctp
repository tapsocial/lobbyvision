<style>
    #sidebar .nav li {margin: 0;}
</style>
<div id="sidebar">
    <h4>Look & Feel</h4>
    <ul class="nav nav-pills nav-stacked">

        <li class="<?=($this->request->params['controller'] == 'display') ? 'active' : ''?>">
            <?=$this->Html->link('Display Layout', array('controller'=>'display','action'=>'edit','plugin'=>false));?>
        </li>
        <li class="<?=($this->request->params['controller'] == 'Designs' && $this->request->params['action'] == 'update_logo') ? 'active' : ''?>">
            <?=$this->Html->link('Company Logo',array('controller'=>'Designs','action'=>'update_logo','plugin'=>false));?>
        </li>
        <li class="<?=($this->request->params['controller'] == 'Designs' && $this->request->params['action'] == 'update_background') ? 'active' : ''?>">
            <?=$this->Html->link('Appearance',array('controller'=>'Designs','action'=>'update_background','plugin'=>false));?>
        </li>
    </ul>

    <h4>Content Manager</h4>
    <ul class="nav nav-pills nav-stacked">
        <li class="<?=($this->request->params['controller'] == 'events')     ? 'active' : ''?>">
            <?=$this->Html->link('Events',             array('controller'=>'events',   'action'=>'index','plugin'=>false));?>
        </li>
        <li class="<?=($this->request->params['controller'] == 'promotions') ? 'active' : ''?>">
            <?=$this->Html->link('Promotions & Sales', array('controller'=>'promotions','action'=>'index','plugin'=>false));?>
        </li>
        <li class="<?=($this->request->params['controller'] == 'TeamMembers' && $this->request->params['action'] == 'index') ? 'active' : ''?>">
            <?=$this->Html->link('Team Members',array('controller'=>'TeamMembers','action'=>'index','plugin'=>false));?>
        </li>
        <li class="<?=($this->request->params['controller'] == 'Slideshows') ? 'active' : ''?>">
            <?=$this->Html->link('Slideshow',array('controller'=>'Slideshows','action'=>'index','plugin'=>false));?>
        </li>
    </ul>
    <h4>Connections</h4>
    <ul class="nav nav-pills nav-stacked">
        <li class="<?=($this->request->params['controller'] == 'user_connects') ? 'active' : ''?>">
            <?=$this->Html->link('Manage Ticker',array('controller'=>'user_connects','action'=>'hashTags','plugin'=>false),array('class'=>'','style'=>'color:#'));?>
        </li>
        <li class="<?=($this->request->params['controller'] == 'UsersNewsCategories') ? 'active' : ''?>">
            <?=$this->Html->link('Reuters™ News',array('controller'=>'UsersNewsCategories','action'=>'edit','plugin'=>false));?>
        </li>
    </ul>

    <h4><i class="fa fa-cutlery"></i> Restaurant</h4>
    <ul class="nav nav-pills nav-stacked">
        <li class="<?=($this->request->params['controller'] == 'restaurant_periods') ? 'active' : ''?>">
            <a href="/restaurant_periods">Service Periods</a>
        </li>
        <li class="<?=($this->request->params['controller'] == 'restaurant_categories') ? 'active' : ''?>">
            <a href="/restaurant_categories">Categories</a>
        </li>
        <li class="<?=($this->request->params['controller'] == 'restaurant_menu_items') ? 'active' : ''?>">
            <a href="/restaurant_menu_items">Menu Items</a>
        </li>
    </ul>
</div>