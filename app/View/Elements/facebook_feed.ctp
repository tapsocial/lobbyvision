<?php //echo "<div class=\"fb-feed\">";

// set counter to 0, because we only want to display 10 posts
$i = 0;
if($facebookFeed['data'] && !empty($facebookFeed['data'])){
foreach($facebookFeed['data'] as $post) {

    if ($post['type'] == 'status' || $post['type'] == 'link' || $post['type'] == 'photo') {

        // open up an fb-update div
        echo "<span class=\"fb-update\">";

            // post the time
        if(isset($post['picture']))
            echo '<span class="feed-img fimage">'.$this->Html->image($post['picture'],array('class'=>'')).'
        '.$this->Html->image('ficon.png',array('class'=>'ficon')).'</span>';
        else
            echo '<span class="feed-img fimage"><img src="https://graph.facebook.com/'.$post['from']['id'].'/picture" class="">'.$this->Html->image('ficon.png',array('class'=>'ficon')).'</span>';

            // check if post type is a status
            if ($post['type'] == 'status') {
                echo "<small  class='date'>" . date("jS M, Y", (strtotime($post['created_time']))) . "</small>";
                 if (isset($post['story']) && empty($post['story']) === false) {
                    echo "<p>" . $post['story'] . "</p>";
                } elseif (empty($post['message']) === false) {
                    echo "<p>" . $post['message'] . "</p>";
                }
            }

            // check if post type is a link
            if ($post['type'] == 'link') {
                echo "<small class='date'>" . date("jS M, Y", (strtotime($post['created_time']))) . "</small>";
                echo "<p>" . $post['from']['name'] . "</p>";
                  if (isset($post['story']) && empty($post['story']) === false) {
                    echo "<p>" . $post['story'] . "</p>";
                } elseif (empty($post['message']) === false) {
                    echo "<p>" . $post['message'] . "</p>";
                }
               // echo "<p><a href=\"" . $post['link'] . "\" target=\"_blank\">" . $post['link'] . "</a></p>";
            }

            // check if post type is a photo
            if ($post['type'] == 'photo') {
                echo "<small class='date'>" . date("jS M, Y", (strtotime($post['created_time']))) . "</small>";
                if (empty($post['story']) === false) {
                    echo "<p>" . $post['story'] . "</p>";
                } elseif (empty($post['message']) === false) {
                    echo "<p>" . $post['message'] . "</p>";
                }
              //  echo "<p><a href=\"" . $post['link'] . "\" target=\"_blank\">View photo &rarr;</a></p>";
            }

        echo "</span>"; // close fb-update div

        $i++; // add 1 to the counter if our condition for $post['type'] is met
    }

    //  break out of the loop if counter has reached 10
    if ($i == 10) {
        break;
    }
} // end the foreach statement
}
//echo "</div>"; ?>