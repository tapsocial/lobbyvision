<?
/**
 * Displays the events
 *
 * We can set the description content as 25vh because we know the height and width to be always 2x2
 */

$selector = '#widget-id-' . $widget['WidgetInstance']['id'];
?>
<script type="text/javascript">
$(document).ready(function(){
    $('<?=$selector ?> .bxslider').bxSlider({
        mode:        'fade',
        auto:        true,
        infiniteLoop:true,
        controls:    false,
        pager:       false,
        pause:       15000,
        adaptiveHeight: true,
    });
});
</script>
<style>
    .event-block .bx-viewport { position: initial !important;}
    .event-block .description { max-height: 16vh;     overflow: hidden; }
</style>
<div class="block-content <?=(!$widget['WidgetInstance']['show_title']) ? 'vcenter' : ''?>">
    <ul class="bxslider list-unstyled">
    <? foreach ($widgetContent as $value) : ?>
        <li>
            <? if ($value['Event']['image']) :?>
                <div class="col-md-6" style="
                        background-image: url('/uploads/event/image/<?=$value['Event']['image'] ?>');
                        background-position: center center;
                        background-repeat: no-repeat;
                        background-size: cover;
                        height: 21.5vh;
                        ">
                </div>
            <? endif; ?>
            <div class="col-md-<?=($value['Event']['image']) ? 6 :  12?>">
                <? if ($widget['WidgetInstance']['show_title']) :?>
                    <h3><?php echo $value['Event']['event_name']; ?></h3>
                <? else :?>
                    <h2><?php echo $value['Event']['event_name']; ?></h2>
                <? endif ?>
                <p class="description"><?=$value['Event']['description']?></p>
                <br />
                <h5><?php
                  if($value['Event']['all_day'] && $this->Time->format( $value['Event']['start'],'%d-%m-%Y')==$this->Time->format($value['Event']['end'],'%d-%m-%Y')){
                     echo $this->Time->format('F d', $value['Event']['start']);
                  }elseif(!$value['Event']['all_day'] && $this->Time->format( $value['Event']['start'],'%d-%m-%Y')==$this->Time->format($value['Event']['end'],'%d-%m-%Y')){
                     echo $this->Time->format('F d/h:ia', $value['Event']['start']); ?> - <?php echo $this->Time->format('F d/h:i a', $value['Event']['end']); ?></small>
                     <?php
                  }elseif($value['Event']['all_day'] && $this->Time->format( $value['Event']['start'],'%d-%m-%Y')!=$this->Time->format($value['Event']['end'],'%d-%m-%Y')){
                     echo $this->Time->format('F d', $value['Event']['start']); ?> - <?php echo $this->Time->format('F d', $value['Event']['end']); ?></small>
                     <?php
                  }else{
                     echo $this->Time->format('M d/h:i a', $value['Event']['start']); ?> - <?php echo $this->Time->format('M d/h:i a', $value['Event']['end']); ?></small>
                <?php  }?>
                </h5>
            </div>
        </li>
    <? endforeach ?>
    </ul>
</div>