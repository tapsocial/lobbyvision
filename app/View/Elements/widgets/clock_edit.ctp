<div class="control-group">
    <?php echo $this->Form->label('WidgetInstanceClock.datetime_format', 'Display as', array('class' => 'control-label'));?>
    <div class="controls">
        <?=$this->Form->input('WidgetInstanceClock.datetime_format',array(
                'class'   => 'form-control',
                'options' => array(
                    WidgetInstanceClock::TIME_ONLY => WidgetInstanceClock::TIME_ONLY,
                    WidgetInstanceClock::DATE_ONLY => WidgetInstanceClock::DATE_ONLY,
                    WidgetInstanceClock::DATE_TIME => WidgetInstanceClock::DATE_TIME,
                ),
        ));?>
    </div><!-- .controls -->
</div><!-- .control-group -->