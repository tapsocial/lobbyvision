<style type="text/css">
.ticker {
    font-size: 5vh;
    margin: 2vh;
}

.ticker p {
    display: inline-block;
}

.ticker .fimage {
    padding-left: 2vw;
    padding-right: 1vw;
}
</style>
<div class="main-footer clearfix">
    <div class="twitter-feed-area clearfix row">
        <?php if (! empty($widgetContent)) : ?>
        <div class="twitter-scroll-area">
            <div class="ticker">
                <span class="ticker-holder">
                    <marquee scrollamount="2" scrolldelay="5">
                        <?php
                            if (! empty($widgetContent)) {
                                foreach ($widgetContent as $feed) {
                                    echo $feed['content'];
                                }
                            }
                            ?>
                    </marquee>
                </span>
            </div>
        </div>
        <?php endif ?>
    </div>
</div>