<?
/**
 * Displays the promotions
 *
 * We can set the description content as 25vh because we know the height and width to be always 2x2
 */

$selector = '#widget-id-' . $widget['WidgetInstance']['id'];
?>
<script type="text/javascript">
$(document).ready(function(){
    $('<?=$selector ?> .bxslider').bxSlider({
        mode:        'fade',
        auto:        true,
        infiniteLoop:true,
        controls:    false,
        pager:       false,
        pause:       15000,
        adaptiveHeight: true,
    });
});
</script>
<style>
    .promo-block .bx-viewport { position: initial !important;}
    .promo-block .description { max-height: 16vh;     overflow: hidden; }
</style>
<div class="block-content <?=(!$widget['WidgetInstance']['show_title']) ? 'vcenter' : ''?>">
    <ul class="bxslider list-unstyled">
    <? foreach ($widgetContent as $value) : ?>
        <li>
            <? if ($value['Promotion']['image']) :?>
                <div class="col-md-6" style="
                        background-image: url('/uploads/promotion/image/<?=$value['Promotion']['image'] ?>');
                        background-position: center center;
                        background-repeat: no-repeat;
                        background-size: cover;
                        height: 21.5vh;
                        ">
                </div>
            <? endif; ?>
            <div class="col-md-<?=($value['Promotion']['image']) ? 6 :  12?>">
                <? if ($widget['WidgetInstance']['show_title']) :?>
                    <h3><?php echo $value['Promotion']['name']; ?></h3>
                <? else :?>
                    <h2><?php echo $value['Promotion']['name']; ?></h2>
                <? endif ?>
                <p class="description"><?=$value['Promotion']['description']?></p>
                <br />
                <h5><?php
                  if($value['Promotion']['all_day'] && $this->Time->format( $value['Promotion']['start'],'%d-%m-%Y')==$this->Time->format($value['Promotion']['end'],'%d-%m-%Y')){
                     echo $this->Time->format('F d', $value['Promotion']['start']);
                  }elseif(!$value['Promotion']['all_day'] && $this->Time->format( $value['Promotion']['start'],'%d-%m-%Y')==$this->Time->format($value['Promotion']['end'],'%d-%m-%Y')){
                     echo $this->Time->format('F d/h:i a', $value['Promotion']['start']); ?> - <?php echo $this->Time->format('F d/h:i a', $value['Promotion']['end']); ?></small>
                     <?php
                  }elseif($value['Promotion']['all_day'] && $this->Time->format( $value['Promotion']['start'],'%d-%m-%Y')!=$this->Time->format($value['Promotion']['end'],'%d-%m-%Y')){
                     echo $this->Time->format('F d', $value['Promotion']['start']); ?> - <?php echo $this->Time->format('F d', $value['Promotion']['end']); ?></small>
                     <?php
                  }else{
                     echo $this->Time->format('M d/h:i a', $value['Promotion']['start']); ?> - <?php echo $this->Time->format('M d/h:i a', $value['Promotion']['end']);
                     } ?>
               </h5>
            </div>
        </li>
    <? endforeach ?>
    </ul>
</div>