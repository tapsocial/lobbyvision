
<script type="text/javascript">
$(function() {
    $( "#WidgetInstanceWeatherLocation" ).blur(function(){
        //alert($("#WidgetInstanceWeatherLocation").val());
        if($("#WidgetInstanceWeatherLocation").val()=='' || $("#WidgetInstanceWeatherLocation").val()==''){
            $("#loader").hide();
            $("#WidgetInstanceWeatherLocation").val('');
            $("#WidgetInstanceWeatherLocation").focus();
        }

    })

    $( "#WidgetInstanceWeatherLocation" ).autocomplete({
         search: function( event, ui ) {
            $("#loader").show();
        },
        source: "<?php echo Router::url('/');?>WeatherUndergrounds/getAddress",
        minLength: 2,
        /* snip */
        select: function(event, ui) {
            event.preventDefault();
            $("#WidgetInstanceWeatherGeocode").val(ui.item.latlon);
            $("#loader").hide();
        },
        focus: function(event, ui) {
            event.preventDefault();
            $("#WidgetInstanceWeatherLocation").val(ui.item.label);
        },
        response: function(event, ui) {
            $("#loader").hide();
            // ui.content is the array that's about to be sent to the response callback.
            if (ui.content.length === 0) {
                 $("#WidgetInstanceWeatherLocation").val('');
                $(".msg").html("No results found");
            } else {
                $(".msg").html("result found.");
               // $(".msg").empty();
            }
        }
    });
});
</script>

<div class="control-group">
    <?php echo $this->Form->label('WidgetInstanceWeather.location', 'Location', array('class' => 'control-label'));?>
    <div class="controls">
        <?php echo $this->Form->input('WidgetInstanceWeather.location', array(
                'class' => 'form-control',
                'after' => $this->Html->image('ajaxloader.gif', array('id' => 'loader')) .
                     '<br/> <br/> <span class="msg">Find and select your location from the list of cities as you type</span>'
        )); ?>
    </div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
    <?php echo $this->Form->label('WidgetInstanceWeather.geocode', 'Geocode', array('class' => 'control-label'));?>
    <div class="controls">
        <?php echo $this->Form->input('WidgetInstanceWeather.geocode', ['class' => 'form-control']); ?>
    </div><!-- .controls -->
</div><!-- .control-group -->