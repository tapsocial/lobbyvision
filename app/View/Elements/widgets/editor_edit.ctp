<?php echo $this->Html->script('ckeditor/ckeditor'); ?>
<div class="control-group">
    <?php echo $this->Form->label('content', 'Content', array('class' => 'control-label'));?>
    <div class="controls">
        <?php echo $this->Form->input('WidgetInstanceEditor.content', array('class' => 'span12 ckeditor')); ?>
    </div><!-- .controls -->
</div><!-- .control-group -->