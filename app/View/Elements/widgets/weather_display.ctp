<?
$now   = $widgetContent['current']->current_observation->temp_f;
$today = $widgetContent['forecast']->forecast->simpleforecast->forecastday[0];
$low   = $today->low->fahrenheit;
$high  = $today->high->fahrenheit;
$title = $widget['WidgetInstance']['show_title'];

unset($widgetContent['forecast']->forecast->simpleforecast->forecastday[0]);
?>
<style>
         .forecast img.img-responsive {display: inline-block; height: 5vh;}
.noTitle .forecast img.img-responsive {display: inline-block; height: 7vh;}
</style>
<div class="block-content <?=(!$widget['WidgetInstance']['show_title']) ? 'vcenter' : ''?>">
    <div class="row">
        <div class="col-md-5 text-center">
            <? if ($title) :?>
                <h2><?=$now; ?>°</h2>
                <h4><?=$low?>° / <?=$high?>°</h4>
            <? else : ?>
                <h1><?=$now; ?>°</h1>
                <h3><?=$low?>° / <?=$high?>°</h3>
            <? endif ?>
        </div>
        <div class="col-md-3">
            <img class="img-responsive" style="position: relative; top: -0.5vh;"
            src="<?=WidgetInstanceWeather::icon_url($design['Design']['shape'], $widgetContent['current']->current_observation->icon_url);?>" />
        </div>

        <div class="col-md-4">
            <?=(!$title) ? '<br />' : ''?>
            <h3><?=$widgetContent['current']->current_observation->weather;?></h3>
        </div>
    </div>
    <? if ($widget['WidgetInstance']['size_y'] > 1) :?>
    <hr />
    <div class="row forecast">
        <? foreach ($widgetContent['forecast']->forecast->simpleforecast->forecastday as $key  => $forecast) : ?>
            <div class="col-md-3 text-center">
                <h3><?=$forecast->date->weekday_short ?></h3>
                <img  class="img-responsive"  src="<?=WidgetInstanceWeather::icon_url($design['Design']['shape'], $forecast->icon_url);?>" />
                <? if ($title) :?>
                    <h5><?=$forecast->high->fahrenheit; ?>° / <?=$forecast->low->fahrenheit ?>° </h5>
                <? else : ?>
                    <h4><?=$forecast->high->fahrenheit; ?>° / <?=$forecast->low->fahrenheit ?>° </h4>
                <? endif ?>
            </div>
        <? if ($key > 3 ) break; endforeach ?>
    </div>
    <? endif ?>
</div>