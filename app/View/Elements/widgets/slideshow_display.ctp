<?php
$selector = '#widget-id-'.$widget['WidgetInstance']['id'];
?>
<script type="text/javascript">
 $(document).ready(function(){
    $('<?=$selector ?> .bxslider').bxSlider({
        mode: 'fade',
        auto:true,
        infiniteLoop: true,
        hideControlOnEnd: true,
        controls: false,
        pager: false,
        pause: 8000
    });
});
</script>
<style>
.slideshow-block .noTitle {padding: 0;}
</style>
<div class="block-content bxSliderFull">
    <ul class="bxslider list-unstyled">
        <?php foreach ($widgetContent as $key => $slideshow) :?>
        <li class="backgroundFull" style="background-image: url('../uploads/slideshow/image/<?=$slideshow['Slideshow']['image']?>');"></li>
        <?php endforeach ?>
    </ul>
</div>