<?
$selector = '#widget-id-'.$widget['WidgetInstance']['id'].' .block-content.news';
?>
<script>
$(function() {
    $('<?=$selector?>').simplyScroll({
        auto: true,
        orientation: 'vertical',
        frameRate: 12,
        pauseOnHover: false,
        pauseOnTouch: false,
    });
});
</script>
<style>
.news article {margin-bottom: 4vh;}
.simply-scroll  {
    width: 100%;
    height: 100%;
}
.simply-scroll .simply-scroll-clip {
    width: 100%;
    height: 100%;
}
</style>
<div class="block-content news">
    <? foreach($widgetContent as $key  => $new) :?>
        <article>
            <div class="pull-right"><?=$new['News']['news']['pubDate']?></div>
            <h3><?=$new['News']['news']['title']?></h3>
            <p><?=strip_tags($new['News']['news']['description'])?></p>
        </article>
    <? endforeach;?>
</div>