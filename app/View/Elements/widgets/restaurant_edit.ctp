<?
$subWidget  = new WidgetInstanceRestaurant();
$periods    = $subWidget->getPossibleCategoriesGroupByPeriod($this->request->data);
?>
<p>
    Content in this panel will display based upon the service periods that you
    have defined. Select the content you wish to display in this panel for each
    service period.
</p>
<div class="control-group">
    <?php echo $this->Form->label('WidgetInstanceClock.datetime_format', 'Category to display in this panel', array('class' => 'control-label'));?>
    <div class="controls">
        <? foreach ($periods as $key => $period) : ?>
            <? if (!$period['RestaurantCategory'])  continue;?>
            <h5><?=$period['RestaurantPeriod']['name'] ?></h5>
            <?=$this->Form->input('WidgetInstanceRestaurant.WidgetInstanceRestaurantCategory.'. $key.'.widget_instance_restaurant_id', [
                'type'  => 'hidden',
                'value' => $this->request->data['WidgetInstanceRestaurant']['id'],
            ])?>
            <?=$this->Form->input('WidgetInstanceRestaurant.WidgetInstanceRestaurantCategory.'. $key.'.restaurant_category_id',array(
                'class'   => 'form-control',
                'type'    => 'radio',
                'options' => $period['RestaurantCategory'],
                'label'   => null,
                'legend' => false,
                'value' => $period['RestaurantSelectedCategory'],
            ));?>
        <? endforeach ?>
    </div><!-- .controls -->
</div><!-- .control-group -->