<?
$widgetWidth = $widget['WidgetInstance']['size_x'];
?>
<style>
ul.list-full {
    display: table; table-layout: fixed; width: 100%;
}
ul.list-full li {
    display: table-cell;
    width: auto;
}
.restaurant-block .dl-horizontal dt { width:       7vw; font-weight: normal;}
.restaurant-block .dl-horizontal dd { margin-left: 8vw;}
</style>
<div class="block-content no-broken-lines">
    <? foreach($widgetContent as $item) :?>
        <div class="row">
        <? if ($widgetWidth < 2) :?>
            <div class="col-md-12">
                <? if ($item['RestaurantMenuItem']['picture']) :?>
                    <div class="row">
                        <div class="col-md-4">
                            <?=$this->Html->image('../uploads/restaurant_menu_item/picture/'.$item['RestaurantMenuItem']['picture'], array(
                                'class' => 'img-responsive'
                            )); ?>
                        </div>
                        <div class="col-md-8">
                            <h3><?=$item['RestaurantMenuItem']['name']?></h3>
                            <p><?=$item['RestaurantMenuItem']['description']?></p>
                        </div>
                    </div>
                <? else: ?>
                    <h3><?=$item['RestaurantMenuItem']['name']?></h3>
                    <p><?=$item['RestaurantMenuItem']['description']?></p>
                <? endif ?>

                <? if (count($item['RestaurantMenuItemPrice']) < 2) :?>
                    <div class="text-right">
                        <strong> $<?=number_format($item['RestaurantMenuItemPrice'][0]['price'], 2) ?> </strong>
                    </div>
                <? else : ?>
                    <ul  class="list-inline list-full">
                        <? foreach($item['RestaurantMenuItemPrice'] as $price) :?>
                            <li>
                                <?=$price['name'] ?>
                                <br />
                                <strong> $<?=number_format($price['price'], 2) ?> </strong>
                            </li>
                        <?  endforeach;?>
                    </ul>
                <? endif; ?>
            </div>
        <? else :?>
            <div class="col-md-9">
                <? if ($item['RestaurantMenuItem']['picture']) :?>
                    <div class="row">
                        <div class="col-md-4">
                            <?=$this->Html->image('../uploads/restaurant_menu_item/picture/'.$item['RestaurantMenuItem']['picture'], array(
                                'class' => 'img-responsive'
                            )); ?>
                        </div>
                        <div class="col-md-8">
                            <h3><?=$item['RestaurantMenuItem']['name']?></h3>
                            <p><?=$item['RestaurantMenuItem']['description']?></p>
                        </div>
                    </div>
                <? else: ?>
                    <h3><?=$item['RestaurantMenuItem']['name']?></h3>
                    <p><?=$item['RestaurantMenuItem']['description']?></p>
                <? endif ?>

            </div>
            <div class="col-md-3">
                <dl class="dl-horizontal">
                <? foreach($item['RestaurantMenuItemPrice'] as $price) :?>
                    <dt> <?=$price['name']?></dt>
                    <dd> <strong> $<?=number_format($price['price'], 2) ?> </strong> </dd>
                <?  endforeach;?>
                </dl>
            </div>
        <? endif ?>
        </div>
        <hr />
    <? endforeach;?>
</div>
