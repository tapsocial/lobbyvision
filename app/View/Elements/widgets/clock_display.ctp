<?
switch ($widget['WidgetInstanceClock']['datetime_format']) {
    case WidgetInstanceClock::DATE_TIME:
        $format = '<h2>%I:%M%P</h2> <h4>%B %d, %Y</h4>';
        break;
    case WidgetInstanceClock::DATE_ONLY:
        $format = '<h3>%B %d, %Y</h3>';
        break;
    case WidgetInstanceClock::TIME_ONLY:
    default:
        $format = '<h1>%I:%M%P</h1>';
        break;
}
$jqueryContainer = "#widget-id-{$widget['WidgetInstance']['id']} .time-cont";
?>
<div class="block-content <?=(!$widget['WidgetInstance']['show_title']) ? 'vcenter' : ''?>">
    <div class="time-cont text-center"></div>
</div>
<script type="text/javascript">
$(function() {
        if ($('<?=$jqueryContainer?> .time').length > 0) {
            $('<?=$jqueryContainer?> .time').remove();
        };
        $('<?=$jqueryContainer?>').append('<div class="time"></div>');
        $('<?=$jqueryContainer?> .time').jclock({
            format:       '<?=$format ?>',
            timeNotation: '12h',
            am_pm:        true,
            utc:          false,
            utc_offset:   -6
        });
});
</script>
