<?
$selector = '#widget-id-'.$widget['WidgetInstance']['id'];
$bottom   = $widgetContent;
$tail     = array_shift($bottom);
$bottom[] = $tail;
$speed    = 15000;
?>
<script type="text/javascript">
$(document).ready(function(){
    $('<?=$selector?> .teamMain').bxSlider({
        auto:true,
        infiniteLoop: true,
        hideControlOnEnd: true,
        controls: false,
        pager:    false,
        pause:    <?=$speed; ?>,
    });
    $('<?=$selector?> .teamSlider').bxSlider({
        auto:true,
        infiniteLoop: true,
        hideControlOnEnd: true,
        controls: false,
        pager:    false,
        pause:    <?=$speed; ?>,
        minSlides: 4,
        maxSlides: 4,
        moveSlides: 1,
        slideWidth: '250',
    });
});

</script>
<style>
    .main .roundPortrait{
        height: 11vh;
        width: 11vh;
        margin: 0 auto;
    }
    .roundPortrait {
        background-repeat: no-repeat;
        background-position: center center;
        background-size: cover;
    }
    .main .name        {white-space: nowrap; overflow: hidden; }
    .main .description {height: 5vh; overflow: hidden;}
</style>

<div class="block-content <?=(!$widget['WidgetInstance']['show_title']) ? 'vcenter' : ''?>">
    <ul class="bxslider list-unstyled teamMain">
    <? foreach ($widgetContent as $teamMember) :
            $background = '/img/tp-small.png';
            if ($teamMember['TeamMember']['image']) {
                $background = "/uploads/team_member/image/{$teamMember['TeamMember']['image']}";
            }
    ?>
        <li>
            <div>
                <div class="row main">
                    <div class="col-md-2 text-center">
                        <div class="img-responsive img-circle roundPortrait" style="background-image: url('<?=$background ?>'); ">
                        </div>
                    </div>
                    <div class="col-md-10">
                        <h2 class="name"><?=$teamMember['TeamMember']['first_name'].' '.$teamMember['TeamMember']['last_name'];?></h2>
                        <h4><?=$teamMember['TeamMember']['title'];?></h4>
                        <p class="description"><?=$teamMember['TeamMember']['description'];?></p>
                    </div>
                </div>
           </div>
        </li>
    <? endforeach ?>
    </ul>
    <ul class="bxslider list-unstyled teamSlider" style="width: 100%;">
        <? foreach ($bottom as $teamMember) :
            $background = '/img/tp-small.png';
            if ($teamMember['TeamMember']['image']) {
                $background = "/uploads/team_member/image/{$teamMember['TeamMember']['image']}";
            }?>
            <li class="text-center">
                <div class="img-responsive img-circle roundPortrait" style="
                    width: 4vh;
                    height: 4vh;
                    margin: 0.5vh auto;
                    background-image: url('<?=$background?>'); ">
                </div>
                <h4><?=$teamMember['TeamMember']['first_name'].' '.$teamMember['TeamMember']['last_name'];?></h4>
                <p><?=$teamMember['TeamMember']['title'];?></p>
            </li>
        <? endforeach; ?>
    </ul>
</div>