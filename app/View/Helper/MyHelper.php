<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Helper
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('ClassRegistry', 'Utility');
App::uses('AppHelper', 'View/Helper');
App::uses('Hash', 'Utility');

/**
 * Form helper library.
 *
 * Automatic generation of HTML FORMs from given data.
 *
 * @package       Cake.View.Helper
 * @property      HtmlHelper $Html
 * @link http://book.cakephp.org/2.0/en/core-libraries/helpers/form.html
 */
class CustomHelper extends AppHelper {

/**
 * Other helpers used by FormHelper
 *
 * @var array
 */
    public $helpers = array('Html');

/**
 * Options used by DateTime fields
 *
 * @var array
 */
    protected $_options = array(
        'day' => array(), 'minute' => array(), 'hour' => array(),
        'month' => array(), 'year' => array(), 'meridian' => array()
    );

/**
 * List of fields created, used with secure forms.
 *
 * @var array
 */
    public $fields = array();

/**
 * Constant used internally to skip the securing process,
 * and neither add the field to the hash or to the unlocked fields.
 *
 * @var string
 */
    const SECURE_SKIP = 'skip';

/**
 * Defines the type of form being created. Set by FormHelper::create().
 *
 * @var string
 */
    public $requestType = null;

/**
 * The default model being used for the current form.
 *
 * @var string
 */
    public $defaultModel = null;

/**
 * Persistent default options used by input(). Set by FormHelper::create().
 *
 * @var array
 */
    protected $_inputDefaults = array();

/**
 * An array of field names that have been excluded from
 * the Token hash used by SecurityComponent's validatePost method
 *
 * @see FormHelper::_secure()
 * @see SecurityComponent::validatePost()
 * @var array
 */
    protected $_unlockedFields = array();

/**
 * Holds the model references already loaded by this helper
 * product of trying to inspect them out of field names
 *
 * @var array
 */
    protected $_models = array();

/**
 * Holds all the validation errors for models loaded and inspected
 * it can also be set manually to be able to display custom error messages
 * in the any of the input fields generated by this helper
 *
 * @var array
 */
    public $validationErrors = array();

    /**
     * Copies the validationErrors variable from the View object into this instance
     *
     * @param View $View The View this helper is being attached to.
     * @param array $settings Configuration settings for the helper.
     */
    public function __construct(View $View, $settings = array()) {
        parent::__construct($View, $settings);
        $this->validationErrors =& $View->validationErrors;
    }

    public function getImage($path, $options = array(),$extra=array('model'=>'','field'=>'image','thumb'=>'')) {
        if(!empty($extra)){
            $path='../uploads/'.$extra['model'].'/'.$extra['field'].'/'.$extra['thumb'].'/'.$path;
        }
        return $this->image($path, $options = array());
    }
}