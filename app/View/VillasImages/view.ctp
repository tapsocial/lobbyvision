<div class="villasImages view">
<h2><?php echo __('Villas Image'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($villasImage['VillasImage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Villa'); ?></dt>
		<dd>
			<?php echo $this->Html->link($villasImage['Villa']['name'], array('controller' => 'villas', 'action' => 'view', $villasImage['Villa']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Path'); ?></dt>
		<dd>
			<?php echo h($villasImage['VillasImage']['path']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($villasImage['VillasImage']['title']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Villas Image'), array('action' => 'edit', $villasImage['VillasImage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Villas Image'), array('action' => 'delete', $villasImage['VillasImage']['id']), null, __('Are you sure you want to delete # %s?', $villasImage['VillasImage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Villas Images'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Villas Image'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Villas'), array('controller' => 'villas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Villa'), array('controller' => 'villas', 'action' => 'add')); ?> </li>
	</ul>
</div>
