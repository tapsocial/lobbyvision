<div class="villasImages form">
<?php echo $this->Form->create('VillasImage'); ?>
	<fieldset>
		<legend><?php echo __('Edit Villas Image'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('villa_id');
		echo $this->Form->input('path');
		echo $this->Form->input('title');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('VillasImage.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('VillasImage.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Villas Images'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Villas'), array('controller' => 'villas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Villa'), array('controller' => 'villas', 'action' => 'add')); ?> </li>
	</ul>
</div>
