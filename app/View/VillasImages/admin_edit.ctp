<div class="villasImages form">

 <?php echo $this->element('villa-tabs',array('villa_id'=>$id)); ?>


<?php echo $this->Form->create('VillasImage',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Manage Image'); ?></legend>

		<?php if(!empty($VillasImages)){?>
<div class="row-fluid">
<ul class="thumbnails">
<?php foreach($VillasImages as $VillasImage){ ?>
    <li class="thumbnail">
    <?php echo $this->Html->image('../uploads/villas_image/path/thumb/small/'.$VillasImage['VillasImage']['path'],array('class'=>''));?>
  </li>
  <?php } ?>
</ul>
</div>
<?php	} ?>


	<?php
		//echo $this->Form->input('id');		
		echo $this->Form->input('path',array('type' => 'file'));
		echo $this->Form->input('title',array('class'=>'span3'));

    echo $this->Form->input('cover', array('label' => 'Make feature Image'));
	?>

	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		
		<li><?php echo $this->Html->link(__('List Villas'), array('controller' => 'villas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Villa'), array('controller' => 'villas', 'action' => 'add')); ?> </li>
	</ul>
</div>
