<div class="villasImages index">
	<h2><?php echo __('Villas Images'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('villa_id'); ?></th>
			<th><?php echo $this->Paginator->sort('path'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($villasImages as $villasImage): ?>
	<tr>
		<td><?php echo h($villasImage['VillasImage']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($villasImage['Villa']['name'], array('controller' => 'villas', 'action' => 'view', $villasImage['Villa']['id'])); ?>
		</td>
		<td><?php echo h($villasImage['VillasImage']['path']); ?>&nbsp;</td>
		<td><?php echo h($villasImage['VillasImage']['title']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $villasImage['VillasImage']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $villasImage['VillasImage']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $villasImage['VillasImage']['id']), null, __('Are you sure you want to delete # %s?', $villasImage['VillasImage']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Villas Image'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Villas'), array('controller' => 'villas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Villa'), array('controller' => 'villas', 'action' => 'add')); ?> </li>
	</ul>
</div>
