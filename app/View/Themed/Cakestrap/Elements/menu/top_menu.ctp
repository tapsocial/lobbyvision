<?
/**
 * Top menu
 *
 * @property $Menu MenuHelper
 */
/* @var $this View */
?>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
<!--			<a class="brand" target="_blank" href="<?php echo  $this->request->webroot; ?>admin/agents"><?php echo __('TapSocial'); ?></a>
-->			<div class="nav-collapse">
                <ul class="nav">
                    <?php if($authUser['group_id']==1) {
                        $this->Menu->display('adminmenu',array('no_ul'));
                    } elseif($authUser['group_id']==2) {
                        $this->Menu->display('agent',array('no_ul'));
                    } ?>
                <?php //Fill me with your sweet, sweet menu items. ?>

                </ul>
                <ul class="nav pull-right">
                    <li  class=""><?php echo $this->Html->link('Logout',array('controller'=>'users','action'=>'logout','admin'=>false,'plugin'=>false))?></li>
                    </ul>
            </div>
        </div>
    </div>
</div>