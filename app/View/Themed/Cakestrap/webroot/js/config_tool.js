function saveWidget(){
$('#loader small').html('Saving..').show();

    $.ajax({
    type: "POST",
    url: base_url+"UserLayouts/edit",
    data: $('#UserLayoutIndexForm').serialize(),
    })
    .done(function( msg ) {
        $('#loader small').html('Saved!!').hide('slow');
    //alert( "Data Saved: " + msg );
    });
}

$(document).ready(function(){
    $("#menu_link").click(function() {
if($('#sidebar').hasClass('hide')){
    $('#sidebar').removeClass('hide').addClass('show');
    $(this).text('Close Menu');
}
else{
    $('#sidebar').removeClass('show').addClass('hide');
    $(this).text('Open Menu');
}

return false;
})