/*
* Twitter Bootstrappifier for CakePHP 
*
* Author: Mutlu Tevfik Kocak
*
* CakePHP Twitter Bootstrappifier
*
* Selects all con twitter Bootstrap incompatible form and buttons,
* and converts them into pretty Twitter Bootstrap style.
*
*/

var Bootstrappifier = {
	straps		: {
		cake	: function() {
			//All submit forms wrapped to div.action
			$('div.submit').addClass('form-actions text-center');
			//All submit forms converted to primary button
			$('.form-actions input[type="submit"]').addClass('btn btn-info btn-large');
			//All index actions converted into pretty buttons
			$('td[class="actions"] > a[class!="btn"]').addClass('btn');
			$('.index table').addClass('table table-bordered');
			//$('form div.required input').addAttr('required','required');
			$('div.actions ul').addClass('nav-list'); 
			$('ul.nav-list li a').addClass('btn');
if(($('.actions').size()==0 && $('#sidebar').size()==0 ) || $('#sidebar').hasClass('overlay')){

	$('div.form, div.index, div.view').css('width','98%');
}
			$('.control-group iframe.wysihtml5-sandbox').wrap('<div class="input controls" />');
			//All (div.inputs) with default FormHelper style (div.input > label ~ input)
			//converted into Twitter Bootstrap Style (div.clearfix > label ~ div.input)
		//$('#content div.form').addClass('well'); 	
	//$('#content div.form form').addClass('form-horizontal');
	
	//$('div.checkbox').removeClass().addClass('controls checkwrap');//
	//checkright=$('.checkwrap').html().wrap('<label class="checkbox" />');
	
		//$('div.checkbox').removeClass().addClass('checkboxwrap');
			//$('div.checkboxwrap > label ~ input').wrap('<label class="checkbox" />');
	
			//$('div[class!="input added"].input').removeClass().addClass('control-group');
			//$('div.control-group label').addClass('control-label');
			//$('div.control-group > label ~ input').wrap('<div class="input controls" />');
			//$('div.control-group > label ~ select').wrap('<div class="input controls" />');
			//$('div.control-group > label ~ textarea').wrap('<div class="input controls" />');
			
				
		},
		error	: function() {
			$('.message').addClass('alert alert-info');
			$('.flash_success').addClass('alert alert-success');
			$('.flash_warning').addClass('alert');
			$('.error-message').addClass('alert alert-error');
			//$('div.error-message').append($('div.error-message').replaceWith('<span class="help-inline">'+$('div.error-message').text()+'</span'));
			$('.form-error').addClass('error');
			$('.form-error').closest('.clearfix').addClass('error');
		}
	},
	getStraps	: function() {
		var strapsList = new Array();
		$.each(this.straps, function(key, name) {
			strapsList.push(key);
		});
		strapsList = strapsList.join(',');
		return typeof(strapsList == 'array') ? strapsList : new Array();
	},
	load		: function(straps) {
		straps = typeof(straps !== 'string') ? this.getStraps() : straps;
		return $.each(straps.split(','), function(key, value) {
			if(typeof(Bootstrappifier.straps[value] === 'function')) {
				var strapFunction = Bootstrappifier.straps[value];
				strapFunction();
			}
		}) ? true : false;
	},
}
//Styling start when document loads
$(document).ready(function(){
	Bootstrappifier.load();
});