﻿<?php
$cakeDescription = __d('cake_dev', 'TapSocial');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $title_for_layout; ?>
        </title>
        <?php
            echo $this->Html->meta('icon');
            echo $this->fetch('meta');
            echo $this->Html->css('bootstrap.min');
            echo $this->Html->css('bootstrap-responsive.min');
            echo $this->Html->css('core');
            echo $this->Html->css('cake.generic');
            echo $this->fetch('css');
            echo $this->Html->script('libs/jquery-1.10.2.min');
        ?>
            <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <?php
            echo $this->Html->script('cakebootstrap');
            echo $this->Html->script('libs/bootstrap.min');
            echo $this->fetch('script');
        ?>
    </head>
    <body>
        <div id="main-container">
            <div id="header" class="container-fluid" style="margin-top:0px">
            <?php echo $this->Html->link($this->Html->image('TapSocial_top.png',array('style'=>'height:84px; margin-bottom:10px')),'http://www.tapsocial.net',array('escape'=>false));?>
            <?php if($authUser){?>
                <div class="navbar-fixed-top-">
                <?php echo $this->element('menu/top_menu'); ?>
            </div>
            <?php }?>
            </div><!-- #header .container -->
            <div id="content" class="container-fluid">
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>
            </div><!-- #header .container -->

            <div id="footer" class="container-fluid">
                <?php //Silence is golden ?>
            </div><!-- #footer .container -->
        </div><!-- #main-container -->
    </body>
</html>