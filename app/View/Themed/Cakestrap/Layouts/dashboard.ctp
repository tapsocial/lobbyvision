<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'TapSocial');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $title_for_layout; ?>
        </title>
        <?php
            echo $this->Html->meta('icon');

            echo $this->fetch('meta');

            echo $this->Html->css('cake.generic');
            echo $this->Html->css('bootstrap.min');
            echo $this->Html->css('bootstrap-responsive.min');
            echo $this->Html->css('core');
            echo $this->Html->css('dashboard');
            echo $this->fetch('css');

            echo $this->Html->script('libs/jquery-1.10.2.min');?>
            <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
            <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
            <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css" />
            <?php
            echo $this->Html->script('cakebootstrap');
            echo $this->Html->script('libs/bootstrap.min');
            echo $this->fetch('script');
        ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    </head>

    <body>

        <div id="main-container">

            <div id="header" class="container-fluid" style="margin-top:0px">
            <?php echo $this->Html->link($this->Html->image('TapSocial_top.png',array('style'=>'height:84px; margin-bottom:10px')),'/',array('escape'=>false));?>
            <?php if($authUser){?>
                <!-- <div class="navbar-fixed-top-">
                <?php //echo $this->element('menu/top_menu'); ?>
            </div> -->
            <?php }?>
            <div class="span5 pull-right">
                <div class="btn-group pull-right">
                    <?=$this->Html->link('<i class="fa fa-sign-out"></i> Logout',
                            array('controller'=>'users','action'=>'logout?ver='.rand(100,999999)), array('class'=>'btn btn-danger', 'escape' => false)); ?>
                    <?=$this->Html->link('<i class="fa fa-user"></i> My Account',
                            array('controller'=>'users','action'=>'View'), array('class'=>'btn btn-default', 'escape' => false)); ?>
                    <a class="btn btn-info" href="http://tapsocial.net/digital-signage-support-faqs/" target="_blank">
                        <i class="fa fa-question"></i> Help
                    </a>
                    <a class="btn btn-success" target="_blank" href="<?php echo Router::url('/');?>display">
                        <i class="fa fa-television"></i> Launch your Digital Sign!
                    </a>
                </div>
            </div>
            </div><!-- #header .container -->

            <div id="content" class="container-fluid">
                <?php echo $this->Session->flash(); ?>
                <section id="dashboard" class="row-fluid">
                    <div class="span2">
                        <?php echo $this->Element('sidebar',array('cache'=>false));?>
                    </div>
                    <div class="span10">
                        <?php echo $this->fetch('content'); ?>
                    </div>
                </section>
            </div><!-- #header .container -->

            <div id="footer" class="container">
                <?php //Silence is golden ?>
            </div><!-- #footer .container -->

        </div><!-- #main-container -->

        <? if ($this->element('sql_dump')) :?>
        <div class="container">
            <div class="well">
                <small>
                    <?php echo $this->element('sql_dump'); ?>
                </small>
            </div>
        </div><!-- .container -->
        <? endif; ?>
        <?php echo $this->Facebook->init(); ?>
    </body>
</html>
