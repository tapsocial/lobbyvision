﻿<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'TapSocial');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $title_for_layout; ?>
        </title>
        <?php
            echo $this->Html->meta('icon');

            echo $this->fetch('meta');

            echo $this->Html->css('bootstrap.min');
            echo $this->Html->css('bootstrap-responsive.min');
            echo $this->Html->css('core');
            echo $this->Html->css('cake.generic');
            echo $this->Html->css('dashboard');
            echo $this->Html->css('config_tool');
            echo $this->Html->css('bootstrap-select');
            echo $this->fetch('css');

            echo $this->Html->script('libs/jquery-1.10.2.min');?>
            <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
            <?php
            echo $this->Html->script('cakebootstrap');
            echo $this->Html->script('libs/bootstrap.min');
            //echo $this->Html->script('bootstrap.file-input');
            echo $this->Html->script('libs/ui/jquery.ui.position');
            echo $this->Html->script('libs/ui/jquery.ui.core');
            echo $this->Html->script('libs/ui/jquery.ui.widget');
            echo $this->Html->script('libs/ui/jquery.ui.mouse');
            echo $this->Html->script('libs/ui/jquery.ui.draggable');
            echo $this->Html->script('libs/ui/jquery.ui.droppable');
            echo $this->Html->script('libs/ui/jquery.ui.resizable');
            echo $this->Html->script('libs/ui/jquery.ui.dialog');
            echo $this->Html->script('config_tool');
            echo $this->Html->script('jquery.ddslick.min');
            echo $this->Html->script('bootstrap-select');




            echo $this->fetch('script');
        ?>



        <script>
        var base_url='<?php echo Router::url('/');?>';
    $(function() {
        // there's the gallery and the trash
        var $gallery = $( "#gallery" ),
            $trash = $( "#trash #box .droppable" );

        // let the gallery items be draggable
        $( ".ui-widget-content", $gallery ).draggable({
            cancel: "a.ui-icon, .w-select", // clicking an icon won't initiate dragging
            revert: "invalid", // when not dropped, the item will revert back to its initial position
            containment: "document",
            helper: "clone",
            cursor: "move"
        });

        // let the gallery items be draggable
        $( ".body-element", $trash ).draggable({
            cancel: "a.ui-icon, .w-select", // clicking an icon won't initiate dragging
            revert: "invalid", // when not dropped, the item will revert back to its initial position
            containment: "document",
            helper: "clone",
            cursor: "move"
        });

        $('.navbar .droppable').droppable({
            accept: ".header-element",
            //activeClass: "ui-state-highlight",
            drop: function( event, ui ) {
                $( this )
                    .addClass( "-ui-state-highlight" )
                    .find( "small" )
                        .hide();
                deleteImage( ui.draggable,$( this ) );
            }
        });

        $('#ticker').droppable({
            accept: ".ticker-element",
            //activeClass: "ui-state-highlight",
            drop: function( event, ui ) {
                $( this )
                    .addClass( "-ui-state-highlight" )
                    .find( "small" )
                        .hide();
                deleteImage( ui.draggable,$( this ) );
            }
        });

        // let the trash be droppable, accepting the gallery items
        $trash.droppable({
            accept: "#gallery > .body-element,#trash .body-element",
            //activeClass: "ui-state-highlight",
            drop: function( event, ui ) {
                //alert('dd');
                var old_model=$(this).find('input.model').val();

        ui.draggable.closest('.box').children('input.model,input.element').val('');
        //cleanPrevious(old_model);

if(old_model!=''){
    recycleImage($('#'+old_model));

            }

$(this).find('input.model').val(ui.draggable.attr('id'));
//$(this).find('input.label').val(ui.draggable.attr('label'));
$(this).find('input.element').val(ui.draggable.attr('element'));
//$(this).find('input.number_of_display').val(ui.draggable..find('w-select select').val()));
                $( this )
                    .addClass( "ui-state-highlight" )
                    .find( "p" )
                        .remove();
                deleteWidget( ui.draggable,$( this ) );
                //saveWidget();
            }
        });

        // let the gallery be droppable as well, accepting items from the trash
        $( "#gallery").droppable({
            accept: "#trash .ui-widget-content",
            activeClass: "custom-state-active",
            drop: function( event, ui ) {
                //alert('dd');
                recycleImage( ui.draggable );
            }
        });

        // image deletion function
        var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='ui-icon ui-icon-refresh'>Recycle image</a>";
function cleanPrevious(model){
    var validate= false;
    //,#trash input.label
        $('#trash input.model,#trash input.element').each(function(){
            if($(this).val() != '' && $(this).val()==model)
               $(this).val('');
        });


}
        function deleteWidget( $item,$drop ) {
            console.log($drop);
        //alert($drop);
            $item.fadeOut(function() {
                /*var $list = $( "ul", $trash ).length ?
                    $( "ul", $trash ) :
                    $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );*/
                $item.find( "a.ui-icon-trash" ).remove();
                $item.width
                $item.append( recycle_icon ).appendTo( $drop ).fadeIn(function() {
                    $item
                        .animate({ width: Math.floor($drop.innerWidth()-8),height: Math.floor($drop.innerHeight()-8) })
                        .find( ".ui-widget-content" )
                            .animate({ height: Math.floor($drop.innerHeight()),width: $item.width });
                });
            });
        }


        function deleteImage( $item,$drop ) {
            console.log($drop);
        //alert($drop);
            $item.fadeOut(function() {
                /*var $list = $( "ul", $trash ).length ?
                    $( "ul", $trash ) :
                    $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );*/

                $item.find( "a.ui-icon-trash" ).remove();
                $item.append( recycle_icon ).appendTo( $drop ).fadeIn(function() {
                    $item
                        .animate({ width: $item.width })
                        .find( "img" )
                            .animate({ height: "100%" });
                });
            });
        }

        // image recycle function
        var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image</a>";
        function recycleImage( $item ) {
            //alert('dd');
//input.label,
            $item.closest('.box').children('input.model').val('');

            $item.fadeOut(function() {
                $item
                    .find( "a.ui-icon-refresh" )
                        .remove()
                    .end()
                    .css( {"width":"","height":""})
                    .append( trash_icon )
                    .find( "img" )
                        .css( "height", "" )
                    .end()
                    .appendTo( $( "#gallery" ) )
                    .fadeIn();
            });


        }

function initNavElement(){
    $('.navbar .droppable').droppable({
            accept: ".header-element",
            //activeClass: "ui-state-highlight",
            drop: function( event, ui ) {
                $( this )
                    .addClass( "-ui-state-highlight" )
                    .find( "small" )
                        .hide();
                deleteImage( ui.draggable,$( this ) );
            }
        });
}

        // resolve the icons behavior with event delegation
        $( ".ui-widget-content" ).click(function( event ) {
            var $item = $( this ),
                $target = $( event.target );

            if ( $target.is( "a.ui-icon-trash" ) ) {
                deleteImage( $item );
            } else if ( $target.is( "a.ui-icon-zoomin" ) ) {
                //viewLargerImage( $target );
            } else if ( $target.is( "a.ui-icon-refresh" ) ) {
                recycleImage( $item );
            }

            return false;
        });
    });
    </script>
    </head>

    <body>

        <div id="main-container">

        <div id="header" class="container" style="margin-top:0px">
            <?php echo $this->Html->link($this->Html->image('TapSocial_top.png',array('style'=>'height:84px; margin-bottom:10px')),'/',array('escape'=>false));?>
            <?php if($authUser){?>
                <!-- <div class="navbar-fixed-top-">
                <?php //echo $this->element('menu/top_menu'); ?>
            </div> -->
            <?php }?>
            <div class="span5 pull-right">
                <?php echo $this->Html->link('Logout',array('controller'=>'users','action'=>'logout?ver='.rand(100,999999)),array('class'=>'pull-right')); ?>
                <br>
                <?php echo $this->Html->link('My Account',array('controller'=>'users','action'=>'View'),array('class'=>'pull-right')); ?>
<br>
<span class="pull-right">
     <a href="http://tapsocial.net/digital-signage-support-faqs/" target="_blank">Help</a>
</span>
<br>
                    <?php if(!empty($authUser['Contact']['id'])){ ?>
    <h2 style="margin-bottom:0px;"><?php //echo $authUser['Contact']['company_name'];?>
<a class="btn btn-success pull-right" target="_blank" href="<?php echo Router::url('/');?>Designs/lobbyVision">Launch Your Digital Sign</a><br>
        </h2>
    <?php } ?>
            </div>
            </div><!-- #header .container -->

            <div id="content" class="container">
                <section id="dashboard">
                <?php echo $this->Session->flash(); ?>
                <div id="sidebar_container" style="display: ">
                <?php echo $this->Element('sidebar',array('cache'=>false,'hover'=>true));?>
            </div>
                <?php echo $this->fetch('content'); ?>
                </section>
            </div><!-- #header .container -->

            <div id="footer" class="container">
                <?php //Silence is golden ?>
            </div><!-- #footer .container -->

        </div><!-- #main-container -->

        <div class="container">
            <div class="">
                <small>
                    <?php //echo $this->element('sql_dump'); ?>
                </small>
            </div>
        </div><!-- .container -->
<?php //echo $this->element('ticker'); ?>

        <?php echo $this->Facebook->init(); ?>
        <script type="text/javascript">
$(document).ready(function(){
    $('.selectpicker').click(function(){
$(this).simulate('mousedown');


    })
})



$('#TeamMember .iconselect').ddslick({
    //data: ddData,
    width: 65,
    imagePosition: "left",
  //  selectText: "Select your favorite social network",
     onSelected: function(selectedData){
         $('#TeamMemberWidgetIconId').val(selectedData.selectedData.value);
     },
});

$('#Event .iconselect').ddslick({
    //data: ddData,
    width: 65,
    imagePosition: "left",
  //  selectText: "Select your favorite social network",
     onSelected: function(selectedData){
         console.log(selectedData);
         $('#EventWidgetIconId').val(selectedData.selectedData.value);
     },
});
$('#Promotion .iconselect').ddslick({
    //data: ddData,
    width: 65,
    imagePosition: "left",
  //  selectText: "Select your favorite social network",
     onSelected: function(selectedData){
             $('#PromotionWidgetIconId').val(selectedData.selectedData.value);
     },
});
$('#News .iconselect').ddslick({
    //data: ddData,
    width: 65,
    imagePosition: "left",
  //  selectText: "Select your favorite social network",

     onSelected: function(selectedData){
             $('#NewsWidgetIconId').val(selectedData.selectedData.value);

     },
});
$('#Slideshow .iconselect').ddslick({
    //data: ddData,
    width: 65,
    imagePosition: "left",
  //  selectText: "Select your favorite social network",
     onSelected: function(selectedData){
             $('#SlideshowWidgetIconId').val(selectedData.selectedData.value);

     },
});
$('#Weather .iconselect').ddslick({
    //data: ddData,
    width: 65,
    imagePosition: "left",
  //  selectText: "Select your favorite social network",
     onSelected: function(selectedData){
             $('#WeatherWidgetIconId').val(selectedData.selectedData.value);
     },
});
    </script>




    </body>

</html>