<?php echo $this->Form->create('TeamMember',array('type'=>'file')); ?>
    <fieldset>
        <legend><?php echo __('Edit Team Member'); ?></legend>
    <?php
        echo $this->Form->input('id');
        echo $this->Form->input('last_name');
        echo $this->Form->input('first_name');
        echo $this->Form->input('title');
        echo $this->Form->input('description',array('maxlength'=>'880'));
        ?>
        <div class="control-group">
            <label></label>
        <?php if(!empty($this->request->data['TeamMember']['image']))
        echo $this->Html->image('../uploads/team_member/image/thumb/small/'.$this->request->data['TeamMember']['image']);?>
    </div>
        <?php
        echo $this->Form->input('image',array('type'=>'file'));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
<div class="actions">
    <ul>

        <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('TeamMember.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('TeamMember.id'))); ?></li>
        <li><?php echo $this->Html->link(__('List Team Members'), array('action' => 'index')); ?></li>

        <li><?php echo $this->Html->link(__('New Team Members'), array('action' => 'add')); ?> </li>
    </ul>
</div>
<?php echo $this->Html->script('jquery.textareaCounter.plugin'); ?>
<script language="javascript">
  $(document).ready(function(){
    var options3 = {
                        'maxCharacterSize': 880,
                        'originalStyle': 'originalTextareaInfo',
                        'warningStyle' : 'warningTextareaInfo',
                        'warningNumber': 40,
                        'displayFormat' : '#left Characters Left / #max'
                };
                $('#TeamMemberDescription').textareaCount(options3, function(data){
                    $('#showData').html(data.input + " characters input. <br />" + data.left + " characters left. <br />" + data.max + " max characters. <br />" + data.words + " words input.");
                });
   });
</script>