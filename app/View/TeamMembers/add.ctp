<?php echo $this->Form->create('TeamMember',array('type'=>'file')); ?>
    <fieldset>
        <legend><?php echo __('Add Team Member'); ?></legend>
    <?php
        echo $this->Form->input('last_name');
        echo $this->Form->input('first_name');
        echo $this->Form->input('title');
        echo $this->Form->input('description',array('maxlength'=>'880'));
        echo $this->Form->input('image',array('type'=>'file'));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
<div class="actions">
    <ul>

        <li><?php echo $this->Html->link(__('Back to List'), array('action' => 'index')); ?></li>

    </ul>
</div>
<?php echo $this->Html->script('jquery.textareaCounter.plugin'); ?>
<script language="javascript">
  $(document).ready(function(){
    var options3 = {
                        'maxCharacterSize': 880,
                        'originalStyle': 'originalTextareaInfo',
                        'warningStyle' : 'warningTextareaInfo',
                        'warningNumber': 40,
                        'displayFormat' : '#left Characters Left / #max'
                };
                $('#TeamMemberDescription').textareaCount(options3, function(data){
                    $('#showData').html(data.input + " characters input. <br />" + data.left + " characters left. <br />" + data.max + " max characters. <br />" + data.words + " words input.");
                });
   });
</script>