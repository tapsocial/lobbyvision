<h2><?php echo __('Team Members'); ?></h2>
<table cellpadding="0" cellspacing="0">
    <tr>
            <th>ID<?php //echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('last_name'); ?></th>
            <th><?php echo $this->Paginator->sort('first_name'); ?></th>
            <th><?php echo $this->Paginator->sort('title'); ?></th>
            <th><?php echo $this->Paginator->sort('image'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
    </tr>
    <?php
    $num=($this->request->params['paging']['TeamMember']['page']-1)*$this->request->params['paging']['TeamMember']['limit'];

    foreach ($teamMembers as $teamMember): $num++;?>
    <tr>
        <td><?php echo $num;//h($teamMember['TeamMember']['id']); ?>&nbsp;</td>

        <td><?php echo h($teamMember['TeamMember']['last_name']); ?>&nbsp;</td>
        <td><?php echo h($teamMember['TeamMember']['first_name']); ?>&nbsp;</td>
        <td><?php echo h($teamMember['TeamMember']['title']); ?>&nbsp;</td>
        <td><?php echo h($teamMember['TeamMember']['image']); ?>&nbsp;</td>

        <td class="actions">
            <?php echo $this->Html->link(__('View'), array('action' => 'view', $teamMember['TeamMember']['id'])); ?>
            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $teamMember['TeamMember']['id'])); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $teamMember['TeamMember']['id']), null, __('Are you sure you want to delete # %s?', $teamMember['TeamMember']['id'])); ?>
        </td>
    </tr>
<?php endforeach; ?>
</table>
<p>
    <?php
    echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
    ));
    ?>
</p>

<div class="paging">
    <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
</div>

<div class="actions">
    <ul>
        <li><?php echo $this->Html->link(__('New Team Member'), array('action' => 'add')); ?></li>

    </ul>
</div>