<style>
.table th.text-right, .table td.text-right {text-align: right;}
</style>
<h2><?php echo __('Service Periods'); ?></h2>
<div class="btn-group">
    <?=$this->Html->link(__('Add a Service Period'), array('controller' => 'restaurant_periods',    'action' => 'add'),   array('class' => 'btn btn-primary')); ?>
</div>
<hr />
<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th><?php echo $this->Paginator->sort('name'); ?></th>
        <th><?php echo $this->Paginator->sort('starts'); ?></th>
        <th><?php echo $this->Paginator->sort('ends'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
    </tr>
    <?php
    foreach ($restaurantPeriods as $restaurantPeriod): ?>
        <tr>
            <td><?php echo h($restaurantPeriod['RestaurantPeriod']['name']); ?>&nbsp;</td>
            <td class="text-right"><?php echo h($restaurantPeriod['RestaurantPeriod']['starts']); ?>:00</td>
            <td class="text-right"><?php echo h($restaurantPeriod['RestaurantPeriod']['ends']); ?>:00</td>
            <td class="actions">
                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $restaurantPeriod['RestaurantPeriod']['id']), array('class' => 'btn btn-mini')); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $restaurantPeriod['RestaurantPeriod']['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $restaurantPeriod['RestaurantPeriod']['id'])); ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<p><small>
    <?=$this->Paginator->counter(array(
        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
    ));
    ?>
</small></p>

<div class="pagination">
    <ul>
        <?php
            echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
            echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
        ?>
    </ul>
</div><!-- .pagination -->