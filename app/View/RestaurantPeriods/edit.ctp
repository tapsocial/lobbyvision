<h2><?php echo __('Edit Restaurant Period'); ?></h2>
<div class="btn-group">
    <?php echo $this->Html->link(__('list service periods'),     array('action' => 'index'), ['class' => 'btn btn-primary']); ?>
    <?php echo $this->Html->link(__('list categories'),  array('controller' => 'restaurant_categories', 'action' => 'index'), ['class' => 'btn btn-default']); ?>
    <?php echo $this->Html->link(__('new category'),     array('controller' => 'restaurant_categories', 'action' => 'add'), ['class' => 'btn btn-default']); ?>
</div>
<hr />
<?php echo $this->Form->create('RestaurantPeriod', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
    <fieldset>
        <?php echo $this->Form->input('id', array('class' => 'span12')); ?>
        <div class="control-group">
            <?php echo $this->Form->label('name', 'Name', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('name', array('class' => 'span12', 'placeholder' => '(e.g. Breakfast)')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

        <div class="control-group">
            <?php echo $this->Form->label('starts', 'Starts', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('starts', array(
                        'min' => 0, 'max'   => 24,
                        'class' => 'span3 text-right', 'placeholder' => 14, 'after' => ':00 (24:00 hours format)')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

        <div class="control-group">
            <?php echo $this->Form->label('ends', 'Ends', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('ends', array(
                        'min' => 0, 'max'   => 24,
                        'class' => 'span3 text-right', 'placeholder' => 14, 'after' => ':00 (24:00 hours format)')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->
    </fieldset>
<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
