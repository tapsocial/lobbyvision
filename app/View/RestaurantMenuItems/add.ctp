<script>
function newPrice() {
    $('#prices .control-group:last').clone().appendTo('#prices');
    $('#prices .control-group:last input').each(function(i, element) {
      var name = $(element).attr('name');
      name = name.match(/data\[(\w+)\]\[(\d+)\]\[(\w+)\]/);
      name = 'data[' + name[1] +'][' + 1+name[2]  +'][' + name[3]  +']';
      $(element).attr('name', name);
      $(element).val('');
      $(element).removeAttr('required');
    });
}

$(function() {
   newPrice();
   $('body').on('click', '.addPrice', newPrice);

   $('body').on('click', '#prices button.btn-danger', function(event) {
       var parent = $(this).parents('.control-group');
       $(parent).remove();
       return false;
   });

});

</script>
<style>
.nav-tabs    {margin-bottom: 0; }
.tab-content {background: white; padding: 20px 0;}
.tab-content .control-group .input {
    display: inline-block;
    margin-left: 20px;
}
.tab-content .control-group  .controls {
    margin-left: 0;
}

#prices div.actions { display: inline-block; float: none; padding: 0; width: auto;}
</style>
<h2><?php echo __('Add Menu Item.'); ?></h2>
<?php echo $this->Form->create('RestaurantMenuItem', array(
        'type'          => 'file',
        'inputDefaults' => array('label' => false),
        'class' => 'form form-horizontal')
); ?>
    <fieldset>
        <div class="control-group">
            <?php echo $this->Form->label('restaurant_category_id', 'Category', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('restaurant_category_id', array('class' => 'span12')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

        <div class="control-group">
            <?php echo $this->Form->label('name', 'Name', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('name', array('class' => 'span12', 'placeholder' => '(e.g. French Fries)')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

        <div class="control-group">
            <?php echo $this->Form->label('description', 'Description', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('description', array('class' => 'span12', 'type' => 'textarea')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

        <div class="control-group">
            <?php echo $this->Form->label('picture', 'Picture', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('picture', array('class' => 'span12', 'type' => 'file')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->
        <h4>Price</h4>
        <div class="tab-content">
            <div id="prices">
                <div class="control-group">
                        <?php echo $this->Form->input('RestaurantMenuItemPrice.1.name', array(
                                'value'       => 'default',
                                'class'       => 'span12',
                                'placeholder' => 'size',
                        )); ?>
                    <div class="controls">
                        <?php echo $this->Form->input('RestaurantMenuItemPrice.1.price', array(
                                'class' => 'span11 multiPrice text-right',
                                'between' => '<span span10>$</span>',
                                'placeholder' => '9.99',
                        )); ?>
                    </div><!-- .controls -->
                    <div class="actions">
                        <button class="btn btn-danger">delete</button>
                    </div>
                </div><!-- .control-group -->
            </div>
            <a class="btn btn-success addPrice" style="margin-left: 20px;">+ add new price</a>
        </div>

    </fieldset>
    <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>