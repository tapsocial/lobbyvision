<h2><?php echo __('Restaurant Menu Items'); ?></h2>
<div class="btn-group">
    <?=$this->Html->link(__('Add a New Menu Item'), array('action' => 'add'), array('class' => 'btn btn-primary')); ?>
</div>
<hr />
<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th><?php echo $this->Paginator->sort('name'); ?></th>
        <th><?php echo $this->Paginator->sort('restaurant_category_id', 'Category'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
    </tr>
    <?php foreach ($restaurantMenuItems as $restaurantMenuItem): ?>
        <tr>
            <td><?php echo h($restaurantMenuItem['RestaurantMenuItem']['name']); ?>&nbsp;</td>
            <td>
                <?php echo $this->Html->link($restaurantMenuItem['RestaurantCategory']['name'], array(
                    'controller' => 'restaurant_categories', 'action' => 'edit', $restaurantMenuItem['RestaurantCategory']['id'])
                ); ?>
            </td>
            <td class="actions">
                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $restaurantMenuItem['RestaurantMenuItem']['id']), array('class' => 'btn btn-mini')); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $restaurantMenuItem['RestaurantMenuItem']['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $restaurantMenuItem['RestaurantMenuItem']['id'])); ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<p><small>
    <?php
    echo $this->Paginator->counter(array(
    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
    ));
    ?>			</small></p>

<div class="pagination">
    <ul>
        <?php
            echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
            echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
        ?>
    </ul>
</div><!-- .pagination -->
