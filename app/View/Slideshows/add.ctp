<div class="slideshows form">
<?php echo $this->Form->create('Slideshow',array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Add Photo'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('image',array('type'=>'file'));
		echo $this->Form->input('order',array('class'=>'input-small'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Html->link(__('List Slideshows'), array('action' => 'index')); ?></li>
		
	</ul>
</div>
