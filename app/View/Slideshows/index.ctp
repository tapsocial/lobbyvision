<h3><?php echo __('Upload photos to display in the Photo Album widget'); ?></h3>
<table cellpadding="0" cellspacing="0">
<tr>
        <th><?php echo $this->Paginator->sort('id'); ?></th>
        <th><?php echo $this->Paginator->sort('name'); ?></th>
        <th><?php echo $this->Paginator->sort('image'); ?></th>
        <th><?php echo $this->Paginator->sort('order'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
</tr>
<?php foreach ($slideshows as $slideshow): ?>
<tr>
    <td><?php echo h($slideshow['Slideshow']['id']); ?>&nbsp;</td>
    <td><?php echo h($slideshow['Slideshow']['name']); ?>&nbsp;</td>
    <td><?php echo h($slideshow['Slideshow']['image']); ?>&nbsp;</td>
    <td><?php echo h($slideshow['Slideshow']['order']); ?>&nbsp;</td>

    <td class="actions">

        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $slideshow['Slideshow']['id'])); ?>
        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $slideshow['Slideshow']['id']), null, __('Are you sure you want to delete # %s?', $slideshow['Slideshow']['id'])); ?>
    </td>
</tr>
<?php endforeach; ?>
</table>
<p>
<?php
echo $this->Paginator->counter(array(
'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
));
?>	</p>
<div class="paging">
<?php
    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
?>
</div>
<div class="actions">
    <ul>
        <li><?php echo $this->Html->link(__('Add Photo'), array('action' => 'add')); ?></li>


    </ul>
</div>
