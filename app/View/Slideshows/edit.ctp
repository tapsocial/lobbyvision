<?php echo $this->Form->create('Slideshow',array('type'=>'file')); ?>
    <fieldset>
        <legend><?php echo __('Edit Photo'); ?></legend>
    <?php
        echo $this->Form->input('id');
        echo $this->Form->input('name');?>
        <div class="control-group">
            <label></label>
        <?php if(!empty($this->request->data['Slideshow']['image']))
        echo $this->Html->image('../uploads/slideshow/image/thumb/small/'.$this->request->data['Slideshow']['image']);?>
    </div>

        <?php
        echo $this->Form->input('image',array('type'=>'file'));
        echo $this->Form->input('order',array('class'=>'input-small'));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
<div class="actions">
    <ul>

        <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Slideshow.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Slideshow.id'))); ?></li>
        <li><?php echo $this->Html->link(__('List Slideshows'), array('action' => 'index')); ?></li>

    </ul>
</div>
