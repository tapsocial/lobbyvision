<div class="slideshows view">
<h2><?php echo __('Slideshow'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($slideshow['Slideshow']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($slideshow['Slideshow']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($slideshow['Slideshow']['image']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($slideshow['User']['username'], array('controller' => 'users', 'action' => 'view', $slideshow['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order'); ?></dt>
		<dd>
			<?php echo h($slideshow['Slideshow']['order']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Slideshow'), array('action' => 'edit', $slideshow['Slideshow']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Slideshow'), array('action' => 'delete', $slideshow['Slideshow']['id']), null, __('Are you sure you want to delete # %s?', $slideshow['Slideshow']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Slideshows'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Slideshow'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
