<div class="contacts form">
<?php echo $this->Form->create('Contact'); ?>
	<fieldset>
		<legend><?php echo __('Edit Contact Information'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('company_name');
		echo $this->Form->input('contact_name');
		echo $this->Form->input('email');
		echo $this->Form->input('timezone',array('options'=>Configure::read('timezone'),'label'=>'Please select your timezone','default'=>'America/New_York'));
		echo $this->Form->input('phone');
		echo $this->Form->input('contact_preference',array('options'=>array('Phone', 'Email')));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

<!-- 		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Contact.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Contact.id'))); ?></li> -->
		
	</ul>
</div>
