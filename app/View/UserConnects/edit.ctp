<div class="userConnects form">
<?php echo $this->Form->create('UserConnect'); ?>
	<fieldset>
		<legend><?php echo __('Favebook Page'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('pageid',array('label'=>'Facebook Page ID','after'=>'</br><span class="mrgin-left">Eg: http://www.facebook.com/<b>xxxx.xxx</b> - add xxxx.xxx</span>'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Update')); ?>
</div>
<div class="actions">
	<ul>

		
		<li><?php echo $this->Html->link(__('Back to User Connects'), array('action' => 'index')); ?></li>
	
	</ul>
</div>
