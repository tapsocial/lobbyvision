<div class="userConnects view">
<h2><?php echo __('User Connect'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($userConnect['UserConnect']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userConnect['User']['username'], array('controller' => 'users', 'action' => 'view', $userConnect['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($userConnect['UserConnect']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($userConnect['UserConnect']['value']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User Connect'), array('action' => 'edit', $userConnect['UserConnect']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User Connect'), array('action' => 'delete', $userConnect['UserConnect']['id']), null, __('Are you sure you want to delete # %s?', $userConnect['UserConnect']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List User Connects'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Connect'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
