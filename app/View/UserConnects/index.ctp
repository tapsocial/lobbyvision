<div class="userConnects index">
	<h2 style="text-transform: capitalize;"><?php echo __('Social Connections'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($userConnects as $userConnect): ?>
	<tr>
		
		<td style="text-transform: capitalize;"><?php echo h($userConnect['UserConnect']['type']); ?>&nbsp;</td>
		
		<td class="actions">
			<?php 
if($userConnect['UserConnect']['type']=='twitter')
			echo $this->Html->link(__('Manage HashTags'), array('action' => 'hashTags'));
			elseif($userConnect['UserConnect']['type']=='facebook') 
				echo $this->Html->link(__('Change page ID'), array('action' => 'edit',$userConnect['UserConnect']['id']));
			?>
		<!-- 	<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $userConnect['UserConnect']['id'])); ?> -->
			<?php echo $this->Form->postLink(__('Disconnect'), array('action' => 'delete', $userConnect['UserConnect']['id']), null, __('Are you sure you want to delete # %s?', $userConnect['UserConnect']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	
	
</div>

