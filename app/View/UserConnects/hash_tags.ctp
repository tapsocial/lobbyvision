<style type="text/css">
    #tags{
  float:left;
  border:1px solid #ccc;
  padding:5px;
  width: 100%;
}
#tags span.tag{
  cursor:pointer;
  display:block;
  float:left;
 /* color:#fff;
  background:#689;*/
 /* padding:5px;*/
  padding-right:25px;
  margin:4px;
  height: 22px;
}
#tags span.tag:hover{
  opacity:0.7;
}
#tags span.tag:after{
 position:absolute;
 content:"x";
 border:1px solid;
 padding:0 4px;
 margin:3px 0 10px 5px;
 font-size:10px;
 line-height: 15px;
}
#tags input{
  background:#fff;
  border:0;
  margin:4px;
  padding:5px;
  width:auto;
  /*height: 28px;*/

}
#TickerContentHashTagsForm textarea,.content_input{ width: 98%; height: 35px;}
</style>
<?php echo $this->Html->script('addMore'); ?>
<script type="text/javascript">
         function delete_feature(id){
           $.ajax({
               type:"GET",
               url:"<?php  echo  Router::url('delete/');?>"+id,
               success : function(data) {
                $("#content_"+id).remove();
               },
               error : function() {

               },
           })
         }
</script>




<?php

//debug($uconnect);
echo $this->Form->create('Hasgtag',array('onsubmit'=>'return false;')); ?>



 <?php if(isset($uconnect['UserConnect']['type']) && $uconnect['UserConnect']['type']=='twitter'){
  ?>
  <?php echo $this->Html->link('Disconnect Twitter',array('action'=>'delete',$uconnect['UserConnect']['id']),array('class'=>'pull-right'));
 ?>
 <br>
  <?php echo $this->Form->input('Design.show_tweets',array('type'=>'checkbox','label'=>'Live updates from Twitter','div'=>false,'onclick'=>'savetags();')) ?>

<section class="well" id="twitter_container">

    <fieldset >
        <legend><?php echo __('Twitter Hashtags'); ?><br>
        <small><i>Enter any hashtags that you wish to include in your social media ticker.</i></small>
    </legend>

        <section class="checklist ">
            <div id="tags">
        <?php foreach($uconnect['Hashtag'] as $hashTag){ ?>
    <span class="tag btn btn-small" id="tag1" ><?php echo $hashTag['hashtag'];?><input type="hidden" name="data[Hashtag][][hashtag]" value="<?php echo $hashTag['hashtag'];?>" /></span>
    <?php } ?>

    <input type="text btn btn-small" value="" placeholder="Add a HashTag" />
  </div>


</section>
    </fieldset>


</section>
<?php }else{?>

  <?php echo $this->Html->link($this->Html->image('twitter-connect.png'),array('controller'=>'twitter','action'=>'connect','plugin'=>'twitter'),array('class'=>'','escape'=>false)); ?>
  <br>

<?php }   ?>



  <br />
   <?php echo $this->Form->input('Design.ticker_content',array('type'=>'checkbox','label'=>'Add your own messages to the ticker','div'=>false,'onclick'=>'savetags();')) ?>

   </form>


   <?php echo $this->Form->create('TickerContent'); ?>
  <section class="well">
  <?php
  $num=0;
  if(!empty($tickerConents)){
    foreach($tickerConents as $tickerConent){ ?>
    <section class=" " id="features_<?php echo $tickerConent['TickerContent']['id'];?>">
      <button class="btn btn-small remove-box" onclick="delete_content(<?php echo $tickerConent['TickerContent']['id'];?>)" type="button" style="float: right;">X</button>
    <?php

      echo $this->Form->input('TickerContent.'.$num.'.id',array('type'=>'hidden','value'=>$tickerConent['TickerContent']['id']));
    echo $this->Form->input('TickerContent.'.$num.'.content',array('value'=>$tickerConent['TickerContent']['content'],'class'=>'','required'=>'none','label'=>false,'type'=>'textarea'));?>

<div class="clear"></div>
  </section>
      <?php
      $num++;
    }
  }
  ?>
  <section class="rowSet  controls-row">
  <?php
    echo $this->Form->input('TickerContent.'.$num.'.id',array('type'=>'hidden'));
    echo $this->Form->input('TickerContent.'.$num.'.content',array(
                'div'=>false,'label'=>false,'placeholder'=>'Content here',
                'class'=>'content_input','required'=>'none','label'=>false,'type'=>'textarea'));


    ?>
    &nbsp;
<?php //$num++; ?>
  </section>

  <section id="appendDiv">
  </section>


   <input id="addMoreindex" name="index" type="hidden" value="<?php echo $num;?>" />
   <a id="addMore" class="btn-link pull-right" style="margin-top:-20px;line-height:12px" data-toggle="dropdown" href="#">
    Add New
    </a>
    <br>
     <a id="save_content" onclick="saveContent(); return false;" class="btn-link " data-toggle="dropdown" href="#">
   Save content
    </a>
  </section>
<?php //echo $this->Form->end(__('Submit')); ?>
</form>


<script type="text/javascript">
$(document).ready(function(){
  //$('#HasgtagHashTagsForm').submit()
  OpenContainer();
});

    $(function(){

  $('#tags input').on('focusout',function(){
    var txt= this.value.replace(/[^a-zA-Z0-9\+\-\.\#]/g,''); // allowed characters
    if(txt) {
count=$('#tags .tag').length+1;//count(tags
  html='<span class="tag btn btn-small" id="tag'+count+'">'+ txt.replace('#','') +'<input type="hidden" name="data[Hashtag][][hashtag]" value="'+txt.replace('#','')+'" /></span>';
        $(this).before(html);

        savetags();


    }
    this.value="";
  }).on('keyup',function( e ){
    // if: comma,enter (delimit more keyCodes with | pipe)
    if(/(188|13)/.test(e.which)) $(this).focusout();


  });


  $('#tags').on('click','.tag',function(){
     if(confirm("Really delete this tag?")) $(this).remove();
     savetags();
  });

});

function savetags(){
  $.ajax({
    type: "POST",
    url: "<?php echo  Router::url(array(
    'controller' => 'UserConnects',
    'action' => 'hashTags')); ?>",
    data: $('#HasgtagHashTagsForm').serialize(),
    })
    .done(function( msg ) {
    //alert( "Data Saved: " + msg );
    });

}

function saveContent(){
  $('#save_content').text('Saving..');
  $.ajax({
    type: "POST",
    url: "<?php echo  Router::url(array(
    'controller' => 'UserConnects',
    'action' => 'hashTags')); ?>",
    data: $('#TickerContentHashTagsForm').serialize(),
    })
    .done(function( msg ) {
      $('#save_content').text('Saved').delay(3000,function(){
         $('#save_content').text('Save content');
      });

    //alert( "Data Saved: " + msg );
    });

}
function delete_content(id){
  $('#features_'+id).remove();
  $.ajax({
    type: "POST",
    url: "<?php echo  Router::url(array(
    'controller' => 'UserConnects',
    'action' => 'delete_tc')); ?>",
    data: {'data[cid]':id},
    })
    .done(function( msg ) {
      //$('#save_content').text('Saved').delay(3000,function(){
       //  $('#save_content').text('Save content');
      });

}
function OpenContainer(){

}
</script>
