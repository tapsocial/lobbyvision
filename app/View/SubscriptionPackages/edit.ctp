<div class="subscriptionPackages form">
<?php echo $this->Form->create('SubscriptionPackage'); ?>
	<fieldset>
		<legend><?php echo __('Edit Subscription Package'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('months');
		echo $this->Form->input('price_per_month');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SubscriptionPackage.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('SubscriptionPackage.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Subscription Packages'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
