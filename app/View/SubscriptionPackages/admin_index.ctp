<div class="subscriptionPackages index">
	<h2><?php echo __('Subscription Packages'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('months'); ?></th>
			<th><?php echo $this->Paginator->sort('price_per_month'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($subscriptionPackages as $subscriptionPackage): ?>
	<tr>
		<td><?php echo h($subscriptionPackage['SubscriptionPackage']['id']); ?>&nbsp;</td>
		<td><?php echo h($subscriptionPackage['SubscriptionPackage']['name']); ?>&nbsp;</td>
		<td><?php echo h($subscriptionPackage['SubscriptionPackage']['months']); ?>&nbsp;</td>
		<td><?php echo h($subscriptionPackage['SubscriptionPackage']['price_per_month']); ?>&nbsp;</td>
		<td><?php echo h($subscriptionPackage['SubscriptionPackage']['modified']); ?>&nbsp;</td>
		<td><?php echo h($subscriptionPackage['SubscriptionPackage']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $subscriptionPackage['SubscriptionPackage']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $subscriptionPackage['SubscriptionPackage']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $subscriptionPackage['SubscriptionPackage']['id']), null, __('Are you sure you want to delete # %s?', $subscriptionPackage['SubscriptionPackage']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Subscription Package'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
