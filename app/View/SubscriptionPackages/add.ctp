<div class="subscriptionPackages form">
<?php echo $this->Form->create('SubscriptionPackage'); ?>
	<fieldset>
		<legend><?php echo __('Add Subscription Package'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('months');
		echo $this->Form->input('price_per_month');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Subscription Packages'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
