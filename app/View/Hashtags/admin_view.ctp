<div class="hashtags view">
<h2><?php echo __('Hashtag'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($hashtag['Hashtag']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User Connect'); ?></dt>
		<dd>
			<?php echo $this->Html->link($hashtag['UserConnect']['user_id'], array('controller' => 'user_connects', 'action' => 'view', $hashtag['UserConnect']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hashtag'); ?></dt>
		<dd>
			<?php echo h($hashtag['Hashtag']['hashtag']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Hashtag'), array('action' => 'edit', $hashtag['Hashtag']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Hashtag'), array('action' => 'delete', $hashtag['Hashtag']['id']), null, __('Are you sure you want to delete # %s?', $hashtag['Hashtag']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Hashtags'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Hashtag'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List User Connects'), array('controller' => 'user_connects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Connect'), array('controller' => 'user_connects', 'action' => 'add')); ?> </li>
	</ul>
</div>
