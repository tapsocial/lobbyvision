<div class="hashtags form">
<?php echo $this->Form->create('Hashtag'); ?>
	<fieldset>
		<legend><?php echo __('Add Hashtag'); ?></legend>
	<?php
		echo $this->Form->input('user_connect_id');
		echo $this->Form->input('hashtag');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Hashtags'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List User Connects'), array('controller' => 'user_connects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Connect'), array('controller' => 'user_connects', 'action' => 'add')); ?> </li>
	</ul>
</div>
