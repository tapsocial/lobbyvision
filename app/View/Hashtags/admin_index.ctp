<div class="hashtags index">
	<h2><?php echo __('Hashtags'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_connect_id'); ?></th>
			<th><?php echo $this->Paginator->sort('hashtag'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($hashtags as $hashtag): ?>
	<tr>
		<td><?php echo h($hashtag['Hashtag']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($hashtag['UserConnect']['user_id'], array('controller' => 'user_connects', 'action' => 'view', $hashtag['UserConnect']['id'])); ?>
		</td>
		<td><?php echo h($hashtag['Hashtag']['hashtag']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $hashtag['Hashtag']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $hashtag['Hashtag']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $hashtag['Hashtag']['id']), null, __('Are you sure you want to delete # %s?', $hashtag['Hashtag']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Hashtag'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List User Connects'), array('controller' => 'user_connects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Connect'), array('controller' => 'user_connects', 'action' => 'add')); ?> </li>
	</ul>
</div>
