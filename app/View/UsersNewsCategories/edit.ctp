<h2><?php echo __('Reuters™ News'); ?></h2>
<?php echo $this->Form->create('UsersNewsCategory'); ?>
<section class="well">
    <fieldset >
        <section class="checklist ">
            <?php echo $this->Form->select('news_category_id', $newsCategories,array('multiple' => 'checkbox')) ?>
        </section>
    </fieldset>
</section>

<div class="text-right">
    <small ><i>* TapSocial is not responsible for the content of any news articles, headlines or videos.</i></small>
</div>

<?php echo $this->Form->end(__('Submit')); ?>