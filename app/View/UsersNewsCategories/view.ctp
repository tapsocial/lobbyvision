<div class="usersNewsCategories view">
<h2><?php echo __('Users News Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($usersNewsCategory['UsersNewsCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('News Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersNewsCategory['NewsCategory']['name'], array('controller' => 'news_categories', 'action' => 'view', $usersNewsCategory['NewsCategory']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usersNewsCategory['User']['username'], array('controller' => 'users', 'action' => 'view', $usersNewsCategory['User']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Users News Category'), array('action' => 'edit', $usersNewsCategory['UsersNewsCategory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Users News Category'), array('action' => 'delete', $usersNewsCategory['UsersNewsCategory']['id']), null, __('Are you sure you want to delete # %s?', $usersNewsCategory['UsersNewsCategory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users News Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users News Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List News Categories'), array('controller' => 'news_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News Category'), array('controller' => 'news_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
