<div class="usersNewsCategories form">
<?php echo $this->Form->create('UsersNewsCategory'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Users News Category'); ?></legend>
	<?php
		echo $this->Form->input('news_category_id',array('multiple'=>true));
		echo $this->Form->input('user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>					

		<li><?php echo $this->Html->link(__('List Users News Categories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List News Categories'), array('controller' => 'news_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News Category'), array('controller' => 'news_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
