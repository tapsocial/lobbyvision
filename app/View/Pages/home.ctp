<h1>Welcome to TapSocial Digital Signage.</h1>

<div class="row-fluid">
    <div class="span6">
        <div class="hero-unit well">
            <h2>Launch</h2>
            <p>Open your Digital Signage.</p>
            <?=$this->Html->link('Launch', '/launch', ['class'=>'btn btn-large btn-block btn-success']);?>
        </div>
    </div>
    <div class="span6">
        <div class="hero-unit well">
            <h2>Configure</h2>
            <p>Apply the changes you want to show in real time.</p>
            <?=$this->Html->link('Configure', '/configure', ['class'=>'btn btn-large btn-block btn-primary']);?>

        </div>
    </div>
</div>

<div style="max-width: 300px; margin: 0 auto 10px;" class="well text-center">
    <p>Not a user yet?</p>
    <?php echo $this->Html->link('Sign up',
            array('admin'=>false,'controller'=>'users','action'=>'select_package'),
            array('class'=>'btn btn-large')
    );?>
</div>