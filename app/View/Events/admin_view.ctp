<div class="events view">
<h2><?php  echo __('Event'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($event['Event']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('User'); ?></dt>
        <dd>
            <?php echo $this->Html->link($event['User']['email'], array('controller' => 'users', 'action' => 'view', $event['User']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Event Name'); ?></dt>
        <dd>
            <?php echo h($event['Event']['event_name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Event Start'); ?></dt>
        <dd>
            <?php echo h($event['Event']['start']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Event End'); ?></dt>
        <dd>
            <?php echo h($event['Event']['end']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Description'); ?></dt>
        <dd>
            <?php echo h($event['Event']['description']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Image'); ?></dt>
        <dd>
            <?php echo h($event['Event']['image']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Display Begining'); ?></dt>
        <dd>
            <?php echo h($event['Event']['display_begining']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Display Ending'); ?></dt>
        <dd>
            <?php echo h($event['Event']['display_ending']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Modified'); ?></dt>
        <dd>
            <?php echo h($event['Event']['modified']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Created'); ?></dt>
        <dd>
            <?php echo h($event['Event']['created']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('Edit Event'), array('action' => 'edit', $event['Event']['id'])); ?> </li>
        <li><?php echo $this->Form->postLink(__('Delete Event'), array('action' => 'delete', $event['Event']['id']), null, __('Are you sure you want to delete # %s?', $event['Event']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('List Events'), array('action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Event'), array('action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
    </ul>
</div>
