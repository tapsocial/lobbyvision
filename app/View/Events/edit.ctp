<?
/* @var $this View */
echo $this->Html->script('jquery.textareaCounter.plugin');
echo $this->Html->script('datepicker/zebra_datepicker.js');
echo $this->Html->css('datepicker/zebra_datepicker.css');
?>
<script type="text/javascript">
$(document).ready(function(){
    $('#EventAllDay').change(function(){
        var isChecked = $('#EventAllDay').attr('checked')?true:false;
        if (isChecked){
            $('#EventStartHour').val('12');
            $('#EventStartMin').val('00');
            $('#EventStartMeridian').val('am');

            $('#EventEndHour').val('11');
            $('#EventEndMin').val('59');
            $('#EventEndMeridian').val('pm');

            $('#EventStartHour, #EventStartMin, #EventEndHour, #EventStartMeridian, #EventEndMin, #EventEndMeridian')
                .attr('disabled','disabled')
                .addClass('hide');
        } else {
            $('#EventStartHour, #EventStartMin, #EventEndHour, #EventStartMeridian, #EventEndMin, #EventEndMeridian, #EventEndMonth, #EventEndDay, #EventEndYear')
                .removeAttr('disabled hide')
                .removeClass('hide');
            $('.enddate').show();
        }
    }).change();
    var options3 = {
        'maxCharacterSize': 880,
        'originalStyle': 'originalTextareaInfo',
        'warningStyle' : 'warningTextareaInfo',
        'warningNumber': 40,
        'displayFormat' : '#left Characters Left / #max'
    };
    $('#EventDescription').textareaCount(options3, function(data){
        $('#showData').html(data.input + " characters input. <br />" + data.left + " characters left. <br />" + data.max + " max characters. <br />" + data.words + " words input.");
    });
    $('.datePicker').Zebra_DatePicker();
    $('#RecurrenceRepeatType').change(function() {
        $('.recurrence.alert').html('').hide();
        switch ($(this).val()) {
            case 'never':
                $('#RecurrenceRepeatStart').parents('div.input').hide();
                $('#RecurrenceRepeatStart').removeAttr('required');
                break;
            case 'monthly':
                $('.recurrence.alert')
                    .show()
                    .html('<strong>Note:</strong> This entry will appear on your digital sign on the day of the month you select. For example, if you select the 15th, it will appear every month on the 15th indefinitely. Since not all months have 29, 30, or 31 days, consider selecting a date that falls between the 1st and 28th of the month.');
            default:
                $('#RecurrenceRepeatStart').attr('required', 'required');
                $('#RecurrenceRepeatStart').attr('placeholder', 'YYYY-MM-DD');
                $('#RecurrenceRepeatStart').removeAttr('readonly');
                $('#RecurrenceRepeatStart').parents('div.input').show();

        }
        if ($(this).val() == 'never') {
        } else {
        }
    }).change();
});
</script>
<style>
    .mrgin-left, .charleft {float: none; margin-left: 0;}
</style>
<h1><?php echo __('Edit Event'); ?></h1>
<?php echo $this->Form->create('Event', array('type' => 'file')); ?>
    <fieldset>
    <?php
        echo $this->Form->input('id');
        echo $this->Form->input('user_id',array('type'=>'hidden'));
        echo $this->Form->input('event_name');
        echo $this->Form->input('description',array('maxlength'=>'880'));
        echo $this->Form->input('all_day');
        echo $this->Form->input('start');
        echo $this->Form->input('end');
        echo $this->Form->input('image',array('type' => 'file'));
    ?>
    <div class="control-group">
            <label></label>
        <?php
        if(!empty($this->request->data['Event']['image']))
            echo $this->Html->image('../uploads/event/image/thumb/small/'.$this->request->data['Event']['image']);
        ?>
    </div>
    <?php
        echo $this->Form->input('Recurrence.repeat_type',  [
            'label'   => 'Repeat Frequency',
            'options' => $this->viewVars['recurrences'],
            'value'   => $this->viewVars['recurrence']
        ]);
        echo $this->Form->input('Recurrence.repeat_start', [
            'type'  => 'text',
            'class' => 'datePicker',
            'label' => 'Repeat Beginning On Date',
            'after' => '<div class="recurrence alert alert-info"></div>',
        ]);
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>