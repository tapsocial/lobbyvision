<div class="weatherUndergrounds form">
<?php echo $this->Form->create('WeatherUnderground'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Weather Underground'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('country_id');
		echo $this->Form->input('location');
		echo $this->Form->input('url');
		echo $this->Form->input('called');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Weather Undergrounds'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
	</ul>
</div>
