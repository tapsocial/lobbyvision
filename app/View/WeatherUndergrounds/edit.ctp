  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
      <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />


<?php echo $this->Form->create('WeatherUnderground'); ?>
    <fieldset>
        <legend><?php echo __('Edit Weather Underground™'); ?></legend>
    <?php
    //debug($authUser);
        echo $this->Form->input('id');
        echo $this->Form->input('country_id',array('type'=>'hidden','default'=>$authUser['MailingAddress']['country_id'],
            'onchange'=>"test()"));
        echo $this->Form->input('location',array('after'=>$this->Html->image('ajaxloader.gif',array('id'=>'loader')).'<br><span class="mrgin-left msg">Find and select your location from the list of cities as you type</span><br><br>'));
        echo $this->Form->input('url',array('readonly'=>'readonly','label'=>'Location key','required'=>false));
        echo $this->Form->input('latlon',array('readonly'=>'readonly','label'=>'Co-ordinate','required'=>false));
        //echo $this->Form->input('WeatherPanel',array('options'=>$weather_panels,'multiple'=>'checkbox'));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
<div class="actions">
    <ul>



    </ul>
</div>
<script type="text/javascript">

$(function() {
    $( "#WeatherUndergroundLocation" ).blur(function(){
        //alert($("#WeatherUndergroundLocation").val());
        if($("#WeatherUndergroundLocation").val()=='' || $("#WeatherUndergroundLocation").val()==''){
        $("#loader").hide();
        $("#WeatherUndergroundLocation").val('');
        $("#WeatherUndergroundLocation").focus();
        }

    })

    $( "#WeatherUndergroundLocation" ).autocomplete(
    {
         search: function( event, ui ) {
            $("#loader").show();
            if($('WeatherUndergroundUrl').val()=='')
            $('input[type="submit"]').hide();

        },

        source: "<?php echo Router::url('/');?>WeatherUndergrounds/getAddress",
        minLength: 2,
        /* snip */
    select: function(event, ui) {
        event.preventDefault();
        //console.log(ui);
        $("#WeatherUndergroundUrl").val('zmw:'+ui.item.value);
        $("#WeatherUndergroundLatlon").val(ui.item.latlon);
       $("#loader").hide();
       $('input[type="submit"]').show();
    },
    focus: function(event, ui) {
        event.preventDefault();
         $("#WeatherUndergroundLocation").val(ui.item.label);
    },
    response: function(event, ui) {
         $("#loader").hide();
            // ui.content is the array that's about to be sent to the response callback.
            if (ui.content.length === 0) {
                 $("#WeatherUndergroundLocation").val('');
                $(".msg").html("No results found");
            } else {
                $(".msg").html("result found.");
               // $(".msg").empty();
            }
        }

    })

});

</script>