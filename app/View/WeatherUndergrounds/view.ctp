<div class="weatherUndergrounds view">
<h2><?php echo __('Weather Underground'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($weatherUnderground['WeatherUnderground']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($weatherUnderground['User']['username'], array('controller' => 'users', 'action' => 'view', $weatherUnderground['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($weatherUnderground['Country']['name'], array('controller' => 'countries', 'action' => 'view', $weatherUnderground['Country']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Location'); ?></dt>
		<dd>
			<?php echo h($weatherUnderground['WeatherUnderground']['location']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Url'); ?></dt>
		<dd>
			<?php echo h($weatherUnderground['WeatherUnderground']['url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Called'); ?></dt>
		<dd>
			<?php echo h($weatherUnderground['WeatherUnderground']['called']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Weather Underground'), array('action' => 'edit', $weatherUnderground['WeatherUnderground']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Weather Underground'), array('action' => 'delete', $weatherUnderground['WeatherUnderground']['id']), null, __('Are you sure you want to delete # %s?', $weatherUnderground['WeatherUnderground']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Weather Undergrounds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Weather Underground'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
	</ul>
</div>
