<div class="backgroundVideos form">
    <?php echo $this->Form->create('BackgroundVideo', array('type'=>'file', 'inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
        <fieldset>
            <h2><?php echo __('Admin Add Background Video'); ?></h2>
            <div class="control-group">
                <?php echo $this->Form->label('name', 'Name', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $this->Form->input('name', array('class' => 'span12')); ?>
                </div><!-- .controls -->
            </div><!-- .control-group -->

            <div class="control-group">
                <?php echo $this->Form->label('screenshot', 'Screenshot', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $this->Form->input('screenshot', array('type'=>'file', 'class' => 'span12')); ?>
                </div><!-- .controls -->
            </div><!-- .control-group -->

            <div class="control-group">
                <?php echo $this->Form->label('video', 'Video', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $this->Form->input('video', array('type'=>'file', 'class' => 'span12')); ?>
                </div><!-- .controls -->
            </div><!-- .control-group -->

        </fieldset>
    <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
    <?php echo $this->Form->end(); ?>
</div>


<div class="actions">
    <ul class="nav nav-list bs-docs-sidenav">
        <li><?php echo $this->Html->link(__('List Videos'), array('action' => 'index')); ?></li>
    </ul><!-- .nav nav-list bs-docs-sidenav -->
</div><!-- .actions -->
