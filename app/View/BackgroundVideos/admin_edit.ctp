<div class="backgroundVideos form">
    <?php echo $this->Form->create('BackgroundVideo', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
        <fieldset>
            <h2><?php echo __('Admin Edit Background Video'); ?></h2>
            <div class="control-group">
                <div class="controls">
                    <?php echo $this->Form->input('id', array('class' => 'span12')); ?>
                </div><!-- .controls -->
            </div><!-- .control-group -->

            <div class="control-group">
                <?php echo $this->Form->label('name', 'Name', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $this->Form->input('name', array('class' => 'span12')); ?>
                </div><!-- .controls -->
            </div><!-- .control-group -->

            <div class="control-group">
                <?php echo $this->Form->label('screenshot', 'Screenshot', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $this->Form->input('screenshot', array('type'=>'file', 'class' => 'span12')); ?>
                </div><!-- .controls -->
            </div><!-- .control-group -->

            <div class="control-group">
                <?php echo $this->Form->label('video', 'Video', array('class' => 'control-label'));?>
                <div class="controls">
                    <?php echo $this->Form->input('video', array('type'=>'file', 'class' => 'span12')); ?>
                </div><!-- .controls -->
            </div><!-- .control-group -->
        </fieldset>
    <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul class="nav nav-list">
        <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('BackgroundVideo.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('BackgroundVideo.id'))); ?></li>
        <li><?php echo $this->Html->link(__('List   Videos'), array('action' => 'index')); ?></li>
    </ul><!-- .nav nav-list bs-docs-sidenav -->
</div><!-- .actions -->