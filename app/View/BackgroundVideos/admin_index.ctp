        <div class="backgroundVideos index">
            <h2><?php echo __('Background Videos'); ?></h2>
            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
                <tr>
                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('screenshot'); ?></th>
                                            <th><?php echo $this->Paginator->sort('video'); ?></th>
                                            <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                <?php
                foreach ($backgroundVideos as $backgroundVideo): ?>
                    <tr>
                        <td><?php echo h($backgroundVideo['BackgroundVideo']['id']); ?>&nbsp;</td>
                        <td><?php echo h($backgroundVideo['BackgroundVideo']['name']); ?>&nbsp;</td>
                        <td><?php echo h($backgroundVideo['BackgroundVideo']['screenshot']); ?>&nbsp;</td>
                        <td><?php echo h($backgroundVideo['BackgroundVideo']['video']); ?>&nbsp;</td>
                        <td class="actions">
                            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $backgroundVideo['BackgroundVideo']['id']), array('class' => 'btn btn-mini')); ?>
                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $backgroundVideo['BackgroundVideo']['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $backgroundVideo['BackgroundVideo']['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

            <p><small>
                <?php
                    echo $this->Paginator->counter(array(
                            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                ?>			</small></p>

            <div class="pagination">
                <ul>
                    <?php
                        echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
                        echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                    ?>
                </ul>
            </div><!-- .pagination -->
        </div><!-- .index -->

<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul class="nav nav-list bs-docs-sidenav">
        <li><?php echo $this->Html->link(__('New Video'), array('action' => 'add'), array('class' => '')); ?></li>
    </ul><!-- .nav nav-list bs-docs-sidenav -->
</div><!-- .actions -->

