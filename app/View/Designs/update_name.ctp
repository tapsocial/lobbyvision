<div class="designs form">
<?php echo $this->Form->create('Design'); ?>
	<fieldset>
		<legend><?php echo __('Enter the name as you wish it to display'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('display_name',array('label'=>'Title','after'=>'<small> Max 50 char allowed.</small>'));
		echo $this->Form->input('show_name',array('label'=>'Show In Digital Sign','div'=>'false'));
		echo $this->Form->input('datetime_format',array('label'=>'Display Date & Time as','options'=>array('%A %B %d, %Y | %I:%M%p'=>'Date and Time'
			,'%A %B %d, %Y'=>'Date Only'
			,'%I:%M%p'=>'Time Only'
			)));
	?>
	</fieldset>
<div class="no-wrapper">
<?php echo $this->Form->end(__('Update')); ?>
</div>
</div>
<div class="actions">
	<ul>
		
		<li><?php echo $this->Html->link(__('Back to Designs'), array('action' => 'index')); ?></li>
		
	</ul>
</div>
