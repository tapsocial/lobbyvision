<div class="designs view">

<!-- <h4><?php  echo __('Choose Your Layout'); ?></h4>
 -->

<div class="ui-widget ui-helper-clearfix">

<div class="layout_chooser" style="display:">
  <ul class="nav nav-pills">
      <?php foreach($layouts as $layout){
          $class='';
          if($layout_id==$layout['Layout']['id']){
              $class='active';
              $active_layout=$layout;
          }

          echo '<li class="'.$class.'">';
          echo $this->Html->link($this->Html->image('../uploads/layout/image/'.$layout['Layout']['image']),array('controller'=>'Designs','action'=>'index',$layout['Layout']['id']),array('escape'=>false));

          echo '</li>';
      } ?>


    </ul>
</div>
<h4><?php  echo __('Drag & Drop Widgets to Your Display'); ?></h4>
<div id="gallery" class="gallery ui-helper-reset ui-helper-clearfix">

<?php if(!in_array('Event',$widgets)){ ?>
    <div id="Event" class="ui-widget-content body-element" label="Events">
        <h5 class="ui-widget-header">Events</h5>
        <span class="w-select">
        <input class="input-small label span2" type="text" value="Events" name="data[Event][label]" />
    </span>
            <?php
    echo '<small class="w-select">';
//echo $this->Form->input('Event.number_of_display',array('options'=>array(1=>'One large event at a time',2=>'Two smaller events at a time'),'label'=>'Display as','class'=>'selectpicker span2'));
echo $this->Form->input('Event.widget_icon_id',array('type'=>'hide'));
echo '</small>';

              ?>

        <a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
    </div>
    <?php } ?>

    <?php if(!in_array('TeamMember',$widgets)){ ?>
    <div id="TeamMember" class="ui-widget-content body-element" label="Team Members">
        <h5 class="ui-widget-header">Team Members</h5>

        <span class="w-select">
        <input class="input-small label span2" type="text" value="Team Members" name="data[TeamMember][label]" />
    </span>
            <?php
    echo '<small class="w-select">';
//echo $this->Form->input('TeamMember.number_of_display',array('options'=>array(1=>'One large view of single Staff Profile at a time',2=>'Two smaller view of Staff Profile at a time'),'label'=>'Display as','class'=>'selectpicker span2'));
echo $this->Form->input('TeamMember.widget_icon_id',array('type'=>'hide'));
echo '</small>';
              ?>


        <a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
    </div>
    <?php } ?>

    <?php if(!in_array('Promotion',$widgets)){ ?>
    <div id="Promotion"  class="ui-widget-content body-element" label="Sales & Promotions">

        <h5 class="ui-widget-header">Sales & Promotions</h5>
    <span class="w-select">
        <input class="input-small label span2" type="text" value="Promotions" name="data[Promotion][label]" />
    </span>
            <?php
    echo '<small class="w-select">';
//echo $this->Form->input('Promotion.number_of_display',array('options'=>array(1=>'One large promotion at a time',2=>'Two smaller promotions at a time'),'label'=>'Display as','class'=>'selectpicker span2'));
echo '</small>';
echo $this->Form->input('Promotion.widget_icon_id',array('type'=>'hide'));
              ?>



        <a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
    </div>
    <?php } ?>

    <?php if(!in_array('News',$widgets)){ ?>
    <div id="News"  class="ui-widget-content body-element" label="News Headlines">
        <h5 class="ui-widget-header">News Headlines</h5>
    <span class="w-select">
        <input class="input-small label span2" type="text" value="News" name="data[News][label]" />
        <?php echo $this->Form->input('News.widget_icon_id',array('type'=>'hide')); ?>
    </span>




        <a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
    </div>
    <?php } ?>

    <?php if(!in_array('Weather',$widgets)){ ?>
    <div id="Weather"  class="ui-widget-content body-element" label="Weather">
        <h5 class="ui-widget-header">Weather</h5>
    <span class="w-select">
        <input class="input-small label span2" type="text" value="Weather" name="data[Weather][label]" />
    </span>


        <a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
    </div>
    <?php } ?>
</div>
<h5 class="center" id="loader"><small class="success" >Saving..</small> &nbsp;<a class="btn btn-small pull-right btn-success" href="#" onclick="saveWidget();return false;" >Save Display Layout</a></h5>

    <?php echo $this->Form->create('UserLayout');?>
<div id="trash" class="ui-widget-content ui-state-default">
<?php $model_list=array(); ?>
    <div class="navbar">
    <div class="navbar-inner">
    <ul class="nav">


    </ul>
    <ul class="nav pull-right">
        <li class="" ><a sty="line-height:60px;" href="#">Digital Clock</a></li></ul>
    </div>
    </div>



    <?php echo $this->Form->input('layout_id',array('type'=>'hidden','value'=>$active_layout['Layout']['id']));
    ?>

        <?php $boxes=explode('x', $active_layout['Layout']['name']);?>
<div id="box">
<div class="col3 col<?php echo count($boxes);?>">
<?php
$col=0;
$order=0;
foreach($boxes as $box){


    if($box==1){
        $class='box-long box';
        $col++;
    if(isset($user_layout['Widget'][$order])){
        $model=$user_layout['Widget'][$order]['name'];
        $label=$user_layout['Widget'][$order]['label'];
        $element=$user_layout['Widget'][$order]['element'];
        //$icon=$user_layout['Widget'][$order]['widget_icon_id'];

    }
    else{
                $model='';
                $label='';
                $element='';
                //$icon='';
            }

        ?>
        <!-- ****************          Long boxe start  ****************-->
        <div class="<?php echo $class;?> droppable">

            <input class="input-small model" type="hidden" id="Widget-<?php echo $order ?>" name="data[Widget][<?php echo $order; ?>][name]" value="<?php echo $model; ?>" />
            <input class="input-small classname" type="hidden" value="box-long" name="data[Widget][<?php echo $order; ?>][class]" />

            <input class="input-small element" type="hidden" value="<?php echo $element; ?>" name="data[Widget][<?php echo $order; ?>][element]" />



    <?php if($model!=''){
        $model_list[]=$model;
        ?>
            <div id="<?php echo $model; ?>" class="ui-widget-content body-element">


        <h5 class="ui-widget-header"><?php echo $label ?>

    </h5>
    <span class="w-select">
        <input class="input-small label span2" type="text" value="<?php echo $label; ?>" name="data[<?php echo $model?>][label]" />
            </span>
        <?php if($model=='Event' || $model=='Promotion' || $model=='TeamMember'){?>

    <?php /*echo '<small class="w-select">';
echo $this->Form->input($model.'.number_of_display',array('options'=>array(1=>'One large '.str_replace('TeamMember', 'Staff Profile', Inflector::humanize($model)).' at a time',2=>'Two smaller '.str_replace('TeamMember', 'Staff Profile', Inflector::humanize($model)).'s at a time'),'label'=>'Display as','class'=>'selectpicker span2'));
echo '</small>';*/
             } ?>


        <a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-refresh">Delete image</a>
        </div>

    <?php }  ?>
        </div>
    <!-- ****************   small boxes start  ****************-->
        <?php }elseif($box==2){
            $class='box-small box';
                if(isset($user_layout['Widget'][$order])){
                    $model=$user_layout['Widget'][$order]['name'];
                    $label=$user_layout['Widget'][$order]['label'];
                    $number_of_display=$user_layout['Widget'][$order]['number_of_display'];
                    $element=$user_layout['Widget'][$order]['element'];
                }
                else{
                $model='';
                $label='';
                $number_of_display='';
                $element='';
            }
            ?>
            <div class="<?php echo $class;?> droppable">
                <input class="input-small model" type="hidden" id="Widget-<?php echo $order ?>" name="data[Widget][<?php echo $order; ?>][name]" value="<?php echo $model; ?>" />
                <input class="input-small classname" type="hidden" value="box-small" name="data[Widget][<?php echo $order; ?>][class]" />

                <input class="input-small element" type="hidden" value="<?php echo $element; ?>" name="data[Widget][<?php echo $order; ?>][element]" />


                <?php if($model!=''){
        $model_list[]=$model;
        ?>
            <div id="<?php echo $model; ?>" class="ui-widget-content body-element">
        <h5 class="ui-widget-header"><?php echo $label ?>

    </h5>
    <span class="w-select">
        <input class="input-small label span2" type="text" value="<?php echo $label; ?>" name="data[<?php echo $model; ?>][label]" />
            </span>
        <?php if($model=='Event' || $model=='Promotion' || $model=='TeamMember'){
/*	echo '<small class="w-select">';
echo $this->Form->input($model.'.number_of_display',array('options'=>array(1=>'One large '.str_replace('TeamMember', 'Staff Profile', Inflector::humanize($model)).' at a time',2=>'Two smaller '.str_replace('TeamMember', 'Staff Profile', Inflector::humanize($model)).'s at a time'),'label'=>'Display as','class'=>'selectpicker span2'));
echo '</small>';*/
             } ?>

        <a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-refresh">Delete image</a>
        </div>
    <?php } ?>

            </div>
            <?php $order++;
                if(isset($user_layout['Widget'][$order])){
                    $model=$user_layout['Widget'][$order]['name'];
                    $label=$user_layout['Widget'][$order]['label'];
                    $element=$user_layout['Widget'][$order]['element'];
                }
            else{
                $model='';
                $label='';
                $element='';
            }

    ?>
            <div class="<?php echo $class;?> droppable">
                <input class="input-small model" type="hidden" id="Widget-<?php echo $order ?>" name="data[Widget][<?php echo $order; ?>][name]" value="<?php echo $model; ?>" />

                <input class="input-small classname" type="hidden" value="box-small" name="data[Widget][<?php echo $order; ?>][class]" />

                <input class="input-small element" type="hidden" value="<?php echo $element; ?>" name="data[Widget][<?php echo $order; ?>][element]" />

                <?php if($model!=''){
        $model_list[]=$model;
        ?>
            <div id="<?php echo $model; ?>" class="ui-widget-content body-element">
        <h5 class="ui-widget-header"><?php echo $label ?>

    </h5>
    <span class="w-select">
        <input class="input-small label span2" type="text" value="<?php echo $label; ?>" name="data[<?php echo $model; ?>][label]" />
    </span>

        <?php if($model=='Event' || $model=='Promotion' || $model=='TeamMember'){
/*	echo '<small class="w-select">';
echo $this->Form->input($model.'.number_of_display',array('options'=>array(1=>'One large '.str_replace('TeamMember', 'Staff Profile', Inflector::humanize($model)).' at a time',2=>'Two smaller '.str_replace('TeamMember', 'Staff Profile', Inflector::humanize($model)).'s at a time'),'label'=>'Display as','class'=>'selectpicker span2'));
echo '</small>';*/
             } ?>

        <a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-refresh">Delete image</a>
        </div>
    <?php } ?>

            </div>
        <?php
        }
    if($col<count($boxes)){?>
        </div>
        <div class="col3 col<?php echo count($boxes);?>">
    <?php
    }

    $order++;
}
if(!empty($model_list)){?>
<script type="text/javascript">
var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image</a>";

    $(document).ready(function(){
<?php foreach ($model_list as $key => $model) {?>
    $('#trash #<?php echo $model;?> .ui-widget-header').html($('#gallery #<?php echo $model;?>').attr('label'));
    //,#trash #<?php echo $model;?> input.label
<?php } ?>
$('#gallery #<?php echo implode(",#gallery #",$model_list);?>').remove();

$('#trash #<?php echo implode(",#trash #",$model_list);?>').click(function( event ) {
            var $item = $( this ),
                $target = $( event.target );

            if ( $target.is( "a.ui-icon-trash" ) ) {
                deleteImage( $item );
            } else if ( $target.is( "a.ui-icon-zoomin" ) ) {
                viewLargerImage( $target );
            } else if ( $target.is( "a.ui-icon-refresh" ) ) {
                recycleImage( $item );
            }

            return false;
        });


$('.w-select').on( 'click', 'select', function () {
    $(this).focus();
});


$( "#ContactTimezone").change(function() {
    //alert(jQuery('#ContactTimezone').val());
 $.ajax({
    type: "POST",
    url: "<?php echo Router::url('/');?>Contacts/setTimezone/",
    data: { timezone: jQuery('#ContactTimezone').val()}
    })
    .done(function( msg ) {

    });
});


    });
    //$('#trash #<?php echo implode(",#trash #",$model_list);?>').append(trash_icon);

//$('.selectpicker').selectpicker();
</script>
<?php }
 ?>
</div>
<div id="ticker" class="span7 ">

                <div class="inner" style="text-align: center">
                <small style="display:"><i>TapSocial Ticker</i></small>
<?php if($design['Design']['show_ticker']){ ?>

 <div class="bool-slider true"> <div class="inset"> <div class="control"></div> </div>
<input name="show_ticker" type="hidden" value="1" />
</div>
<?php }
else{?>
 <div class="bool-slider false"> <div class="inset"> <div class="control"></div> </div>
<input name="show_ticker" type="hidden" value="0" /></div>
<?php } ?>
</div>

</div>
</form>
</div>

</div>

</div>


</div>

<script type="text/javascript">

</script>
