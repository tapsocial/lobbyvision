<div class="designs form">
<?php echo $this->Form->create('Design'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Design'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('logo');
		echo $this->Form->input('display_name');
		echo $this->Form->input('background');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Designs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
