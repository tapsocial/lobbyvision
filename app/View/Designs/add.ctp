<div class="designs form">
<?php echo $this->Form->create('Design',array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Let’s Get Started!'); ?><br>
		<small><i>There are a few essential elements for your digital sign that need to be entered as a basis.  Please upload your Logo, choose a textual Display Name (your company name or a welcome message)</i></small>
	</legend>
	<?php
		echo $this->Form->input('logo',array('type'=>'file'));
		echo '<br>';
		echo $this->Form->input('display_name');
		echo $this->Form->input('show_name',array('label'=>'Show In Digital Sign','class'=>''));
		//echo $this->Form->input('background',array('type'=>'file'));
		echo '<br>';
		echo $this->Form->input('template_id',array('type'=>'hidden','value'=>1));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
