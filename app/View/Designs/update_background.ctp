<?php
/*echo $this->Html->css('../colorpicker/css/colorpicker');
echo $this->Html->css('../colorpicker/css/layout');
echo $this->Html->script('../colorpicker/js/colorpicker');
echo $this->Html->script('../colorpicker/js/eye');
echo $this->Html->script('../colorpicker/js/utils');
echo $this->Html->script('../colorpicker/js/layout');
*/
echo $this->Html->css('spectrum');
echo $this->Html->script('spectrum');
echo $this->Html->script('cpexample');
$palettes = array(
    'dark'   => 'dark',
    'light'  => 'light',
);
?>
<script type="text/javascript">
    var hexc='';
</script>

<style>
    .row {margin-left: 0;}
    .well {padding: 4px 10px;}
    .well fieldset legend {padding: 0;}
    ul.nav-tabs a:active,a:focus {
        outline: none;
    }
    .background  ul.thumbnails {padding-left: 0;}
    #pictures .bg-link {min-height: 140px;}
    #current img {width: 100%;}

    .sp-picker-container,.sp-palette-container{
        clear: none;
    }
    .sp-picker-container{ width: 100px}
    .sp-button-container{display: none;}
    input[type="file"]{width: 90px;font-size: 1.3em}
    .browse-box input[type="submit"]{float: right;}

    a.thumnail {display: inline-block;}
</style>

<h2>Edit the display appearance.</h2>


<div class="background  well ">
    <fieldset>
        <legend><?php echo __('Select a background from our collection, or upload your own background image.'); ?></legend>
        <?
        // $authUser['Design']['background_type'] <- defines the active tab
        ?>

        <div class="row">
            <div class="span3 text-center" id="current">
                <h4>Current background</h4>
                <?php
                    $color='';
                    if(isset($this->request->data['Design']['background'])){

                        switch ($authUser['Design']['background_type']) {
                            case 1:
                                echo $this->Html->image('../uploads/background_image/src/thumb/small/'.$this->request->data['Design']['background'],array('align'=>'center'));
                                break;
                            case 2:
                                $color=$authUser['Design']['background'];
                                echo "<section style=\"background: {$authUser['Design']['background']}; display:block;width:225px;height:225px;\">&nbsp;</section>";
                                break;
                            case 3:
                                echo $this->Html->image("../files/videos-backgrounds/{$this->request->data['Design']['background']}.jpg", array('align'=>'center'));
                                break;
                            default:
                                throw new Exception('Background type not defined');
                        }
                    }
                ?>
            </div>
            <div class="span9">
                <ul class="nav nav-tabs" style="margin-bottom: 0;">
                    <li class="active"><a data-toggle="tab" href="#color">Solid Color</a></li>
                    <li><a data-toggle="tab" href="#upload">Custom Image</a></li>
                    <li><a data-toggle="tab" href="#pictures">Image Catalog</a></li>
                    <li><a data-toggle="tab" href="#video">Animated Background</a></li>
                </ul>
                <div class="tab-content" style="background: white; padding: 10px;">
                    <div class="tab-pane active" id="color">
                        <?php echo $this->Form->create('BackgroundImage',array('type'=>'file')); ?>
                        <?php echo $this->Form->input('id');?>
                        <?php $color=$authUser['Design']['background']; ?>
                        <input id="full"/>
                    </div>
                    <div class="tab-pane" id="upload">

                        <?php echo $this->Form->create('BackgroundImage',array('type'=>'file')); ?>
                        <?php echo $this->Form->input('id');?>
                        <section class="control-group browse-box well" style="">
                            <br /><br />

                            <?php   echo $this->Form->input('src',array('type'=>'file','label'=>false,'div'=>false,'style'=>'display:none;','onchange'=>'document.getElementById("BackgroundImageSrc").value=this.value.substring(this.value.lastIndexOf("\\")+1);'));
                                //echo $this->Form->input('color',array('id'=>'full'));
                            ?>
                            <input type="button" class="btn btn-info" onclick="document.getElementById('BackgroundImageSrc').click();" value="Browse File" />
                            <?php $options = array(
                                'label' => 'Upload',
                                'value' => 'Upload',
                                'class'=>'btn',
                                'div' => false
                            );
                            echo $this->Form->end($options);
                            ?>
                        </section>

                    </div>
                    <div class="tab-pane" id="pictures">
                        <ul class="thumbnails" style="max-height: 370px;">
                        <?php foreach ($backgroundImages as $backgroundImage) {?>
                            <?php if(isset($this->request->data['Design']['background']) && $this->request->data['Design']['background']==$backgroundImage['BackgroundImage']['src'])  continue;?>
                            <li class="span3">
                                <section><?php echo $backgroundImage['BackgroundImage']['name']; ?>&nbsp;</section>
                                <a class="thumbnail bg-link" href="#" onclick="set_image_link('<?php echo $backgroundImage['BackgroundImage']['src'];?>', $(this)); return false;">
                                    <?=$this->Html->image('../uploads/background_image/src/thumb/small/'.$backgroundImage['BackgroundImage']['src'],array('style'=>'max-height:140px'));?>
                                </a>
                                <?php if($backgroundImage['BackgroundImage']['user_id'] >0 &&
                                $backgroundImage['BackgroundImage']['user_id']==$authUser['id']){?>
                                <?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove icon-remove"></span> Remove ', array('action' => 'bg_delete', $backgroundImage['BackgroundImage']['id']), array
                                    ('escape'=>false,'class'=>'btn btn-default btn-sm'), __('Are you sure you want to delete this background image?', $backgroundImage['BackgroundImage']['id'])); ?>
                               <?php }?>
                        </li>
                        <?php } ?>
                        </ul>
                    </div>
                    <div class="tab-pane" id="video">

                        <ul class="thumbnails" style="max-height: 370px;">
                        <?php foreach ($backgroundVideos as $video) :?>
                            <li class="span3" style="margin-right:3px;">
                                <section><?=$video['BackgroundVideo']['name']?></section>
                                <a class="thumbnail bg-link" href="#" onclick="return setBackground(3, '<?=$video['BackgroundVideo']['video'];?>', $(this));">
                                    <?=$this->Html->image("../uploads/background_video/screenshot/{$video['BackgroundVideo']['screenshot']}", array('style'=>'max-height:140px'));?>
                                </a>
                            </li>
                        <?php endforeach ?>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

    </fieldset>
</div>

<div  class="color-palette well ">
    <fieldset>
        <legend>Select color palette.</legend>
        <ul id="pallete" class="thumbnails" style="padding-right:0px">
            <? foreach ($palettes as $theme) :?>
                <li>
                    <a href="#" data-theme="<?=$theme?>" class="thumnail theme <?php if($this->request->data['Design']['shape']==$theme) echo 'selected clicked';?>">
                        <img src="/img/display/screenshot-theme-<?=$theme?>.png" style="width: 400px;" />
                    </a>
                </li>
            <? endforeach; ?>
        </ul>
    </fieldset>
</div>

<?php echo $this->Form->create('Design'); ?>
    <?php echo $this->Form->input('id');?>

    <?php
        echo $this->Form->input('shape',array('type'=>'hidden','label'=>false));
        echo $this->Form->input('background',array('type'=>'hidden','label'=>false));
        echo $this->Form->input('background_type',array('type'=>'hidden','label'=>false));
        //echo $this->Form->input('color',array('id'=>'full'));
    ?>
    <div class="text-center">
        <button type="submit" class="btn btn-large btn-primary">save</button>
    </div>
<?php echo $this->Form->end(); ?>


<script type="text/javascript">
<?php
if($this->request->data['Design']['background_type']==2){
    ?>
    var hexc= '<?php echo $this->request->data['Design']['background'];?>';

<?php } ?>


(function($){
    //$('a.bg-link').click()
    $('input[type=file]').bootstrapFileInput();


});

function setBackground(type, src, $obj) {
    $('#DesignBackgroundType').val(type);
    $('#DesignBackground').val(src);
    return false;
}

$(function () {
    $('.theme').click(function() {
        $('.theme').removeClass('clicked selected');
        $(this).addClass('clicked selected');
        var theme = $(this).data('theme');
        $('#DesignShape').val(theme);
        return false;
    });
});

function set_image_link(src, obj){
    $('.bg-link').removeClass('label-important');
    obj.addClass('label-important');
    $('#DesignBackgroundType').val(1);
    $('#DesignBackground').val(src);
}
function pick_color(hex){
    $('#DesignBackgroundType').val(2);
    $('#DesignBackground').val(hex);
}


</script>