<div class="designs view">
<h4><?php  echo __('Choose Your Layout'); ?></h4>   


<div class="ui-widget ui-helper-clearfix">

<div class="layout_chooser">
  <ul class="nav nav-pills">
  	<?php foreach($layouts as $layout){
  		$class='';
  		if($layout_id==$layout['Layout']['id']){
  			$class='active';
  			$active_layout=$layout;
  		}
  			
  		echo '<li class="'.$class.'">';
  		echo $this->Html->link($this->Html->image('../uploads/layout/image/'.$layout['Layout']['image']),array('controller'=>'Designs','action'=>'index',$layout['Layout']['id']),array('escape'=>false));

  		echo '</li>';
  	} ?>
 

    </ul>
</div>
<h4><?php  echo __('Drag & Drop Widgets to Your Display'); ?></h4> 
<div id="gallery" class="gallery ui-helper-reset ui-helper-clearfix">


	<div id="Event" class="ui-widget-content body-element" label="Event">
		<h5 class="ui-widget-header">Event</h5>
		
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
	</div>
	<div id="TeamMember" class="ui-widget-content body-element" label="Team Members">
		<h5 class="ui-widget-header">Team Members</h5>
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
	</div>
	<div id="Promotion"  class="ui-widget-content body-element" label="Sales & Promotions">

		<h5 class="ui-widget-header">Sales & Promotions</h5>
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
	</div>
	<?php if($design['Design']['logo']!=''){ ?>
	<div id="Logo"  class="ui-widget-content header-element" label="Company logo">
		<div class="inner">
		<?php		
		if(isset($design['Design']['logo']))
		echo $this->Html->image('../uploads/design/logo/thumb/small/'.$design['Design']['logo'],array('style'=>'max-width:'));
		?>
	</div>
	<h5><small><i>Company logo</i></small></h5>
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
	</div>
	<?php } ?>
		<div id="CompanyName"  class="ui-widget-content header-element" label="Company name">
		<div class="inner"><h5 class="ui-widget-header"><?php echo h($design['Design']['display_name']); ?>
		<small><i>Company name</i></small>
	</h5></div>

		
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
	</div>
	<div id="Ticker"  class="ui-widget-content ticker-element" label="TapSocial Ticker">
		<h5 class="ui-widget-header">TapSocial Ticker</h5>
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
	</div>
	<div id="News"  class="ui-widget-content body-element" label="News Headlines">
		<h5 class="ui-widget-header">News Headlines</h5>
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
	</div>
	<div id="Weather"  class="ui-widget-content body-element" label="Weather">
		<h5 class="ui-widget-header">Weather</h5>
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-trash">Delete image</a>
	</div>
	
</div>
<h5 class="center" id="loader"><small class="success" >Saving..</small> &nbsp;<a class="btn btn-small pull-right btn-success" href="#" onclick="saveWidget();return false;" >Save Display Layout</a></h5>

	<?php echo $this->Form->create('UserLayout');?>
<div id="trash" class="ui-widget-content ui-state-default">
<?php $model_list=array(); ?>
    <div class="navbar">
    <div class="navbar-inner">
    <ul class="nav">
    	<li class="brand droppable">
<?php if($design['Design']['show_logo'] && $design['Design']['logo']!=''){ ?>
    		<small style="display:none">Logo</small>
    		<?php $model_list[]='Logo'; ?>
    <div id="Logo" class="ui-widget-content header-element ui-draggable" >
		<div class="inner">
		<?php if(isset($design['Design']['logo']))
		echo $this->Html->image('../uploads/design/logo/thumb/small/'.$design['Design']['logo'],array('style'=>'max-width:')); ?>
		</div>
	<h5><small><i>Company logo</i></small></h5>	
		 <div class="bool-slider true"> <div class="inset"> <div class="control"></div> </div> </div> 
	<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-refresh">Delete image</a>
</div>   	
<?php }
else{}

	{?>
<small style="display:">Logo</small>

 <div class="bool-slider true"> <div class="inset"> <div class="control"></div> </div> </div> 
<?php } ?>

    	</li>
    <li><div class="company name droppable"  href="#">
<?php if($design['Design']['show'] && $design['Design']['display_name']!=''){ ?>
    		
    		<?php $model_list[]='CompanyName'; ?>
    <div id="CompanyName" class="ui-widget-content body-element ui-draggable" >
		<div class="inner">
		<h5 class="ui-widget-header"><?php echo h($design['Design']['display_name']); ?>
		<small><i>Company name</i></small>
	</h5>
		</div>
	
		
	<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-refresh">Delete image</a>
</div>   	
<?php }
else{?>
<small>Company name</small>
<?php } ?>

    	</div></li>
    
    </ul>
    <ul class="nav pull-right">
    	<li class="" ><a sty="line-height:60px;" href="#">Digital Clock</a></li></ul>
    </div>
    </div>


	
    <?php echo $this->Form->input('layout_id',array('type'=>'hidden','value'=>$active_layout['Layout']['id'])) ?>

    	<?php $boxes=explode('x', $active_layout['Layout']['name']);?>
<div id="box">
<div class="col3">
<?php 
$col=0;
$order=0;
foreach($boxes as $box){


	if($box==1){
		$class='box-long box';
		$col++;
	if(isset($user_layout['Widget'][$order]))
		$model=$user_layout['Widget'][$order]['name'];
	else
		$model='';
		?>
		<div class="<?php echo $class;?> droppable">
			<input class="input-small model" type="hidden" id="Widget-<?php echo $order ?>" name="data[Widget][<?php echo $order; ?>][name]" value="<?php echo $model; ?>" />
			<input class="input-small" type="hidden" value="box-long" name="data[Widget][<?php echo $order; ?>][class]" />
	<?php if($model!=''){ 
		$model_list[]=$model;
		?>
			<div id="<?php echo $model; ?>" class="ui-widget-content body-element">
		<h5 class="ui-widget-header"><?php echo $model ?></h5>		
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-refresh">Delete image</a>
		</div>
	<?php } ?>
		</div>
<?php }elseif($box==2){
			$class='box-small box';
				if(isset($user_layout['Widget'][$order]))
		$model=$user_layout['Widget'][$order]['name'];
				else
		$model='';
			?>
			<div class="<?php echo $class;?> droppable">
				<input class="input-small model" type="hidden" id="Widget-<?php echo $order ?>" name="data[Widget][<?php echo $order; ?>][name]" value="<?php echo $model; ?>" />
				<input class="input-small" type="hidden" value="box-small" name="data[Widget][<?php echo $order; ?>][class]" />
				<?php if($model!=''){ 
		$model_list[]=$model;
		?>
			<div id="<?php echo $model; ?>" class="ui-widget-content body-element">
		<h5 class="ui-widget-header"><?php echo $model ?></h5>		
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-refresh">Delete image</a>
		</div>
	<?php } ?>

			</div>
			<?php $order++; 
				if(isset($user_layout['Widget'][$order]))
				$model=$user_layout['Widget'][$order]['name'];
			else
				$model='';
	?>
			<div class="<?php echo $class;?> droppable">
				<input class="input-small model" type="hidden" id="Widget-<?php echo $order ?>" name="data[Widget][<?php echo $order; ?>][name]" value="<?php echo $model; ?>" />

				<input class="input-small" type="hidden" value="box-small" name="data[Widget][<?php echo $order; ?>][class]" />
				<?php if($model!=''){ 
		$model_list[]=$model;
		?>
			<div id="<?php echo $model; ?>" class="ui-widget-content body-element">
		<h5 class="ui-widget-header"><?php echo $model ?></h5>		
		
		<a href="link/to/trash/script/when/we/have/js/off" title="Delete this image" class="ui-icon ui-icon-refresh">Delete image</a>
		</div>
	<?php } ?>

			</div>
		<?php
		}
	if($col<3){?>
		</div>
		<div class="col3">
	<?php
	}
	
	$order++;
}
if(!empty($model_list)){?>
<script type="text/javascript">
var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image</a>";

	$(document).ready(function(){
<?php foreach ($model_list as $key => $model) {?>
	$('#trash #<?php echo $model;?> .ui-widget-header').html($('#gallery #<?php echo $model;?>').attr('label'));
<?php } ?>
$('#gallery #<?php echo implode(",#gallery #",$model_list);?>').remove();

$('#trash #<?php echo implode(",#trash #",$model_list);?>').click(function( event ) {
			var $item = $( this ),
				$target = $( event.target );

			if ( $target.is( "a.ui-icon-trash" ) ) {
				deleteImage( $item );
			} else if ( $target.is( "a.ui-icon-zoomin" ) ) {
				viewLargerImage( $target );
			} else if ( $target.is( "a.ui-icon-refresh" ) ) {
				recycleImage( $item );
			}

			return false;
		});

	});
	//$('#trash #<?php echo implode(",#trash #",$model_list);?>').append(trash_icon);


</script>
<?php }
 ?>
</div>
<div id="ticker" class="span7 droppable"></div>
</form>
</div>

</div>

</div>


</div>
