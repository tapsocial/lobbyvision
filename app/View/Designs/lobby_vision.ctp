<div class="designs view">
<h2><?php  echo __('Design'); ?></h2>
	<dl>
		
		<dt><?php echo __('Logo'); ?></dt>
		<dd>
<?php		if(isset($design['Design']['logo']))
		echo $this->Html->image('../uploads/design/logo/thumb/small/'.$design['Design']['logo'],array('style'=>'width:125px'));
		?>

			&nbsp;
		</dd>
		<dt><?php echo __('Display Name'); ?></dt>
		<dd>
			<?php echo h($design['Design']['display_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Background'); ?></dt>
		<dd>
				<?php 
$color='';
	if(isset($design['Design']['background'])){

		if($design['Design']['background_type']==1){
			echo $this->Html->image('../uploads/background_image/src/thumb/small/'.$design['Design']['background'],array('style'=>'width:125px'));
		}elseif($design['Design']['background_type']==2){
			$color=$design['Design']['background'];
			?>
		<section style="background-color:<?php echo $design['Design']['background'];?>;display:block;width:125px;height:100px;">&nbsp;</section>
			
		<?php }
		
	}
	
	?>

			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($design['Design']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($design['Design']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
