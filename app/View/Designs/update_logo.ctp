<h2>Upload company's logo.</h2>
<?php echo $this->Form->create('Design',array('type'=>'file')); ?>
<fieldset>
    <div class="control-group">
        <div class="controls">
        <?php
            echo $this->Form->input('id');

            if(isset($this->request->data['Design']['logo'])) {
                echo __('<p>Current Logo</p>');
                echo $this->Html->image('../uploads/design/logo/thumb/small/'.$this->request->data['Design']['logo']);
            }
        ?>
        </div>
    </div>
    <?php
        echo $this->Form->input('logo',array('type'=>'file','label' => false));
    ?>
</fieldset>
<div class="no-wrapper">
    <?php echo $this->Form->end(__('Update')); ?>
</div>
