<div class="widgetIcons form">
<?php echo $this->Form->create('WidgetIcon',array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Edit Widget Icon'); ?></legend>
	<?php
		echo $this->Form->input('id');?>
	<div class="control-group">
			<label></label>
		<?php if(!empty($this->request->data['WidgetIcon']['image']))
		echo $this->Html->image('../uploads/widget_icon/image/thumb/small/'.$this->request->data['WidgetIcon']['image']);?>
	</div>
		<?php
		echo $this->Form->input('image',array('type'=>'file'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('WidgetIcon.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('WidgetIcon.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Widget Icons'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Add Icon'), array('action' => 'add')); ?></li>
	</ul>
</div>
