<div class="widgetIcons form">
<?php echo $this->Form->create('WidgetIcon',array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Add Widget Icon'); ?></legend>
	<?php
		echo $this->Form->input('image',array('type'=>'file'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Widget Icons'), array('action' => 'index')); ?></li>
	</ul>
</div>
