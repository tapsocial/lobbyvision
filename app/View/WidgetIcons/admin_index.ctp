<div class="widgetIcons index">
	<h2><?php echo __('Widget Icons'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($widgetIcons as $widgetIcon): ?>
	<tr>
		<td><?php echo h($widgetIcon['WidgetIcon']['id']); ?>&nbsp;</td>
		<td><?php 
echo $this->Html->image('../uploads/widget_icon/image/thumb/small/'.$widgetIcon['WidgetIcon']['image'],array('style'=>'max-width:65px','class'=>''));

		 ?>&nbsp;</td>
		<td class="actions">
			
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $widgetIcon['WidgetIcon']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $widgetIcon['WidgetIcon']['id']), null, __('Are you sure you want to delete # %s?', $widgetIcon['WidgetIcon']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('New Widget Icon'), array('action' => 'add')); ?></li>
	</ul>
</div>
