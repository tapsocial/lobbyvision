<?
/* @var $this View */
$widgetType = $this->request->data['WidgetMaster']['name'];
?>
<style>
    .form-horizontal .controls {margin-left: 16px; width: 75%;}
</style>

<h2><?php echo __('Edit Widget'); ?></h2>
<hr />
<?php echo $this->Form->create('WidgetInstance', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
    <fieldset>
        <?php echo $this->Form->input('id', array('class' => 'span12')); ?>
        <div class="control-group">
            <?php echo $this->Form->label('name', 'Name', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('name', array('class' => 'span12')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

        <div class="control-group">
            <?php echo $this->Form->label('name', 'Show title', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('show_title', array('class' => 'span1')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

        <div class="control-group">
            <?php echo $this->Form->label('name', 'Opacity', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('opacity', array(
                    'class'   => 'span2',
                    'options' => ['0' => 'clear', '30' => 'light', '60' => 'medium',  '100' => 'solid'],
                )); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

        <? if (count($this->request->data) > 2) :?>
            <?=$this->Form->input('WidgetInstance'.ucfirst($widgetType).'.id'); ?>
        <? endif; ?>
        <?=$this->element("widgets/{$widgetType}_edit"); ?>
    </fieldset>
<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
