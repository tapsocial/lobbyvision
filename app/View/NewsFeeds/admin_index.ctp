<div class="newsFeeds index">
	<h2><?php echo __('News Feeds'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('news_category_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('feed_url'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($newsFeeds as $newsFeed): ?>
	<tr>
		<td><?php echo h($newsFeed['NewsFeed']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($newsFeed['NewsCategory']['name'], array('controller' => 'news_categories', 'action' => 'view', $newsFeed['NewsCategory']['id'])); ?>
		</td>
		<td><?php echo h($newsFeed['NewsFeed']['name']); ?>&nbsp;</td>
		<td><?php echo h($newsFeed['NewsFeed']['feed_url']); ?>&nbsp;</td>
		<td><?php echo h($newsFeed['NewsFeed']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $newsFeed['NewsFeed']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $newsFeed['NewsFeed']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $newsFeed['NewsFeed']['id']), null, __('Are you sure you want to delete # %s?', $newsFeed['NewsFeed']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New News Feed'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List News Categories'), array('controller' => 'news_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News Category'), array('controller' => 'news_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
