<div class="newsFeeds form">
<?php echo $this->Form->create('NewsFeed'); ?>
	<fieldset>
		<legend><?php echo __('Add News Feed'); ?></legend>
	<?php
		echo $this->Form->input('news_category_id');
		echo $this->Form->input('name');
		echo $this->Form->input('feed_url');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List News Feeds'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List News Categories'), array('controller' => 'news_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News Category'), array('controller' => 'news_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
