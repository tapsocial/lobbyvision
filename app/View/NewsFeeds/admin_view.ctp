<div class="newsFeeds view">
<h2><?php echo __('News Feed'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($newsFeed['NewsFeed']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('News Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($newsFeed['NewsCategory']['name'], array('controller' => 'news_categories', 'action' => 'view', $newsFeed['NewsCategory']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($newsFeed['NewsFeed']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Feed Url'); ?></dt>
		<dd>
			<?php echo h($newsFeed['NewsFeed']['feed_url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($newsFeed['NewsFeed']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit News Feed'), array('action' => 'edit', $newsFeed['NewsFeed']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete News Feed'), array('action' => 'delete', $newsFeed['NewsFeed']['id']), null, __('Are you sure you want to delete # %s?', $newsFeed['NewsFeed']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List News Feeds'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News Feed'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List News Categories'), array('controller' => 'news_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News Category'), array('controller' => 'news_categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
