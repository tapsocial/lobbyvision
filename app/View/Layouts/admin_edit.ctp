<div class="layouts form">
<?php echo $this->Form->create('Layout',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Layout'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('image',array('type' => 'file'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Layout.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Layout.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Layouts'), array('action' => 'index')); ?></li>
	</ul>
</div>
