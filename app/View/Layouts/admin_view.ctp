<div class="layouts view">
<h2><?php echo __('Layout'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($layout['Layout']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($layout['Layout']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($layout['Layout']['image']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Layout'), array('action' => 'edit', $layout['Layout']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Layout'), array('action' => 'delete', $layout['Layout']['id']), null, __('Are you sure you want to delete # %s?', $layout['Layout']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Layouts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Layout'), array('action' => 'add')); ?> </li>
	</ul>
</div>
