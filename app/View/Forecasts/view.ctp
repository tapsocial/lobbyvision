<div class="forecasts view">
<h2><?php echo __('Forecast'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($forecast['Forecast']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Weather Underground'); ?></dt>
		<dd>
			<?php echo $this->Html->link($forecast['WeatherUnderground']['user_id'], array('controller' => 'weather_undergrounds', 'action' => 'view', $forecast['WeatherUnderground']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Forecast'); ?></dt>
		<dd>
			<?php echo h($forecast['Forecast']['forecast']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Url'); ?></dt>
		<dd>
			<?php echo h($forecast['Forecast']['url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($forecast['Forecast']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($forecast['Forecast']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Forecast'), array('action' => 'edit', $forecast['Forecast']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Forecast'), array('action' => 'delete', $forecast['Forecast']['id']), null, __('Are you sure you want to delete # %s?', $forecast['Forecast']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Forecasts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Forecast'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Weather Undergrounds'), array('controller' => 'weather_undergrounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Weather Underground'), array('controller' => 'weather_undergrounds', 'action' => 'add')); ?> </li>
	</ul>
</div>
