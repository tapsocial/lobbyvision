<div class="forecasts form">
<?php echo $this->Form->create('Forecast'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Forecast'); ?></legend>
	<?php
		echo $this->Form->input('weather_underground_id');
		echo $this->Form->input('forecast');
		echo $this->Form->input('url');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Forecasts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Weather Undergrounds'), array('controller' => 'weather_undergrounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Weather Underground'), array('controller' => 'weather_undergrounds', 'action' => 'add')); ?> </li>
	</ul>
</div>
