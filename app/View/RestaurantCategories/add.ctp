<h2><?php echo __('Add Restaurant Category'); ?></h2>
<?php echo $this->Form->create('RestaurantCategory', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
    <fieldset>
        <div class="control-group">
            <?php echo $this->Form->label('name', 'Category Name', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('name', array('class' => 'span12', 'placeholder' => '(e.g. Sides)')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

        <div class="control-group">
            <?php echo $this->Form->label('name', 'Service periods', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('RestaurantPeriod', ['multiple' => 'checkbox']);?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

    </fieldset>
    <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
