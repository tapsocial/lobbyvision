<script>
$(function() {
    $('.sortable').sortable().disableSelection();
    $('form').submit(function() {
        var sort = $('.sortable').sortable('serialize');
        $('#RestaurantCategorySort').val(sort);
        return true;
    });
});
</script>
<style>
    .sortable { list-style-type: none; margin: 0; padding: 0;  margin-left: 60px; cursor: move;}
    .sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
    .sortable li span { position: absolute; margin-left: -1.3em; }
</style>
<h2><?php echo __('Edit category.'); ?></h2>
<?php echo $this->Form->create('RestaurantCategory', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
    <fieldset>
        <?php echo $this->Form->input('id', array('class' => 'span12')); ?>
        <div class="control-group">
            <?php echo $this->Form->label('name', 'Category Name', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('name', array('class' => 'span12', 'placeholder' => '(e.g. Sides)')); ?>
            </div><!-- .controls -->
        </div><!-- .control-group -->

        <div class="control-group">
            <?php echo $this->Form->label('name', 'Service periods', array('class' => 'control-label'));?>
            <div class="controls">
                <?php echo $this->Form->input('RestaurantPeriod', ['multiple' => 'checkbox']);?>
            </div><!-- .controls -->
        </div><!-- .control-group -->
        <div class="control-group">
            <?php echo $this->Form->input('sort', ['type' => 'hidden']);?>
            <h3>Sort menu items:</h3>
            <ul class="sortable">
                <? foreach ($this->request->data['RestaurantMenuItem'] as $item) :?>
                    <li class="_btn_ btn-info" id="sort_order=<?=$item['id'] ?>">
                        <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                        <?=$item['name'] ?>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>
    </fieldset>
    <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
