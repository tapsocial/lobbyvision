<div class="weatherPanels form">
<?php echo $this->Form->create('WeatherPanel'); ?>
	<fieldset>
		<legend><?php echo __('Add Weather Panel'); ?></legend>
	<?php
		echo $this->Form->input('panel_name');
		echo $this->Form->input('code');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Html->link(__('List Weather Panels'), array('action' => 'index')); ?></li>
		
		
	</ul>
</div>
