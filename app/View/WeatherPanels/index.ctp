<div class="weatherPanels index">
	<h2><?php echo __('Weather Panels'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('Panel Name'); ?></th>
			<th><?php echo $this->Paginator->sort('code'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($weatherPanels as $weatherPanel): ?>
	<tr>
		<td><?php echo h($weatherPanel['WeatherPanel']['id']); ?>&nbsp;</td>
		<td><?php echo h($weatherPanel['WeatherPanel']['Panel Name']); ?>&nbsp;</td>
		<td><?php echo h($weatherPanel['WeatherPanel']['code']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $weatherPanel['WeatherPanel']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $weatherPanel['WeatherPanel']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $weatherPanel['WeatherPanel']['id']), null, __('Are you sure you want to delete # %s?', $weatherPanel['WeatherPanel']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Weather Panel'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Weather Undergrounds'), array('controller' => 'weather_undergrounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Weather Underground'), array('controller' => 'weather_undergrounds', 'action' => 'add')); ?> </li>
	</ul>
</div>
