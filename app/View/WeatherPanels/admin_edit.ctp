<div class="weatherPanels form">
<?php echo $this->Form->create('WeatherPanel'); ?>
	<fieldset>
		<legend><?php echo __('Edit Weather Panel'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('panel_name');
		echo $this->Form->input('code');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('WeatherPanel.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('WeatherPanel.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Weather Panels'), array('action' => 'index')); ?></li>
		
	</ul>
</div>
