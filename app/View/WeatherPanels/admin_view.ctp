<div class="weatherPanels view">
<h2><?php echo __('Weather Panel'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($weatherPanel['WeatherPanel']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Panel Name'); ?></dt>
		<dd>
			<?php echo h($weatherPanel['WeatherPanel']['Panel Name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($weatherPanel['WeatherPanel']['code']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Weather Panel'), array('action' => 'edit', $weatherPanel['WeatherPanel']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Weather Panel'), array('action' => 'delete', $weatherPanel['WeatherPanel']['id']), null, __('Are you sure you want to delete # %s?', $weatherPanel['WeatherPanel']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Weather Panels'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Weather Panel'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Weather Undergrounds'), array('controller' => 'weather_undergrounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Weather Underground'), array('controller' => 'weather_undergrounds', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Weather Undergrounds'); ?></h3>
	<?php if (!empty($weatherPanel['WeatherUnderground'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('Location'); ?></th>
		<th><?php echo __('Url'); ?></th>
		<th><?php echo __('Called'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($weatherPanel['WeatherUnderground'] as $weatherUnderground): ?>
		<tr>
			<td><?php echo $weatherUnderground['id']; ?></td>
			<td><?php echo $weatherUnderground['user_id']; ?></td>
			<td><?php echo $weatherUnderground['country_id']; ?></td>
			<td><?php echo $weatherUnderground['location']; ?></td>
			<td><?php echo $weatherUnderground['url']; ?></td>
			<td><?php echo $weatherUnderground['called']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'weather_undergrounds', 'action' => 'view', $weatherUnderground['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'weather_undergrounds', 'action' => 'edit', $weatherUnderground['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'weather_undergrounds', 'action' => 'delete', $weatherUnderground['id']), null, __('Are you sure you want to delete # %s?', $weatherUnderground['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Weather Underground'), array('controller' => 'weather_undergrounds', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
