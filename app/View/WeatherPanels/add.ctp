<div class="weatherPanels form">
<?php echo $this->Form->create('WeatherPanel'); ?>
	<fieldset>
		<legend><?php echo __('Add Weather Panel'); ?></legend>
	<?php
		echo $this->Form->input('Panel Name');
		echo $this->Form->input('code');
		echo $this->Form->input('WeatherUnderground');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Weather Panels'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Weather Undergrounds'), array('controller' => 'weather_undergrounds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Weather Underground'), array('controller' => 'weather_undergrounds', 'action' => 'add')); ?> </li>
	</ul>
</div>
