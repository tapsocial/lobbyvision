<div class="newsCategories view">
<h2><?php echo __('News Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($newsCategory['NewsCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($newsCategory['NewsCategory']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($newsCategory['NewsCategory']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit News Category'), array('action' => 'edit', $newsCategory['NewsCategory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete News Category'), array('action' => 'delete', $newsCategory['NewsCategory']['id']), null, __('Are you sure you want to delete # %s?', $newsCategory['NewsCategory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List News Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List News Feeds'), array('controller' => 'news_feeds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News Feed'), array('controller' => 'news_feeds', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related News Feeds'); ?></h3>
	<?php if (!empty($newsCategory['NewsFeed'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('News Category Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Feed Url'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($newsCategory['NewsFeed'] as $newsFeed): ?>
		<tr>
			<td><?php echo $newsFeed['id']; ?></td>
			<td><?php echo $newsFeed['news_category_id']; ?></td>
			<td><?php echo $newsFeed['name']; ?></td>
			<td><?php echo $newsFeed['feed_url']; ?></td>
			<td><?php echo $newsFeed['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'news_feeds', 'action' => 'view', $newsFeed['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'news_feeds', 'action' => 'edit', $newsFeed['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'news_feeds', 'action' => 'delete', $newsFeed['id']), null, __('Are you sure you want to delete # %s?', $newsFeed['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New News Feed'), array('controller' => 'news_feeds', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
