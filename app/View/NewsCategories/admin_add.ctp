<div class="newsCategories form">
<?php echo $this->Form->create('NewsCategory'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add News Category'); ?></legend>
	<?php
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List News Categories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List News Feeds'), array('controller' => 'news_feeds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News Feed'), array('controller' => 'news_feeds', 'action' => 'add')); ?> </li>
	</ul>
</div>
