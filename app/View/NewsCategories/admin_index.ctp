<div class="newsCategories index">
	<h2><?php echo __('News Categories'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($newsCategories as $newsCategory): ?>
	<tr>
		<td><?php echo h($newsCategory['NewsCategory']['id']); ?>&nbsp;</td>
		<td><?php echo h($newsCategory['NewsCategory']['name']); ?>&nbsp;</td>
		<td><?php echo h($newsCategory['NewsCategory']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $newsCategory['NewsCategory']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $newsCategory['NewsCategory']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $newsCategory['NewsCategory']['id']), null, __('Are you sure you want to delete # %s?', $newsCategory['NewsCategory']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New News Category'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List News Feeds'), array('controller' => 'news_feeds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News Feed'), array('controller' => 'news_feeds', 'action' => 'add')); ?> </li>
	</ul>
</div>
