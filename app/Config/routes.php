<?php
//Router::connect('/', array('controller' => 'users', 'action' => 'login'));
Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));

Router::connect('/launch',    ['controller' => 'display', 'action' => 'index']);
Router::connect('/configure', ['controller' => 'display', 'action' => 'edit']);

/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));


/* Paypal IPN plugin */
Router::connect('/paypal_ipn/process', array('plugin' => 'paypal_ipn', 'controller' => 'instant_payment_notifications', 'action' => 'process'));
/* Optional Route, but nice for administration */
Router::connect('/paypal_ipn/*', array('admin' => 'true', 'plugin' => 'paypal_ipn', 'controller' => 'instant_payment_notifications', 'action' => 'index'));
/* End Paypal IPN plugin */


/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
    CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
    require CAKE . 'Config' . DS . 'routes.php';
