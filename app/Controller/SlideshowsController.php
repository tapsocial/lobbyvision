<?php
App::uses('AppController', 'Controller');
/**
 * Slideshows Controller
 *
 * @property Slideshow $Slideshow
 */
class SlideshowsController extends AppController {

public $components = array('Paginator');
public $paginate = array(
        'limit' => 10,
        'order' => array(
            'Slideshow.id' => 'asc'
        )
    );

  function beforeFilter() {
 	parent::beforeFilter();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Slideshow->recursive = 0;

		 // we prepare our query, the cakephp way!
    $this->paginate = array(
        'conditions' => array('Slideshow.user_id' => $this->user['id']),
        'limit' => 10,
        'order' => array('id' => 'desc')
    );
    $slideshows = $this->paginate('Slideshow',array('Slideshow.user_id' => $this->user['id']));
    // we are using the 'User' model



		$this->set('slideshows', $slideshows);
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Slideshow->create();

			$this->request->data['Slideshow']['user_id']=$this->user['id'];
			$this->request->data['Slideshow']['dir']='slideshow';
			$this->Slideshow->validate['image'] = array(
			 	'Empty' => array('check' => true),
			        'HttpPost' => array(
			            'check' => true
			        )
			    );

			if ($this->Slideshow->save($this->request->data)) {
				$this->Session->setFlash(__('The slideshow has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The slideshow could not be saved. Please, try again.'));
			}
		}
		$users = $this->Slideshow->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Slideshow->exists($id)) {
			throw new NotFoundException(__('Invalid slideshow'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$this->Slideshow->validate['image'] = array(
			 	'Empty' => array('check' => true),
			        'HttpPost' => array(
			            'check' => true
			        )
			    );


			if ($this->Slideshow->save($this->request->data)) {
				$this->Session->setFlash(__('The slideshow has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The slideshow could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Slideshow.' . $this->Slideshow->primaryKey => $id));
			$this->request->data = $this->Slideshow->find('first', $options);
		}
		$users = $this->Slideshow->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Slideshow->id = $id;
		if (!$this->Slideshow->exists()) {
			throw new NotFoundException(__('Invalid slideshow'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Slideshow->delete()) {
			$this->Session->setFlash(__('Slideshow deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Slideshow was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}
}
