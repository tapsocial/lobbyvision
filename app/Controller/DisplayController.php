<?php
App::uses('AppController', 'Controller');

/**
 * Display Controller
 *
 * @property WidgetInstance $WidgetInstance
 * @property WidgetMaster   $WidgetMaster
 */
class DisplayController extends AppController
{
    public $uses = ['WidgetInstance', 'WidgetMaster'];

    public function index()
    {
        $userId  = $this->user['id'];
        $widgets = $this->WidgetInstance->getByUser($userId);
        $this->set(compact('widgets'));
    }

    public function edit()
    {
        $userId  = $this->user['id'];
        if ($this->request->is('post') || $this->request->is('put')) {
            $widgets = $this->request->data['Display']['serialized'];
            $widgets = json_decode($widgets, true);
            $this->WidgetInstance->deleteRemoved($userId, $widgets);
            foreach ($widgets as $widget) {
                if (!empty($widget['id'])) {
                    $this->WidgetInstance->id = $widget['id'];
                } else {
                    $widget['user_id'] = $userId;
                    $this->WidgetInstance->create();
                }
                $save = $this->WidgetInstance->save($widget);
                if (!$save) {
                    throw new \Exception('Subwidget save was broken');
                }
            }
            $this->Session->setFlash(__('Display updated successfully.'));
            $this->redirect(['action' => 'edit']);
        }
        $masters = $this->WidgetMaster->find('all', ['contain' => false, 'order' => 'name']);
        foreach($masters as $key => $master) {
            $subWidget = 'WidgetInstance'.ucfirst($master['WidgetMaster']['name']);
            $masters[$key]['WidgetMaster'] += [
                'singleton' => $this->WidgetInstance->$subWidget->singleton,
                'icon'      => $this->WidgetInstance->$subWidget->icon,
            ];
        }
        $widgets = $this->WidgetInstance->getByUser($userId);
        $this->set(compact('widgets', 'masters'));
    }
}