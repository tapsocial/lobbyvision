<?php
App::uses('AppController', 'Controller');
/**
 * UsersNewsCategories Controller
 *
 * @property UsersNewsCategory $UsersNewsCategory
 */
class UsersNewsCategoriesController extends AppController {

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit() {
        $id   = $this->Auth->user('id');
        $data = null;
        if (!$this->UsersNewsCategory->User->exists($id)) {
            throw new NotFoundException(__('Invalid users news category'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['UsersNewsCategory']['user_id'] = $id;
            if (    isset($this->request->data['UsersNewsCategory']['news_category_id'])
                && !empty($this->request->data['UsersNewsCategory']['news_category_id'])
            ) {
                foreach ($this->request->data['UsersNewsCategory']['news_category_id'] as $value) {
                    $data[] = array(
                        'UsersNewsCategory' => array(
                            'news_category_id' => $value,
                            'user_id'          => $id
                    ));
                }
            }

            $this->UsersNewsCategory->deleteAll(array('UsersNewsCategory.user_id' => $id), false);
            if (!empty($data) && $this->UsersNewsCategory->saveMany($data)) {
                $this->Session->setFlash(__('Your Reuters™ news selections have been saved'), 'flash/success');
                //$this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Your Reuters™ news selections have been saved'), 'flash/success');
                //$this->Session->setFlash(__('Your Reuters™ news selections could not be saved. Please, try again.'), 'flash/error');
            }
        } else {
            $this->UsersNewsCategory->recursive=-1;
            $options = array('conditions' => array('UsersNewsCategory.user_id' => $id));
            $this->request->data['UsersNewsCategory']['news_category_id'] = $this->UsersNewsCategory->find('list', $options);
            //$this->request->data['UsersNewsCategory']=
        }
        $newsCategories = $this->UsersNewsCategory->NewsCategory->find('list');
        $users          = $this->UsersNewsCategory->User->find('list');
        $this->set(compact('newsCategories', 'users'));
    }


/**
 * admin_index method
 *
 * @return void
 */
    public function admin_index() {
        $this->UsersNewsCategory->recursive = 0;
        $this->set('usersNewsCategories', $this->paginate());
    }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_view($id = null) {
        if (!$this->UsersNewsCategory->exists($id)) {
            throw new NotFoundException(__('Invalid users news category'));
        }
        $options = array('conditions' => array('UsersNewsCategory.' . $this->UsersNewsCategory->primaryKey => $id));
        $this->set('usersNewsCategory', $this->UsersNewsCategory->find('first', $options));
    }

/**
 * admin_add method
 *
 * @return void
 */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->UsersNewsCategory->create();
            if ($this->UsersNewsCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The users news category has been saved'), 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The users news category could not be saved. Please, try again.'), 'flash/error');
            }
        }
        $newsCategories = $this->UsersNewsCategory->NewsCategory->find('list');
        $users = $this->UsersNewsCategory->User->find('list');
        $this->set(compact('newsCategories', 'users'));
    }

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_edit($id = null) {
        if (!$this->UsersNewsCategory->exists($id)) {
            throw new NotFoundException(__('Invalid users news category'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->UsersNewsCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The users news category has been saved'), 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The users news category could not be saved. Please, try again.'), 'flash/error');
            }
        } else {
            $options = array('conditions' => array('UsersNewsCategory.' . $this->UsersNewsCategory->primaryKey => $id));
            $this->request->data = $this->UsersNewsCategory->find('first', $options);
        }
        $newsCategories = $this->UsersNewsCategory->NewsCategory->find('list');
        $users = $this->UsersNewsCategory->User->find('list');
        $this->set(compact('newsCategories', 'users'));
    }

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null) {
        $this->UsersNewsCategory->id = $id;
        if (!$this->UsersNewsCategory->exists()) {
            throw new NotFoundException(__('Invalid users news category'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->UsersNewsCategory->delete()) {
            $this->Session->setFlash(__('Users news category deleted'), 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Users news category was not deleted'), 'flash/error');
        $this->redirect(array('action' => 'index'));
    }
}
