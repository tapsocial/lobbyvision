<?php
App::uses('AppController', 'Controller');
/**
 * WeatherPanels Controller
 *
 * @property WeatherPanel $WeatherPanel
 */
class WeatherPanelsController extends AppController {



  function beforeFilter() {
 	parent::beforeFilter();


}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->WeatherPanel->recursive = 0;
		$this->set('weatherPanels', $this->paginate());
	}



/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->WeatherPanel->create();
			if ($this->WeatherPanel->save($this->request->data)) {
				$this->Session->setFlash(__('The weather panel has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The weather panel could not be saved. Please, try again.'));
			}
		}
		$weatherUndergrounds = $this->WeatherPanel->WeatherUnderground->find('list');
		$this->set(compact('weatherUndergrounds'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->WeatherPanel->exists($id)) {
			throw new NotFoundException(__('Invalid weather panel'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->WeatherPanel->save($this->request->data)) {
				$this->Session->setFlash(__('The weather panel has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The weather panel could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('WeatherPanel.' . $this->WeatherPanel->primaryKey => $id));
			$this->request->data = $this->WeatherPanel->find('first', $options);
		}
		$weatherUndergrounds = $this->WeatherPanel->WeatherUnderground->find('list');
		$this->set(compact('weatherUndergrounds'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->WeatherPanel->id = $id;
		if (!$this->WeatherPanel->exists()) {
			throw new NotFoundException(__('Invalid weather panel'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->WeatherPanel->delete()) {
			$this->Session->setFlash(__('Weather panel deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Weather panel was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}
}
