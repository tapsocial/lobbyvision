<?php
App::uses('AppController', 'Controller');
/**
 * WidgetInstances Controller
 *
 * @property WidgetInstance $WidgetInstance
 */
class WidgetInstancesController extends AppController
{
    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->WidgetInstance->exists($id)) {
            throw new NotFoundException(__('Invalid widget instance'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->WidgetInstance->saveAll($this->request->data, ['deep' => true])) {
                $this->Session->setFlash(__('The widget instance has been saved'), 'flash/success');
                $this->redirect(array('action' => 'edit', 'controller' => 'display'));
            } else {
                $this->Session->setFlash(__('The widget instance could not be saved. Please, try again.'), 'flash/error');
            }
        } else {
            $this->request->data = $this->WidgetInstance->getData($id);
        }
    }

}
