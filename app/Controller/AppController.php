<?php
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $components = array(
        'Acl',
        'Auth' => array(
            'authenticate' => array('Form' => array( 'userModel' => 'User',
                'fields' => array(
                    'username' => 'username',
                    'password' => 'password'
                )
            )),
        ),
        'Session',
        'Email',
        'Facebook.Connect',
        'Twitter.Twitter',
        //'Facebook.Api',
        //'DebugKit.Toolbar'
    );

    public $user;

    public $helpers = array('Html', 'Form', 'Session','Menubuilder.Menu','Custom','Facebook.Facebook');

    public function beforeFilter()
    {
        if ($this->Auth->loggedIn()) {
            $this->user = $this->Auth->user();
            if ($this->Auth->loggedIn() && $this->user['group_id'] == 2) {
                // debug($this->Auth->user())
                $this->loadModel('User');

                $this->loadModel('Template');
                $this->set('all_templates', $this->Template->find('list'));

                $this->User->recursive = 0;
                $this->user = array_merge($this->user, $this->User->find('first', [
                    'conditions' => ['User.id' => $this->Auth->user('id')],
                    'contain'    => ['FConnect', 'Design', 'MailingAddress'],
                ]));

                $this->layout = 'dashboard';
                if (isset($this->user['Contact']['timezone']) && $this->user['Contact']['timezone'] != '') {
                    // $this->user['Contact']['timezone']
                    date_default_timezone_set($this->user['Contact']['timezone']);

                    $dtz = new DateTimeZone($this->user['Contact']['timezone']);
                    $time_in_sofia = new DateTime('now', $dtz);
                    $offset = $dtz->getOffset($time_in_sofia) / 3600;
                    $timeOffset = ($offset < 0 ? $offset : "+" . $offset);

                    $this->set('timeOffset', $timeOffset);
                    // echo timezone_offset_get($this->user['Contact']['timezone']);
                }

                if ($this->user['FConnect']['type'] == 'facebook') {
                    $Facebook = new FB();
                    $u = $Facebook->api('/me');
                    $fUser = unserialize($this->user['FConnect']['value']);
                    $permissions = $Facebook->api("/" . $fUser['username'] . "/permissions");
                    if (! $permissions || empty($permissions['data'])) {
                        $this->User->FConnect->id = $this->user['FConnect']['id'];
                        $this->User->FConnect->delete();
                    } else {
                        $this->User->FConnect->id = $this->user['FConnect']['id'];
                        $this->User->FConnect->saveField('permissions', serialize($permissions['data']));
                    }
                }

                if (isset($this->user['UserConnect']['value'])) {
                    $twitterUser = unserialize($this->user['UserConnect']['value']);
                    CakeSession::write('Twitter.User.oauth_token', $twitterUser['oauth_token']);
                    CakeSession::write('Twitter.User.oauth_token_secret', $twitterUser['oauth_token_secret']);
                } else {
                    $this->Session->delete('Twitter.User');
                }
                // debug($permissions);
                // debug($u);

                // Router::url( $this->request->here, true );

                {
                    $this->set('fconnect', $this->Connect->user());
                }
            }
        }
        // debug($this->user);
        $this->set('authUser', $this->user);

        Configure::load('custom');
        Configure::load('timezone');

        $this->theme = 'Cakestrap';


        $this->Auth->loginAction = '/users/login?redirect=' . $this->request->here;
        $this->Auth->logoutRedirect = array(
            'admin' => false,
            'plugin' => false,
            'controller' => 'users',
            'action' => 'login'
        );
    }

    function afterPaypalNotification($txnId)
    {
        debug($txnId);
        // Here is where you can implement code to apply the transaction to your app.
        // for example, you could now mark an order as paid, a subscription, or give the user premium access.
        // retrieve the transaction using the txnId passed and apply whatever logic your site needs.

        $transaction = ClassRegistry::init('PaypalIpn.InstantPaymentNotification')->findById($txnId);
        $this->log($transaction['InstantPaymentNotification']['id'], 'paypal');
        // Tip: be sure to check the payment_status is complete because failure
        // are also saved to your database for review.

        if ($transaction['InstantPaymentNotification']['payment_status'] == 'Completed') {
            // Yay! We have monies!
        } else {
            // Oh no, better look at this transaction to determine what to do; like email a decline letter.
        }
    }
}
