<?php
App::uses('AppController', 'Controller');
/**
 * Events Controller
 *
 * @property Event $Event
 */
class EventsController extends AppController {

public $paginate = array(
        'order' => array(
            'Event.start' => 'desc'
        ),
        //'limit'=>10,
    );

/**
 * index method
 *
 * @return void
 */
    public function index() {
        $this->Event->recursive = 0;
        $options=array('Event.user_id'=>$this->user['id']);
        $this->set('events', $this->paginate($options));
    }

/**
 * add method
 *
 * @return void
 */
    public function add() {

        if ($this->request->is('post')) {
            $this->Event->create();
            $this->request->data['Event']['user_id']=$this->user['id'];
            $this->request->data['Event']['dir']='villaimage';
            $this->Event->validate['image'] = array(
                 'Empty' => array('check' => false),
                    'HttpPost' => array(
                        'check' => false
                    )
                );

            $this->request->data['Event']['recurrence_id'] = $this->Event->Recurrence->getForModel($this->Event, $this->request->data);
            if ($this->Event->save($this->request->data)) {
                $this->Session->setFlash(__('Your event has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The event could not be saved. Please, try again.'));
            }
        }
        $recurrences = $this->Event->Recurrence->getTypes();
        $recurrence  = 'never';
        $this->set(compact('recurrences', 'recurrence'));
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {
        $this->Event->id = $id;
        if (!$this->Event->exists()) {
            throw new NotFoundException(__('Invalid event'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Event->validate['image'] = array(
                'Empty'    => array('check' => false),
                'HttpPost' => array('check' => false),
            );
            $this->request->data['Event']['user_id']       = $this->user['id'];
            $this->request->data['Event']['recurrence_id'] = $this->Event->Recurrence->getForModel($this->Event, $this->request->data);
            if ($this->Event->save($this->request->data)) {
                $this->Session->setFlash(__('The event has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The event could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Event->find('first', [
                'conditions' => ['Event.id' => $id],
                'contain'    => ['Recurrence'],
            ]);
            $recurrences = $this->Event->Recurrence->getTypes();
            $recurrence  = $this->Event->Recurrence->detectType($this->request->data);
            $this->set(compact('recurrences', 'recurrence'));
        }
    }

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Event->id = $id;
        if (!$this->Event->exists()) {
            throw new NotFoundException(__('Invalid event'));
        }
        if ($this->Event->delete()) {
            $this->Session->setFlash(__('Event deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Event was not deleted'));
        $this->redirect(array('action' => 'index'));
    }
}
