<?php
App::uses('AppController', 'Controller');
/**
 * WeatherUndergrounds Controller
 *
 * @property WeatherUnderground $WeatherUnderground
 */
class WeatherUndergroundsController extends AppController {

public function beforeFilter() {
    parent::beforeFilter();

     $this->Auth->allow('getAddress');

}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit() {

            $id=$this->Auth->user('id');







        if (!$this->WeatherUnderground->User->exists($id)) {
            throw new NotFoundException(__('Invalid weather underground'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['WeatherUnderground']['user_id']=$id;
            if ($this->WeatherUnderground->save($this->request->data)) {
                $this->saveWeather($this->request->data);
                $this->Session->setFlash(__('Your Weather Underground™ city has been updated.'));
                //$this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Your Weather Underground™ city has been updated. could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('WeatherUnderground.user_id' => $id));
            $this->request->data = $this->WeatherUnderground->find('first', $options);
        }
        $users = $this->WeatherUnderground->User->find('list');
        $countries = $this->WeatherUnderground->Country->find('list');
        $weather_panels = $this->WeatherUnderground->WeatherPanel->find('list');
        //debug($WeatherPanel);
        $this->set(compact('users', 'countries','weather_panels'));
    }


/**
 * admin_index method
 *
 * @return void
 */
    public function admin_index() {
        $this->WeatherUnderground->recursive = 0;
        $this->set('weatherUndergrounds', $this->paginate());
    }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_view($id = null) {
        if (!$this->WeatherUnderground->exists($id)) {
            throw new NotFoundException(__('Invalid weather underground'));
        }
        $options = array('conditions' => array('WeatherUnderground.' . $this->WeatherUnderground->primaryKey => $id));
        $this->set('weatherUnderground', $this->WeatherUnderground->find('first', $options));
    }

/**
 * admin_add method
 *
 * @return void
 */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->WeatherUnderground->create();
            if ($this->WeatherUnderground->save($this->request->data)) {
                $this->Session->setFlash(__('Your Weather Underground™ city has been updated.'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The weather underground could not be saved. Please, try again.'));
            }
        }
        $users = $this->WeatherUnderground->User->find('list');
        $countries = $this->WeatherUnderground->Country->find('list');
        $this->set(compact('users', 'countries'));
    }

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_edit($id = null) {
        if (!$this->WeatherUnderground->exists($id)) {
            throw new NotFoundException(__('Invalid weather underground'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->WeatherUnderground->save($this->request->data)) {
                $this->Session->setFlash(__('Your Weather Underground™ city has been updated.'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Your Weather Underground™ city could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('WeatherUnderground.' . $this->WeatherUnderground->primaryKey => $id));
            $this->request->data = $this->WeatherUnderground->find('first', $options);
        }
        $users = $this->WeatherUnderground->User->find('list');
        $countries = $this->WeatherUnderground->Country->find('list');
        $this->set(compact('users', 'countries'));
    }

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null) {
        $this->WeatherUnderground->id = $id;
        if (!$this->WeatherUnderground->exists()) {
            throw new NotFoundException(__('Invalid weather underground'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->WeatherUnderground->delete()) {
            $this->Session->setFlash(__('Weather underground deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Weather underground was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    /**
     * Get autocomplete options for the locations
     *
     * @param string $query
     *
     * @todo move it outside the deprecated controller
     */
    public function getAddress ($query = '')
    {
        if (!$query) {
            $query = $_REQUEST['term'];
        }

        $apiUrl = "http://autocomplete.wunderground.com/aq";

        $json_string = file_get_contents($apiUrl . '?query=' . $query);
        $parsed_json = json_decode($json_string);
        $results     = [];
        foreach ($parsed_json->RESULTS as $location) {
            $results[] = array(
                    'label'  => $location->name,
                    'value'  => $location->zmw,
                    'latlon' => $location->lat . ',' . $location->lon
            );
        }
        // debug($parsed_json);
        header('Content-type: application/json');
        die(json_encode($results));
    }

    private function saveWeather($weather=null){


        if($weather['WeatherUnderground']['url']!=''){
                $current_url="http://api.wunderground.com/api/b44170f5e063aef0/conditions/q/".$weather['WeatherUnderground']['url'].'.json';

                $forcast_url="http://api.wunderground.com/api/b44170f5e063aef0/forecast/q/".$weather['WeatherUnderground']['url'].'.json';

            }
            else
            return false;


        $this->loadModel('Forecast');
        $forecast=$this->Forecast->find('first',array('conditions'=>array('weather_underground_id'=>$weather['WeatherUnderground']['id'])));

        if($forecast){
            $data['Forecast']['id']=$forecast['Forecast']['id'];
        }
        if($weather['WeatherUnderground']['url'])
        {

            //return false;
            $data['Forecast']['weather_underground_id']=$weather['WeatherUnderground']['id'];
            $data['Forecast']['url']=$weather['WeatherUnderground']['url'];

//die('fff');

            if(!isset($forecast['Forecast']['forecast']) || $forecast['Forecast']['forecast']=='' || strtotime($forecast['Forecast']['modified']) < (time()-3600)){


             $json_forecast = file_get_contents($forcast_url);

            $results=json_decode($json_forecast);


            /*// $url = "http://www.dietadom.fr/test.php";
            //  Initiate curl
            $ch = curl_init();
            // Disable SSL verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // Will return the response, if false it print the response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // Set the url
            curl_setopt($ch, CURLOPT_URL,$forcast_url);
            // Execute
            $json_forecast=curl_exec($ch);

            // Will dump a beauty json :3
            $results=json_decode($json_forecast, true);*/



             if(!isset($results->error)){
                  $data['Forecast']['forecast']=$json_forecast;
                 $forecast['Forecast']['forecast']=$json_forecast;
             }
                 else
                     $data['Forecast']['forecast']='';

             }

            /* if($forecast['Forecast']['current']=='' || strtotime($forecast['Forecast']['modified']) < (time()-3600))*/
             {
             $json_current = file_get_contents($current_url);
             $current_results=json_decode($json_current);

            /* // $url = "http://www.dietadom.fr/test.php";
            //  Initiate curl
            $ch = curl_init();
            // Disable SSL verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // Will return the response, if false it print the response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // Set the url
            curl_setopt($ch, CURLOPT_URL,$current_url);
            // Execute
            $json_current=curl_exec($ch);

            // Will dump a beauty json :3
            $current_results=json_decode($json_current, true);*/



             if(!isset($current_results->error)){
                  $data['Forecast']['current']=$json_current;
                 $forecast['Forecast']['current']=$json_current;
             }
                 else
                     $data['Forecast']['current']='';
             }
             $this->Forecast->save($data);

    }
}
}
