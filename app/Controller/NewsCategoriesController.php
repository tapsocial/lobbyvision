<?php
App::uses('AppController', 'Controller');
/**
 * NewsCategories Controller
 *
 * @property NewsCategory $NewsCategory
 */
class NewsCategoriesController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->NewsCategory->recursive = 0;
		$this->set('newsCategories', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->NewsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid news category'));
		}
		$options = array('conditions' => array('NewsCategory.' . $this->NewsCategory->primaryKey => $id));
		$this->set('newsCategory', $this->NewsCategory->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->NewsCategory->create();
			if ($this->NewsCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The news category has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The news category could not be saved. Please, try again.'), 'flash/error');
			}
		}
		
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->NewsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid news category'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->NewsCategory->save($this->request->data)) {
				$this->Session->setFlash(__('The news category has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The news category could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('NewsCategory.' . $this->NewsCategory->primaryKey => $id));
			$this->request->data = $this->NewsCategory->find('first', $options);
		}
		//$users = $this->NewsCategory->User->find('list');
		//$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->NewsCategory->id = $id;
		if (!$this->NewsCategory->exists()) {
			throw new NotFoundException(__('Invalid news category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->NewsCategory->delete()) {
			$this->Session->setFlash(__('News category deleted'), 'flash/success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('News category was not deleted'), 'flash/error');
		return $this->redirect(array('action' => 'index'));
	}
}
