<?php
App::uses('AppController', 'Controller');
/**
 * UserConnects Controller
 *
 * @property UserConnect $UserConnect
 */
class UserConnectsController extends AppController {

 public $components = array('RequestHandler');

public function beforeFilter() {
	 parent::beforeFilter();
}
/**
 * index method
 *
 * @return void
 */
	public function index() {

		$options=array('user_id'=>$this->user['id']);
		$this->UserConnect->recursive = 0;
		$this->set('userConnects', $this->paginate($options));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UserConnect->exists($id)) {
			throw new NotFoundException(__('Invalid user connect'));
		}
		$options = array('conditions' => array('UserConnect.' . $this->UserConnect->primaryKey => $id));
		$this->set('userConnect', $this->UserConnect->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UserConnect->create();
			if ($this->UserConnect->save($this->request->data)) {
				$this->Session->setFlash(__('The user connect has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user connect could not be saved. Please, try again.'));
			}
		}
		$users = $this->UserConnect->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UserConnect->exists($id)) {
			throw new NotFoundException(__('Invalid user connect'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->UserConnect->save($this->request->data)) {
				$this->Session->setFlash(__('The user connect has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user connect could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserConnect.' . $this->UserConnect->primaryKey => $id));
			$this->request->data = $this->UserConnect->find('first', $options);
		}
		$users = $this->UserConnect->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		//die('dd'.$id);
		$this->UserConnect->id = $id;

		//echo $this->UserConnect->id;
		
		if (!$this->UserConnect->exists()) {
			throw new NotFoundException(__('Invalid user connect'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->UserConnect->delete()) {
			$this->Session->setFlash(__('You are disconnected from Twitter.'));
			return $this->redirect(array('controller'=>'Designs','action' => 'index'));
		}
		$this->Session->setFlash(__('User connect was not deleted'));
		return $this->redirect(array('action' => 'hashTags'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->UserConnect->recursive = 0;
		$this->set('userConnects', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->UserConnect->exists($id)) {
			throw new NotFoundException(__('Invalid user connect'));
		}
		$options = array('conditions' => array('UserConnect.' . $this->UserConnect->primaryKey => $id));
		$this->set('userConnect', $this->UserConnect->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->UserConnect->create();
			if ($this->UserConnect->save($this->request->data)) {
				$this->Session->setFlash(__('The user connect has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user connect could not be saved. Please, try again.'));
			}
		}
		$users = $this->UserConnect->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->UserConnect->exists($id)) {
			throw new NotFoundException(__('Invalid user connect'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->UserConnect->save($this->request->data)) {
				$this->Session->setFlash(__('The user connect has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user connect could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserConnect.' . $this->UserConnect->primaryKey => $id));
			$this->request->data = $this->UserConnect->find('first', $options);
		}
		$users = $this->UserConnect->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->UserConnect->id = $id;
		if (!$this->UserConnect->exists()) {
			throw new NotFoundException(__('Invalid user connect'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserConnect->delete()) {
			$this->Session->setFlash(__('User connect deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User connect was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}

	function hashTags($user_id=null){
		

		if(!$user_id && $this->user['group_id']==2){
			$user_id=$this->user['id'];
		}
		if (!$this->UserConnect->User->exists($user_id)) {
			throw new NotFoundException(__('Invalid user connect'));
		}

	$uconnect=$this->UserConnect->find('first',array('conditions'=>array('UserConnect.user_id'=>$user_id,'type'=>'twitter')));
if ($this->request->is('post') || $this->request->is('put')) {

	if(isset($this->request->data['TickerContent'])){
		$this->saveTickerContent();
	}elseif(isset($this->request->data['Hashtag'])){
		$this->savehashtag($uconnect);
	}
	//pp::uses('MyModel', 'Model');

}
$this->loadModel('Design');
$design=$this->Design->find('first',array('conditions'=>array('user_id'=>$user_id)));
		

if(isset($this->request->data['Design'])){
	$this->Design->id=$design['Design']['id'];
	debug($design['Design']);
	debug($this->request->data['Design']);
	$this->Design->saveField('show_tweets',$this->request->data['Design']['show_tweets']);
	$this->Design->saveField('ticker_content',$this->request->data['Design']['ticker_content']);
}

if($design)
			$this->request->data = $design;
		//debug($hashTags);
		$this->loadModel('TickerContent');
		$tickerConents=$this->TickerContent->find('all',array('conditions'=>array(
			'user_id'=>$user_id)));


		$this->set(compact('uconnect','tickerConents'));

	}
private function savehashtag($uconnect){
		$this->request->data['UserConnect']['id']=$uconnect['UserConnect']['id'];

	$this->UserConnect->Hashtag->deleteAll(array('user_connect_id'=>$uconnect['UserConnect']['id']));
$hashtags=array();
	foreach ($this->request->data['Hashtag'] as $key => $value) {

		if(in_array($value['hashtag'],$hashtags))
			continue;
$hashtags[]=$value['hashtag'];
					$this->request->data['Hashtag'][$key]['user_connect_id']=$uconnect['UserConnect']['id'];
				}

			if ($this->request->is('ajax')) {
			   // $this->disableCache();
				

				if($this->UserConnect->Hashtag->saveMany($this->request->data['Hashtag']))
				echo 'Saved';
			else
				echo 'Not Saved';

			}
			$this->UserConnect->Hashtag->saveMany($this->request->data['Hashtag']);
			/*$this->Session->setFlash(__('#tags Saved.'));
				return $this->redirect(array('action' => 'hashTags'));*/
}
	private function saveTickerContent(){
		$this->loadModel('TickerContent');
		foreach($this->request->data['TickerContent'] as $key=>$villa){			
			$this->request->data['TickerContent'][$key]['user_id']=$this->user['id'];
			if($villa['content']=='')	
				unset($this->request->data['TickerContent'][$key]);

			}

			if ($this->TickerContent->saveAll($this->request->data['TickerContent'])) {
				$this->Session->setFlash(__('The Ticker content has been saved'), 'flash/success');
				//$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Ticker content could not be saved. Please, try again.'), 'flash/error');
			}

	}
	function delete_tc(){
		$this->loadModel('TickerContent');
	//	debug($this->request->data);
		$this->TickerContent->id = $this->request->data['cid'];
		if (!$this->TickerContent->exists()) {
			throw new NotFoundException(__('Invalid user connect'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->TickerContent->delete())
			die('deleted');
		else
			die('cannot be delted');
	}
}
