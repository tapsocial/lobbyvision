<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {
//var $helpers=array('TimezoneSelect');
var $helpers=array('PaypalIpn.Paypal','CustomPaginator');
var $components = array('Otp','RequestHandler');

public $paginate = array(
        'fields' => array('DISTINCT User.id','User.*','Group.*'),
        'limit' => 20,
         'order' => array(
            'User.id' => 'asc'
        )
    
    );


public function beforeFilter() {
    parent::beforeFilter();
    	$this->Auth->allow('register','login','logout','forgotpassword','newpassword','select_package','CheckUnique','confirm_purchase','thank_you','view','setTimezone','admin_logout');

    
}


public function setTimezone(){
	//$_SESSION['time'] = $_GET['time'];
	$this->Session->write('timezone',$_GET['time']);
	echo $_GET['time'];
	die();
}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view() {
		if($this->Auth->user())
		$this->User->id=$this->user['id'];
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->set('user', $this->User->read(null, $this->user['id']));
	}



function select_package(){
	if ($this->request->is('post')) {
			$this->User->create();
			if ($this->request->data['User']['subscription_package_id']) {
				$this->Session->write('Register.subscription_package_id',$this->request->data['User']['subscription_package_id']);
				return $this->redirect(array('action' => 'register'));
			} else {
				
				$this->Session->setFlash(__('please choose a package.'));
			}
		}
		else{
			$this->Session->delete('Register.subscription_package_id');
		}
		
	$subscriptionPackages = $this->User->SubscriptionPackage->find('all');
	$subscriptionPackages = Set::combine(
               $subscriptionPackages,
               '{n}.SubscriptionPackage.id',
               array(
                 '{2}',
                 '{n}.SubscriptionPackage.months',
                 '{n}.SubscriptionPackage.price_per_month',
                 '{n}.SubscriptionPackage.description'
               )
        ); 
		
	$this->set(compact('subscriptionPackages'));
		
}

    public function register()
    {
        $this->loadModel('Address');
        if (! $this->Session->check('Register.subscription_package_id')) {
            $this->Session->setFlash(__('please choose a package.'));
            $this->redirect(array( 'action' => 'select_package' ));
        }

        if ($this->request->is('post')) {
            $this->User->create();
            $this->request->data['User']['group_id'] == 2;
            unset($this->User->Contact->validate['user_id']);
            unset($this->User->MailingAddress->validate['user_id']);
            unset($this->User->BillingAddress->validate['user_id']);
            if ($this->User->saveAssociated($this->request->data, array( 'deep' => true ))) {
                $this->Session->setFlash(__('Confirm your purchase and registration information below.'));
                $this->Session->write('User.id', $this->User->id);
                $this->redirect(array( 'action' => 'confirm_purchase' ));
            } else {
                $this->Session->setFlash(__('Please correct the Registration errors highlighted below.'));
            }
        }
        $subscriptionPackage = $this->User->SubscriptionPackage->find('first', array(
            'conditions' => array( 'SubscriptionPackage.id' => $this->Session->read('Register.subscription_package_id') )
        ));

        $countries = $this->Address->Country->find('list');
        $this->set(compact('subscriptionPackage', 'countries'));
    }


    public function confirm_purchase()
    {
        if (isset($_GET['tx']) && $_GET['tx'] != '') {
            $this->User->id = $this->Session->read('User.id');
            $this->User->saveField('id_package', $this->Session->read('Register.subscription_package_id'));
            $this->User->saveField('txn', $_GET['tx']);
            $this->User->saveField('active', 1);
            $Email = new CakeEmail('smtp');
            $Email->template('welcome')
                ->emailFormat('html')
                ->subject('Welcome to TapSocial Digital Signage')
                ->to($this->User->field('email'))
                ->from('accounts@tapsocial.net')
                ->send();
            $this->redirect(array( 'action' => 'thank_you' ));
        }
        // Please use the following identity token when setting up Payment Data Transfer on your website.3ycUD4cCVHTyHVb178JkW0U4DZaChA5zfh7bxmj8D4ElYEze21FyF5Irl2q
        if (! $this->Session->check('Register.subscription_package_id')) {
            $this->Session->setFlash(__('please choose a package.'));
            $this->redirect(array( 'action' => 'select_package' ));
        }

        if ($this->request->is('post')) {
            $this->User->create();
            $this->request->data['User']['group_id'] == 2;
            unset($this->User->Contact->validate['user_id']);
            unset($this->User->MailingAddress->validate['user_id']);
            unset($this->User->BillingAddress->validate['user_id']);
            if ($this->User->saveAssociated($this->request->data, array( 'deep' => true ))) {
                $this->Session->setFlash(__('The user account has been saved'));
                $this->redirect(array( 'action' => 'confirm_purchase' ));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        }
        $userId = $this->Session->read('User.id') ;
        $this->request->data = $this->User->find('first', array(
            'conditions' => array( 'User.id' => $userId ),
            'contain'    => ['Contact', 'MailingAddress', 'BillingAddress'],
        ));
        $subscriptionPackage = $this->User->SubscriptionPackage->find('first', array(
            'conditions' => array( 'SubscriptionPackage.id' => $this->Session->read('Register.subscription_package_id') )
        ));
        $this->loadModel('Address');
        $countries = $this->Address->Country->find('list');
        $this->set(compact('subscriptionPackage', 'countries', 'userId'));
    }


function CheckUnique($username=null) {
	$this->layout=false;
	
	
if($username) {

	if ($this->User->findByUsername($username))
	{
		$this->set('availibility','notavailable');
	} else{
		$this->set('availibility', 'available');
	
	}
	
}
if ($this->request->is('ajax')) {
	//$this->render('ajax');
	//$this->render('Users/check_unique');
	}
}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if($this->request->data['User']['password']=='')
				unset($this->request->data['User']['password']);
			
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$this->User->recursive=1;
			$this->request->data = $this->User->read(null, $id);
		}
		$subscription_packages = $this->User->SubscriptionPackage->find('list');
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups','subscription_packages'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate('User'));
		
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_view($id = null) {
        $this->User->id = $id;
        $user = $this->User->find('first', [
            'conditions' => ['User.id' => $id],
        ]);
        $user = array_merge($user['User'], $user);
        unset($user['User']);
        $this->Auth->login($user);

        $this->redirect(array('controller'=>'admin','action'=>'index','admin'=>false));

    }

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The User has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->loadModel('Address');
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			/*$this->User->validator()->remove('username');
			$this->User->validator()->remove('email');*/
			if($this->request->data['User']['password']=='')
				unset($this->request->data['User']['password']);
//debug($this->request->data);
			if ($this->User->saveAssociated($this->request->data,array('deep' => true))) {
				$this->Session->setFlash(__('User detail has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {

				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			//$this->User->recursive=1;
			$this->request->data = $this->User->read(null, $id);
		}
		$countries = $this->Address->Country->find('list');
		$subscription_packages = $this->User->SubscriptionPackage->find('list');
		$groups = $this->User->Group->find('list');
		if($this->request->data['User']['group_id']==1)
			$this->render('administrator');

		/*$subscriptionPackage = $this->User->SubscriptionPackage->find('first',array('conditions'=>array('SubscriptionPackage.id'=>$this->request->data['User']['subscription_package_id'])));	
$this->request->data['SubscriptionPackage']=$subscriptionPackage['SubscriptionPackage'];*/

		$this->set(compact('groups','subscriptionPackage','countries','subscription_packages'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}
	
	public function thank_you() {

	}

    public function login()
    {
        if ($this->Auth->login()) {
            $user = $this->Auth->User();
            if ($user['group_id'] == 1) {
                $this->redirect(array( 'controller' => 'admin', 'action' => 'index', 'admin' => false ));
            } elseif ($user['group_id'] == 2) {
                if (! $user['active']) {
                    $this->loadModel('InstantPaymentNotification');
                    $ipn = $this->InstantPaymentNotification->find('first', array(
                        'conditions' => array(
                            'payment_status' => 'Completed',
                            'txn_id' => $user['txn']
                        )
                    ));
                    if ($ipn && $ipn['InstantPaymentNotification']['txn_id'] != '') {
                        $this->User->id = $user['id'];
                        $this->User->saveField('active', 1);
                    }
                }
                $redirect = array( 'controller' => 'admin', 'action' => 'index', 'admin' => false );
                if (!empty($_GET['redirect'])) {
                    $redirect = $_GET['redirect'];
                }
                $this->redirect($redirect);
            }
        } else {
            if ($this->request->is('post') && isset($this->request->data) && ! empty($this->request->data)) {
                // die('dd');
                $this->Session->setFlash(__('The login information that you provided is incorrect.  Please try again.'), 'default', array(), 'auth');
            }
        }
    }

	public function logout() {

		  if ($this->Auth->loggedIn()) {
		  	

		 	 $this->Auth->logout();
		  	$this->Session->delete('User');	  	
		  	
		  	$this->Session->setFlash(__('Good Bye!!'));
 			return $this->redirect('/users/login');
		  }
		  else{
		  		
 		return $this->redirect($this->Auth->redirect('/'));
		  }
		  
		    //Leave empty for now.
	}
		public function admin_logout() {

		  if ($this->Auth->login()) {
		  	$this->Session->setFlash(__('Good Bye!!'));

 				return $this->redirect($this->Auth->logout());
		  }
		  else{
		  		
 		return $this->redirect($this->Auth->redirect('/'));
		  }
		  
		    //Leave empty for now.
	}

  public  function forgotpassword()
  {
  	if($this->request->data){
  		$username = $this->request->data['User']['username'];

  		$user = $this->User->Find('first', array(
        			'conditions' => array(
        				'OR'=>array('User.username' => $username,'User.email' => $username)
        				)
        			));
//debug($user);
        if($user){ 
        			$randomPass = $this->Auth->password($this->generatePassword());
 					$nowstr =  date('Y-m-d G:i:s ', time());
 					$now = strtotime($nowstr); 
                    $expstr =date('Y-m-d G:i:s ', time()+ 24*3600);
                    $exp = strtotime($expstr);
                    // create the OTP - TTL = time to live
                    $otp = $this->Otp->createOTP(array('email'=>$user['User']['email'],'password'=>$randomPass,'ttl'=> $exp) );
                    $ticketdata = array('Ticket'=>array('hash' => $otp, 'email' => $user['User']['email'],  'expires' => $expstr));
                    $this->loadModel('Ticket');
                    $this->Ticket->save($ticketdata);
                    
                     // send mail
                $Email = new CakeEmail('smtp');
                $Email->viewVars(array('otp' => $otp));
                $Email->template('password_reset')
                ->emailFormat('html')
                ->subject('You requested a new TapSocial password')
                ->to($user['User']['email'])
                ->from('accounts@tapsocial.net')
                ->send();

                  

                    $this->Session->setFlash("An email has been sent to the email address on file.  Please check your email for password reset instructions.");
                    $this->redirect(array('action'=>'login'));         
                }
                else{
               

                    $this->Session->setFlash("Email ID does not exists");
                   // $this->redirect(array('controller' => 'users', 'action' => 'forgotpassword'));         
                }
  	}
  }

 public function generatePassword ($length = 8){
        // inicializa variables
        $password = "";
        $i = 0;
        $possible = "0123456789bcdfghjkmnpqrstvwxyz"; 
        
        // agrega random
        while ($i < $length){
            $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
            
            if (!strstr($password, $char)) { 
                $password .= $char;
                $i++;
            }
        }
        return $password;
    } 
     

public function newpassword($otp)
 {

 	    if($otp){

 		$this->loadModel('Ticket');
 		$checkdetails = $this->Ticket->Find('first', array('conditions' => array('Ticket.hash' => $otp)));
 		if($checkdetails){
 			$nowstr =  date('Y-m-d G:i:s ', time());
 			 $noww = strtotime($nowstr);
 			 $exp = strtotime($checkdetails['Ticket']['expires']);
 			 if($noww <  $exp){ 
 			 	$userdetails = $this->User->Find('first', array('conditions' => array('User.email' => $checkdetails['Ticket']['email'])));
                if($userdetails){ 
              

                if ($this->request->is('post')) { 
 			    $password = $this->request->data['User']['password'];
 			    $confirm_password = $this->request->data['User']['confirm_password'];
 			    if($password == $confirm_password){
                 $password = AuthComponent::password($this->request->data['User']['password']);
                 if($this->User->updateAll(array('User.password' => "'$password'"), array('User.id' => $userdetails['User']['id']))){
                 $this->Ticket->delete($checkdetails['Ticket']['id']);
 				
 				      // send mail
                $Email = new CakeEmail('smtp');
                $Email->template('password_changed')
                ->emailFormat('html')
                ->subject('Password Changed')
                ->to($userdetails['User']['email'])
                ->from('accounts@tapsocial.net')
                ->send();

                 $this->Session->setFlash("Your Password changed successfully.");
 				 $this->redirect(array('controller' => 'users', 'action' => 'login'));
                 }
 			   }
 			else{
 				$this->Session->setFlash("The password values that you entered did not match.  Please try again.");
 				return $this->redirect(array('controller' => 'users', 'action' => 'newpassword',$otp));
 			}
 		  }
         }
 		}
 		else{
 			 $this->Ticket->delete($checkdetails['Ticket']['id']);
 			 $this->Session->setFlash("Token has expired");
      	     $this->redirect('/');
 		}
 	  }
 	  else{
 	  	$this->Session->setFlash("Invalid");
      	     $this->redirect('/');
 	  }
 	    $this->set('token', $otp); 
    }
    else{
 			 $this->Session->setFlash("Invalid");
      	     $this->redirect('/');
    }
 }
	public function admin_changepassword()
	{
		//debug($this->user);
		if($this->request->is('post')){
		$this->request->data['User']['id'] = $this->user['id'];
		$current_password = AuthComponent::password($this->request->data['User']['current_password']);
        $getpassword = $this->User->find('first', array('conditions' => array('User.id' => $this->user['id'])));

        if($current_password != $getpassword['User']['password']){
 			 $this->Session->setFlash("Your current password does not match..");
        }
        else{
        	    if($this->request->data['User']['new_password'] == $this->request->data['User']['confirm_password']){

        	     $password = AuthComponent::password($this->request->data['User']['new_password']);
                 if($this->User->updateAll(array('User.password' => "'$password'"), array('User.id' => $this->user['id']))){
 				 $this->Session->setFlash("Password changed");
 				 $this->redirect($this->Auth->logout());
                 }
             }
             else{
             	$this->Session->setFlash("Your Confirm password does not match..");
             }
        }
		}
	}
	
	public function fconnect($return_url=''){
		$this->User->id=$this->user['id'];	
			
		$Facebook = new FB();
   		$u= $Facebook->api('/me');



			$data['UserConnect']['user_id']=CakeSession::read('Auth.User.id');
			$data['UserConnect']['type'] = 'facebook';
			$data['UserConnect']['value'] = serialize($u);
			if($tuser=$this->User->UserConnect->find('first',array('conditions'=>array('user_id'=>$data['UserConnect']['user_id'],'type'=>'facebook')))){
				//debug($tuser);
				$data['UserConnect']['id']=$tuser['UserConnect']['id'];
			}


		//if($this->User->field('facebook_id')!=$u['id'])
			if($this->User->UserConnect->save($data)){
				$this->Session->setFlash("You are connected with facebook.");
				return $this->redirect(array('controller' => 'UserConnects'));
				
			}
		
	//	die($return_url);
		//die('dd');
		
	}
	public function connections(){

	}
	public function afterFacebookLogin(){
		die('afterFacebookLogin');
		//debug($this->Facebook
	}

	 public function twitter_connect() { 
		//$this->Twitter->setupApp(Configure::read('Twitter.consumerKey'), Configure::read('Twitter.consumerSecret')); 

	 	//$this->Twitter->connectApp(Router::url('/', true).'Users/connections'); 

	 } 

	
}
