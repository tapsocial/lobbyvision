<?php
App::uses('AppController', 'Controller');
/**
 * PaymentTypes Controller
 *
 * @property PaymentType $PaymentType
 */
class PaymentTypesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PaymentType->recursive = 0;
		$this->set('paymentTypes', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PaymentType->exists($id)) {
			throw new NotFoundException(__('Invalid payment type'));
		}
		$options = array('conditions' => array('PaymentType.' . $this->PaymentType->primaryKey => $id));
		$this->set('paymentType', $this->PaymentType->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PaymentType->create();
			if ($this->PaymentType->save($this->request->data)) {
				$this->Session->setFlash(__('The payment type has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The payment type could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PaymentType->exists($id)) {
			throw new NotFoundException(__('Invalid payment type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PaymentType->save($this->request->data)) {
				$this->Session->setFlash(__('The payment type has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The payment type could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('PaymentType.' . $this->PaymentType->primaryKey => $id));
			$this->request->data = $this->PaymentType->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PaymentType->id = $id;
		if (!$this->PaymentType->exists()) {
			throw new NotFoundException(__('Invalid payment type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PaymentType->delete()) {
			$this->Session->setFlash(__('Payment type deleted'), 'flash/success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Payment type was not deleted'), 'flash/error');
		return $this->redirect(array('action' => 'index'));
	}
}
