<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class AdminController extends AppController {
//var $helpers=array('TimezoneSelect');
var $helpers=array('PaypalIpn.Paypal');
var $components = array('Email','Otp','RequestHandler');
public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('index');
}

/**
 * index method
 *
 * @return void
 */
    public function index() {

        if(!$this->Auth->loggedIn()){
            $this->Session->setFlash(__('Please login.'));
            $this->redirect(array('controller'=>'Users','action'=>'login','admin'=>false));

        }



        if($this->user['group_id']==1)
        {
            //die('dd');
            $this->redirect(array('controller'=>'users','action'=>'index','admin'=>true));
        }
        if($this->user['group_id']==2)
        {
            if(!$this->user['User']['active']){
                $this->Auth->logout();
                $this->Session->setFlash(__('Your Subcription is not authenticated or waiting for approval.'));
            $this->redirect(array('controller'=>'users','action'=>'login','admin'=>false));
                /* $this->Layout='default';
                $this->render('/Elements/payment_incomplete');*/
            }
                else{
                    $this->redirect(array('controller'=>'display', 'action' => 'edit','admin'=>false));
                }

            //$this->redirect(array('controller'=>'users','action'=>'view',$this->user['id'],'admin'=>false));
        }
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->read(null, $id));
    }


}
