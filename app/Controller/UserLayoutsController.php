<?php
App::uses('AppController', 'Controller');
/**
 * UserLayouts Controller
 *
 * @property UserLayout $UserLayout
 */
class UserLayoutsController extends AppController {

	function beforeFilter() {
 		parent::beforeFilter();
 		 $this->Auth->allow('edit');
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if(!$id)
			$id=$this->user['User']['id'];
if(!$id)
	return;

$UserLayout=$this->UserLayout->find('first',
			array('conditions'=>
				array(
				'user_id'=>$id,
				'layout_id'=>$this->request->data['UserLayout']['layout_id'],
				//'default'=>1
				)
			)
			);
//debug($UserLayout);

if(!empty($UserLayout))
	$this->request->data['UserLayout']['id']=$UserLayout['UserLayout']['id'];

/*if(isset($this->request->data['UserLayout']['layout_id']) && !empty($this->request->data['UserLayout']['layout_id']))
	$this->request->data['UserLayout']['id']=$UserLayout['UserLayout']['id'];*/

$this->request->data['UserLayout']['id']=$this->request->data['UserLayout']['layout_id'];

$this->request->data['UserLayout']['user_id']=$id;
$this->request->data['UserLayout']['default']=1;

$this->UserLayout->updateAll(
				array('UserLayout.default'=>0),
				array('UserLayout.user_id'=>$id)
				);
$this->UserLayout->Widget->deleteAll(array('Widget.user_layout_id'=>$this->request->data['UserLayout']['id']));

//echo $this->request->data['UserLayout']['id'];

				
	foreach($this->request->data['Widget'] as $key=>$widget){
	//	debug($widget);
		/*if(!isset($widget['name']))
			continue;*/

			$this->request->data['Widget'][$key]['name']=$widget['name'];

			$this->request->data['Widget'][$key]['order']=$key;

			if($this->request->data['Widget'][$key]['class']=='box-long' && isset($this->request->data[$widget['name']]['number_of_display'])){
				$this->request->data['Widget'][$key]['number_of_display']=$this->request->data[$widget['name']]['number_of_display'];
			}else{
				$this->request->data['Widget'][$key]['number_of_display']=0;
			}
			if(isset($this->request->data[$widget['name']]['label'])){
				$this->request->data['Widget'][$key]['label']=$this->request->data[$widget['name']]['label'];
			}
			if(isset($this->request->data[$widget['name']]['widget_icon_id']) && $this->request->data[$widget['name']]['widget_icon_id']!=''){
				$this->request->data['Widget'][$key]['widget_icon_id']=$this->request->data[$widget['name']]['widget_icon_id'];
			}else{
				$this->request->data['Widget'][$key]['widget_icon_id']=0;
			}
			

			//$this->request->data['Widget'][$key]['label']=$widget['label'];
			//$this->request->data['Widget'][$key]['user_layout_id']=1;
	}
		if (!$this->UserLayout->User->exists($id)) {
			throw new NotFoundException(__('Invalid user layout'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
		//debug($this->request->data);
			if ($this->UserLayout->saveAssociated($this->request->data, array('deep' => true))) {
				$this->loadModel('Design');
				$this->Design->recursive=-1;
				$design=$this->Design->find('first',array('fields'=>array('Design.id'),
				'conditions'=>array('Design.user_id'=>$this->user['User']['id'])));
				$this->Design->id=$design['Design']['id'];
				$this->Design->saveField('user_id',$this->user['User']['id']);
				//echo date('Y-m-d H:i:s');
			//	$this->Session->setFlash(__('The user layout has been saved'), 'flash/success');
			//	return $this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The user layout could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('UserLayout.' . $this->UserLayout->primaryKey => $id));
			$this->request->data = $this->UserLayout->find('first', $options);
		}
		$users = $this->UserLayout->User->find('list');
		$layouts = $this->UserLayout->Layout->find('list');
		$this->set(compact('users', 'layouts'));
		die('');
		//$this->layout=false;
	}
	

}
