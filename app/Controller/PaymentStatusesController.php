<?php
App::uses('AppController', 'Controller');
/**
 * PaymentStatuses Controller
 *
 * @property PaymentStatus $PaymentStatus
 */
class PaymentStatusesController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PaymentStatus->recursive = 0;
		$this->set('paymentStatuses', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PaymentStatus->exists($id)) {
			throw new NotFoundException(__('Invalid payment status'));
		}
		$options = array('conditions' => array('PaymentStatus.' . $this->PaymentStatus->primaryKey => $id));
		$this->set('paymentStatus', $this->PaymentStatus->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PaymentStatus->create();
			if ($this->PaymentStatus->save($this->request->data)) {
				$this->Session->setFlash(__('The payment status has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The payment status could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PaymentStatus->exists($id)) {
			throw new NotFoundException(__('Invalid payment status'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PaymentStatus->save($this->request->data)) {
				$this->Session->setFlash(__('The payment status has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The payment status could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('PaymentStatus.' . $this->PaymentStatus->primaryKey => $id));
			$this->request->data = $this->PaymentStatus->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PaymentStatus->id = $id;
		if (!$this->PaymentStatus->exists()) {
			throw new NotFoundException(__('Invalid payment status'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->PaymentStatus->delete()) {
			$this->Session->setFlash(__('Payment status deleted'), 'flash/success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Payment status was not deleted'), 'flash/error');
		return $this->redirect(array('action' => 'index'));
	}
}
