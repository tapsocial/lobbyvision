<?php
App::uses('AppController', 'Controller');
/**
 * Contacts Controller
 *
 * @property Contact $Contact
 */
class ContactsController extends AppController {


public function beforeFilter() {
    parent::beforeFilter();
 $this->Auth->allow('setTimezone');
}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Contact->recursive = 0;
		$this->set('contacts', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if($this->Auth->user()){
			$contact=$this->Contact->find('first', array('conditions'=>array('user_id'=>$this->Auth->user('id'))));
			if(!$contact)
				return $this->redirect(array('action' => 'add'));
			$this->set('contact',$contact );
		}else{
		$this->Contact->id = $id;
		if (!$this->Contact->exists()) {
			throw new NotFoundException(__('Invalid contact'));
		}
		$this->set('contact', $this->Contact->read(null, $id));
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Contact->create();
			$this->request->data['Contact']['user_id']=$this->user['id'];
			if ($this->Contact->save($this->request->data)) {
				$this->Session->setFlash(__('Your contact information has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Your contact information could not be saved. Please, try again.'));
			}
		}
		$users = $this->Contact->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Contact->id = $id;
		if (!$this->Contact->exists()) {
			throw new NotFoundException(__('Invalid contact'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Contact->save($this->request->data)) {
				$this->Session->setFlash(__('Your contact information has been saved'));
				return $this->redirect(array('action' => 'view',$id));
			} else {
				$this->Session->setFlash(__('Your contact information could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Contact->read(null, $id);
		}
		$users = $this->Contact->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Contact->id = $id;
		if (!$this->Contact->exists()) {
			throw new NotFoundException(__('Invalid contact'));
		}
		if ($this->Contact->delete()) {
			$this->Session->setFlash(__('Contact deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Contact was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}

public function setTimezone(){


		$this->Contact->id=$this->user['Contact']['id'];
		$this->Contact->saveField('timezone',$_POST['timezone']);
			die('saved');

	}


}
