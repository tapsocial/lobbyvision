<?php
App::uses('AppController', 'Controller');
/**
 * Forecasts Controller
 *
 * @property Forecast $Forecast
 */
class ForecastsController extends AppController {

/**
 * index method
 *
 * @return void
 */
/*	public function index() {
		$this->Forecast->recursive = 0;
		$this->set('forecasts', $this->paginate());
	}


	public function view($id = null) {
		if (!$this->Forecast->exists($id)) {
			throw new NotFoundException(__('Invalid forecast'));
		}
		$options = array('conditions' => array('Forecast.' . $this->Forecast->primaryKey => $id));
		$this->set('forecast', $this->Forecast->find('first', $options));
	}


	public function add() {
		if ($this->request->is('post')) {
			$this->Forecast->create();
			if ($this->Forecast->save($this->request->data)) {
				$this->Session->setFlash(__('The forecast has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The forecast could not be saved. Please, try again.'));
			}
		}
		$weatherUndergrounds = $this->Forecast->WeatherUnderground->find('list');
		$this->set(compact('weatherUndergrounds'));
	}


	public function edit($id = null) {
		if (!$this->Forecast->exists($id)) {
			throw new NotFoundException(__('Invalid forecast'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Forecast->save($this->request->data)) {
				$this->Session->setFlash(__('The forecast has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The forecast could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Forecast.' . $this->Forecast->primaryKey => $id));
			$this->request->data = $this->Forecast->find('first', $options);
		}
		$weatherUndergrounds = $this->Forecast->WeatherUnderground->find('list');
		$this->set(compact('weatherUndergrounds'));
	}


	public function delete($id = null) {
		$this->Forecast->id = $id;
		if (!$this->Forecast->exists()) {
			throw new NotFoundException(__('Invalid forecast'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Forecast->delete()) {
			$this->Session->setFlash(__('Forecast deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Forecast was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}


	public function admin_index() {
		$this->Forecast->recursive = 0;
		$this->set('forecasts', $this->paginate());
	}


	public function admin_view($id = null) {
		if (!$this->Forecast->exists($id)) {
			throw new NotFoundException(__('Invalid forecast'));
		}
		$options = array('conditions' => array('Forecast.' . $this->Forecast->primaryKey => $id));
		$this->set('forecast', $this->Forecast->find('first', $options));
	}


	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Forecast->create();
			if ($this->Forecast->save($this->request->data)) {
				$this->Session->setFlash(__('The forecast has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The forecast could not be saved. Please, try again.'));
			}
		}
		$weatherUndergrounds = $this->Forecast->WeatherUnderground->find('list');
		$this->set(compact('weatherUndergrounds'));
	}


	public function admin_edit($id = null) {
		if (!$this->Forecast->exists($id)) {
			throw new NotFoundException(__('Invalid forecast'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Forecast->save($this->request->data)) {
				$this->Session->setFlash(__('The forecast has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The forecast could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Forecast.' . $this->Forecast->primaryKey => $id));
			$this->request->data = $this->Forecast->find('first', $options);
		}
		$weatherUndergrounds = $this->Forecast->WeatherUnderground->find('list');
		$this->set(compact('weatherUndergrounds'));
	}


	public function admin_delete($id = null) {
		$this->Forecast->id = $id;
		if (!$this->Forecast->exists()) {
			throw new NotFoundException(__('Invalid forecast'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Forecast->delete()) {
			$this->Session->setFlash(__('Forecast deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Forecast was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}*/
}
