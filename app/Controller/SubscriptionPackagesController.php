<?php
App::uses('AppController', 'Controller');
/**
 * SubscriptionPackages Controller
 *
 * @property SubscriptionPackage $SubscriptionPackage
 */
class SubscriptionPackagesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SubscriptionPackage->recursive = 0;
		$this->set('subscriptionPackages', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->SubscriptionPackage->id = $id;
		if (!$this->SubscriptionPackage->exists()) {
			throw new NotFoundException(__('Invalid subscription package'));
		}
		$this->set('subscriptionPackage', $this->SubscriptionPackage->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SubscriptionPackage->create();
			if ($this->SubscriptionPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The subscription package has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The subscription package could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->SubscriptionPackage->id = $id;
		if (!$this->SubscriptionPackage->exists()) {
			throw new NotFoundException(__('Invalid subscription package'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->SubscriptionPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The subscription package has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The subscription package could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->SubscriptionPackage->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->SubscriptionPackage->id = $id;
		if (!$this->SubscriptionPackage->exists()) {
			throw new NotFoundException(__('Invalid subscription package'));
		}
		if ($this->SubscriptionPackage->delete()) {
			$this->Session->setFlash(__('Subscription package deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Subscription package was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->SubscriptionPackage->recursive = 0;
		$this->set('subscriptionPackages', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->SubscriptionPackage->id = $id;
		if (!$this->SubscriptionPackage->exists()) {
			throw new NotFoundException(__('Invalid subscription package'));
		}
		$this->set('subscriptionPackage', $this->SubscriptionPackage->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->SubscriptionPackage->create();
			if ($this->SubscriptionPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The subscription package has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The subscription package could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->SubscriptionPackage->id = $id;
		if (!$this->SubscriptionPackage->exists()) {
			throw new NotFoundException(__('Invalid subscription package'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->SubscriptionPackage->save($this->request->data)) {
				$this->Session->setFlash(__('The subscription package has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The subscription package could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->SubscriptionPackage->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->SubscriptionPackage->id = $id;
		if (!$this->SubscriptionPackage->exists()) {
			throw new NotFoundException(__('Invalid subscription package'));
		}
		if ($this->SubscriptionPackage->delete()) {
			$this->Session->setFlash(__('Subscription package deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Subscription package was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}
}
