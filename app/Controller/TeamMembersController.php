<?php
App::uses('AppController', 'Controller');
/**
 * TeamMembers Controller
 *
 * @property TeamMember $TeamMember
 */
class TeamMembersController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->TeamMember->regenerateThumbnails();
		$this->TeamMember->recursive = 0;
		$options=array('TeamMember.user_id'=>$this->user['id']);
		$this->set('teamMembers', $this->paginate($options));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->TeamMember->id = $id;
		if (!$this->TeamMember->exists()) {
			throw new NotFoundException(__('Invalid team member'));
		}
		$this->set('teamMember', $this->TeamMember->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TeamMember->create();
			$this->request->data['TeamMember']['user_id']=$this->user['id'];
			
			$this->TeamMember->validate['image'] = array(
			 	'Empty' => array('check' => false)			        
			    );

			if ($this->TeamMember->save($this->request->data)) {
				$this->Session->setFlash(__('Your team member profile has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Your team member profile sidecould not be saved. Please, try again.'));
			}
		}
		$users = $this->TeamMember->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->TeamMember->id = $id;
		if (!$this->TeamMember->exists()) {
			throw new NotFoundException(__('Invalid team member'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TeamMember->save($this->request->data)) {
				$this->Session->setFlash(__('Your team member profile has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team member profile could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->TeamMember->read(null, $id);
		}
		$users = $this->TeamMember->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->TeamMember->id = $id;
		if (!$this->TeamMember->exists()) {
			throw new NotFoundException(__('Invalid team member'));
		}
		if ($this->TeamMember->delete()) {
			$this->Session->setFlash(__('Team member deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Team member was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TeamMember->recursive = 0;
		$this->set('teamMembers', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->TeamMember->id = $id;
		if (!$this->TeamMember->exists()) {
			throw new NotFoundException(__('Invalid team member'));
		}
		$this->set('teamMember', $this->TeamMember->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TeamMember->create();
			if ($this->TeamMember->save($this->request->data)) {
				$this->Session->setFlash(__('The team member has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team member could not be saved. Please, try again.'));
			}
		}
		$users = $this->TeamMember->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->TeamMember->id = $id;
		if (!$this->TeamMember->exists()) {
			throw new NotFoundException(__('Invalid team member'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TeamMember->save($this->request->data)) {
				$this->Session->setFlash(__('The team member has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team member could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->TeamMember->read(null, $id);
		}
		$users = $this->TeamMember->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->TeamMember->id = $id;
		if (!$this->TeamMember->exists()) {
			throw new NotFoundException(__('Invalid team member'));
		}
		if ($this->TeamMember->delete()) {
			$this->Session->setFlash(__('Team member deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Team member was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}
}
