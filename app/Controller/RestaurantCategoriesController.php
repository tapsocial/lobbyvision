<?php
App::uses('AppController', 'Controller');
/**
 * RestaurantCategories Controller
 *
 * @property RestaurantCategory $RestaurantCategory
 */
class RestaurantCategoriesController extends AppController {

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $userId = $this->user['id'];
        $this->RestaurantCategory->contain(['RestaurantPeriod']);
        $restaurantCategories = $this->paginate(null, ['RestaurantCategory.user_id' => $userId]);
        $this->set(compact('restaurantCategories'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $userId = $this->user['id'];
        if ($this->request->is('post')) {
            $this->RestaurantCategory->create();
            $this->request->data['RestaurantCategory']['user_id'] = $userId;
            if ($this->RestaurantCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The restaurant category has been saved'), 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The restaurant category could not be saved. Please, try again.'), 'flash/error');
            }
        }
        $restaurantPeriods = $this->RestaurantCategory->RestaurantPeriod->find('list', [
            'conditions' => ['RestaurantPeriod.user_id' => $userId]
        ]);
        $this->set(compact('restaurantPeriods'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $userId = $this->user['id'];
        if (!$this->RestaurantCategory->exists($id)) {
            throw new NotFoundException(__('Invalid restaurant category'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            parse_str($this->request->data['RestaurantCategory']['sort'], $newOrder);
            foreach($newOrder['sort_order'] as $sortOrder => $itemId) {
                $data = [
                    'id'        => $itemId,
                    'sort_order' => $sortOrder
                ];
                $this->RestaurantCategory->RestaurantMenuItem->save($data);
            }
            if ($this->RestaurantCategory->save($this->request->data)) {
                $this->Session->setFlash(__('The restaurant category has been saved'), 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The restaurant category could not be saved. Please, try again.'), 'flash/error');
            }
        } else {
            $options = array(
                'conditions' => array('RestaurantCategory.' . $this->RestaurantCategory->primaryKey => $id),
                'contain'    => ['RestaurantPeriod', 'RestaurantMenuItem' => ['order' => 'sort_order']]
            );
            $this->request->data = $this->RestaurantCategory->find('first', $options);
        }
        $restaurantPeriods = $this->RestaurantCategory->RestaurantPeriod->find('list', [
            'conditions' => ['RestaurantPeriod.user_id' => $userId]
        ]);
        $this->set(compact('users', 'restaurantPeriods'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->RestaurantCategory->id = $id;
        if (!$this->RestaurantCategory->exists()) {
            throw new NotFoundException(__('Invalid restaurant category'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->RestaurantCategory->delete()) {
            $this->Session->setFlash(__('Restaurant category deleted'), 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Restaurant category was not deleted'), 'flash/error');
        $this->redirect(array('action' => 'index'));
    }
}
