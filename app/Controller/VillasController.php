<?php
App::uses('AppController', 'Controller');
/**
 * Villas Controller
 *
 * @property Villa $Villa
 */
class VillasController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Villa->recursive = 0;
		$this->set('villas', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Villa->exists($id)) {
			throw new NotFoundException(__('Invalid villa'));
		}
		$options = array('conditions' => array('Villa.' . $this->Villa->primaryKey => $id));
		$this->set('villa', $this->Villa->find('first', $options));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		
		$this->Villa->recursive = 0;
		if($this->user['group_id'] == 1){
	    }
	    if($this->user['group_id'] == 2){ 
		$this->paginate = array(
        'conditions' => array('Villa.agent_id' => $this->user['Agent']['id'] ),
        'limit' => 10
         );
	}

		$this->set('villas', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Villa->exists($id)) {
			throw new NotFoundException(__('Invalid villa'));
		}
		$options = array('conditions' => array('Villa.' . $this->Villa->primaryKey => $id));
		$this->set('villa', $this->Villa->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Villa->create();

			if($this->user['group_id']==2)
				$this->request->data['Villa']['agent_id']=$this->user['Agent']['id'];
			if ($this->Villa->save($this->request->data)) {
				$this->Session->setFlash(__('The villa has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The villa could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$agents = $this->Villa->Agent->find('list');
		$countries = $this->Villa->Country->find('list');
		$states = $this->Villa->State->find('list',array('conditions'=>array('country_id'=>250)));
		$this->set(compact('agents', 'countries', 'states'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Villa->exists($id)) {
			throw new NotFoundException(__('Invalid villa'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Villa->save($this->request->data)) {
				$this->Session->setFlash(__('The villa has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The villa could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Villa.' . $this->Villa->primaryKey => $id));
			$this->request->data = $this->Villa->find('first', $options);
		}
		$this->set('id',$id);
		$agents = $this->Villa->Agent->find('list');
		$countries = $this->Villa->Country->find('list');
		$states = $this->Villa->State->find('list');
		$this->set(compact('agents', 'countries', 'states'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Villa->id = $id;
		if (!$this->Villa->exists()) {
			throw new NotFoundException(__('Invalid villa'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Villa->delete()) {
			$this->Session->setFlash(__('Villa deleted'), 'flash/success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Villa was not deleted'), 'flash/error');
		return $this->redirect(array('action' => 'index'));
	}
}
