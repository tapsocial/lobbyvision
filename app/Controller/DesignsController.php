<?php
App::uses('AppController', 'Controller');
/**
 * Designs Controller
 *
 * @property Design $Design
 */
class DesignsController extends AppController {

var $default_icon=array(
            'Widget'=>15,
            'TeamMember'=>11,
            'Promotion'=>16,
            'News'=>13,
            'Weather'=>12
            );

    function beforeFilter() {
         parent::beforeFilter();
         $this->Auth->allow('saveNews');
    }

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        Configure::load('Twitter.twitter', 'default', false);
        $this->consumerKey = Configure::read('Twitter.consumerKey');
        $this->consumerSecret = Configure::read('Twitter.consumerSecret');

        $this->set('default_icon',$this->default_icon);
    }

    public function updateField(){

    }

    private function populateDeafault($layout_id){
        $data=array('UserLayout' => array(
                    'layout_id' => $layout_id,
                    'user_id' => $this->user['id'],
                    'default' => 1
                ),
                'Widget' => array(
                    array(
                        'name' => 'News',
                        'class' => 'box-long',
                        'order' => 0
                    ),
                    array(
                        'name' => 'Promotion',
                        'class' => 'box-long',
                        'order' => 1
                    ),
                     array(
                        'name' => 'Weather',
                        'class' => 'box-small',
                        'order' => 2
                    ),
                    array(
                        'name' => 'TeamMember',
                        'class' => 'box-small',
                        'order' => 3
                    )
                )
            );

        $this->loadModel('UserLayout');
        $this->UserLayout->saveAssociated($data, array('deep' => true));

        $user_layout=$this->UserLayout->find('first',
                array('conditions'=>array('UserLayout.user_id'=>$this->user['id'],
                    'UserLayout.default'=>1))
                );
        //$design=array('')
        return $user_layout;
    }

    /**
     * @deprecated
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function index($layout_id=null,$user_id = null) {

        //$layout_id=1;

        if(!$user_id){
            $user_id=$this->user['id'];
        }
        if ($this->request->is('ajax')) {
            $this->set('ajax',true);
        }
        $this->loadModel('UserLayout');
        if(!$layout_id){

            $joins=array(
                array(
                        'table' => 'layouts',
                        'alias' => 'Layout',
                        'type' => 'INNER',
                        'conditions' => array('Layout.id = UserLayout.layout_id')
                    ),
                );

        $user_layout=$this->UserLayout->find('first',
            array(
                'contain' => ['Layout'],
                'conditions'=>array('UserLayout.user_id'=>$user_id,
                'UserLayout.default'=>1,'Layout.id !='=>'NULL'))
            );

        if($user_layout)
        $layout_id=$user_layout['UserLayout']['layout_id'];
        }
        else{
            $user_layout=false;
        }


        //debug($user_layout);

        if(!$user_layout || empty($user_layout)){
            $user_layout=$this->UserLayout->find('first',
            array('conditions'=>array('UserLayout.user_id'=>$user_id,
                'UserLayout.layout_id'=>$layout_id,
                ),
            'order'=>array('UserLayout.modified DESC'))
            );
            if($user_layout){
                $this->UserLayout->id=$user_layout['UserLayout']['id'];
                $this->UserLayout->saveField('default',1);
            }
        }

        if(isset($user_layout['Widget']) && !empty($user_layout['Widget']))
        foreach($user_layout['Widget'] as $key=>$widget){
            if(isset($user_layout['Widget']))
            $this->request->data[$user_layout['Widget'][$key]['name']]['number_of_display']=$user_layout['Widget'][$key]['number_of_display'];
        if(!$user_layout['Widget'][$key]['widget_icon_id'] && isset($this->default_icon[$user_layout['Widget'][$key]['name']])){
            $this->request->data[$user_layout['Widget'][$key]['name']]['widget_icon_id']=$this->default_icon[$user_layout['Widget'][$key]['name']];
        }else{
            $this->request->data[$user_layout['Widget'][$key]['name']]['widget_icon_id']=$user_layout['Widget'][$key]['widget_icon_id'];
        }

        //$this->request->data[$user_layout['Widget'][$key]['name']]['label']=$user_layout['Widget'][$key]['label'];
        }

        //debug($user_layout);
        if(!empty($user_layout) && !$layout_id)
            $layout_id=$user_layout['UserLayout']['layout_id'];

        if(!$layout_id){
            //$user_layout=$this->populateDeafault(1);
            $layout_id=1;
        }



        $this->loadModel('Layout');
        $this->Layout->recursive=-1;
        $layouts=$this->Layout->find('all');
        $this->Design->recursive=-1;
        $this->Design->User->id=$user_id;
        if (!$this->Design->User->exists()) {
            throw new NotFoundException(__('Invalid design'));
        }
        $design=$this->Design->find('first', array('conditions'=>array('user_id'=>$user_id)));
        if(!$design){
            $this->Session->setFlash(__('Please configure your logo, display name, and other Look & Feel elements before launching your Digital Sign.'));
            $this->redirect(array('action' => 'add'));
        }else{
            //$this->redirect(array('action' => 'edit',$design['Design']['id']));
        }

        $this->loadModel('WidgetIcon');
        $icons=$this->WidgetIcon->find('list');
        if(!empty($user_layout['Widget'])){
            $widgets = Set::classicExtract($user_layout['Widget'], '{n}.name');
        }else{
            $widgets =array();
        }
            $this->set(compact('structure','layouts','user_layout','ffwidgets', 'icons','widgets'));


        $this->set(array('design'=>$design,'layout_id'=>$layout_id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function add() {


        if ($this->request->is('post')) {
            $this->Design->create();
            $this->request->data['Design']['user_id']=$this->user['id'];
            $this->request->data['Design']['container_color']='#666666';
            $this->request->data['Design']['opacity']='50';
            $this->request->data['Design']['template_id']=1;
            if ($this->Design->save($this->request->data)) {
                $this->Session->setFlash(__('Please arrange widgets for your Digital Signage before viewing the Full Screen experience.'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The design could not be saved. Please, try again.'));
            }
        }
        $templates = $this->Design->Template->find('list');
        $this->set(compact('templates'));
    }

    public function update_logo()
    {
        $user_id                = $this->user['id'];
        $this->Design->User->id = $user_id;
        $design                 = $this->Design->find('first', array('conditions'=>array('user_id'=>$user_id)));
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Design']['user_id'] = $user_id;
            $this->Design->validate['logo'] = array(
                'Empty'    => array('check' => false),
                'HttpPost' => array('check' => false)
            );

            if ($this->Design->save($this->request->data)) {
                $this->Session->setFlash(__('Your logo has been successfully uploaded'));
            } else {
                $this->Session->setFlash(__('The logo could not be saved. Please, try again.'));
            }
        }

        if(isset($design['Design']['id'])) {
            $this->request->data = $this->Design->read(null, $design['Design']['id']);
        }
    }

    public function update_background() {

        $this->loadModel('BackgroundImage');
        $this->loadModel('BackgroundVideo');

        $user_id=$this->user['id'];
        $this->Design->User->id=$user_id;
        $this->Design->recursive=-1;
        $design=$this->Design->find('first', array('conditions'=>array('user_id'=>$user_id)));

        if ($this->request->is('post') || $this->request->is('put')) {

            if(isset($this->request->data['BackgroundImage'])){
                $this->request->data['BackgroundImage']['user_id']=$user_id;
                if ($this->BackgroundImage->save($this->request->data)) {
                    $this->Design->id=$design['Design']['id'];
                    $this->Design->saveField('background',$this->BackgroundImage->field('src'));
                    $this->Design->saveField('background_type',1);
                    $this->Session->setFlash(__('Your background image has been uploaded.'));
                    $this->redirect(array('action' => 'update_background'));
                } else {
                    $this->Session->setFlash(__('Your background image could not be upload. Please, try again.'));
                }
            }elseif(isset($this->request->data['Design'])){
                $this->request->data['Design']['user_id']=$user_id;
                if ($this->Design->save($this->request->data)) {
                    $this->Session->setFlash(__('Your background has been saved'));
                    $this->redirect(array('action' => 'update_background'));
                } else {
                    $this->Session->setFlash(__('The design could not be saved. Please, try again.'));
                }
            }
        } elseif(isset($design['Design']['id'])) {
            $this->request->data = $this->Design->read(null, $design['Design']['id']);
        }

        $backgroundImages = $this->BackgroundImage->find('all', array('order'=>'id DESC', 'conditions'=>array('user_id IN'=>array($user_id,0))));
        $backgroundVideos = $this->BackgroundVideo->find('all', array('order'=>'id DESC'));
        $this->set(compact('backgroundImages', 'backgroundVideos'));
    }

    public function bg_delete($id = null) {
        $this->loadModel('BackgroundImage');
        $this->BackgroundImage->id = $id;
        if (!$this->BackgroundImage->exists()) {
            throw new NotFoundException(__('Invalid background image'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->BackgroundImage->delete()) {
            $this->Session->setFlash(__('Background image deleted'));
            $this->redirect(array('action' => 'update_background'));
        }
        $this->Session->setFlash(__('Background image was not deleted'));
        $this->redirect(array('action' => 'update_background'));
    }

    /**
     * @deprecated
     *
     * @param string $theme
     * @param unknown $user_id
     * @throws NotFoundException
     */
    public function lobbyVision($theme='',$user_id = null) {
        //$this->Design->regenerateThumbnails();
        if(!$user_id){
            $user_id=$this->user['id'];
        }


        $this->Design->recursive=-1;
        $design=$this->Design->find('first', array('conditions'=>array('user_id'=>$user_id)));

        if(!$design){
            $this->Session->setFlash(__('Please configure your digital sign content before launching.'));
            $this->redirect(array('action' => 'add'));
        }else{
            //$this->redirect(array('action' => 'edit',$design['Design']['id']));
        }


        $this->loadModel('UserLayout');
        $user_layout=$this->UserLayout->find('first',
            array('conditions'=>array('UserLayout.user_id'=>$user_id,
                'UserLayout.default'=>1))
            );


        if(!empty($user_layout['Widget'])){
            $widgets = Set::classicExtract($user_layout['Widget'], '{n}.name');
        }
        else
            $widgets=array();

        unset($this->UserLayout);

        /*if(!isset($layout_id))
            $layout_id=2;*/
            $this->set(compact('user_layout'));
        if(!empty($user_layout)){
            $layout_id=$user_layout['UserLayout']['layout_id'];



        $this->loadModel('Layout');
        $this->Layout->recursive=-1;
        $layout=$this->Layout->find('first',array(
            'conditions'=>array('id'=>$layout_id)
            )
        );
        unset($this->Layout);

        $this->set(compact('layout'));

        $this->Design->User->id=$user_id;
        if (!$this->Design->User->exists()) {
            throw new NotFoundException(__('Invalid design'));
        }




        if($design['Design']['template_id']){
            $this->Design->Template->recursive=-1;
            $template=$this->Design->Template->find('first', array('conditions'=>array('id'=>$design['Design']['template_id'])));
            //echo $template['Template']['folder_name'];

        }
        if(in_array('Weather', $widgets))
            $weather=$this->getWeather();
        else
            $weather=false;
        //debug($weather);
        if($weather){
        $weather['Forecast']['forecast']=json_decode($weather['Forecast']['forecast']);
        $weather['Forecast']['current']=json_decode($weather['Forecast']['current']);
        }
        if(in_array('TeamMember', $widgets))
            $members=$this->getMembers();
        else
            $members=false;





//debug($tfeed);
        if(in_array('News', $widgets))
            $news=$this->getNews();
        else
            $news=false;


        //debug($news);

        $social_feeds=array();
        $tfeed=array();
        $tFeed=array();
        $tfeedHashtags=array();


        $facebookFeed=false;//$this->getFacebookFeed();

        if($design['Design']['show_ticker']){

            if($design['Design']['ticker_content']){
                $tc=$this->getTickerContent();
                if(!empty($tc)){
                $tcf=$tFeed=$this->formatTC($tc);
                //$social_feeds=$tcf;
                }
            }

            if($facebookFeed && !empty($facebookFeed))
        $social_feeds=$this->formatFacebookFeed($facebookFeed);





        if(isset($this->user['UserConnect']['value']) && $this->user['UserConnect']['value']!=''){
        $tuser=unserialize($this->user['UserConnect']['value']);
        $screen_name=$tuser['profile']['screen_name'];

    if($design['Design']['show_tweets']){

        $tfeed=$this->TwitterFeed($screen_name);
        $tfeedHashtags=$this->TwitterFeedByHash();

        if($tfeedHashtags && !empty($tfeedHashtags))
        $tfeed=array_merge($tfeed,$tfeedHashtags);
        if(!empty($tfeed))
        $tFeed=$this->formatTwitterFeed($tfeed);
        $social_feeds=array_merge($tFeed,$social_feeds);

        unset($tFeed,$tfeedHashtags);

    }

        }

        if(isset($social_feeds) && !empty($social_feeds))
        $social_feeds = Set::sort($social_feeds, '{n}.created', 'desc');
        }
        //debug($social_feeds);
        if(!empty($tc)){
        $fk=0;
        $ctk=0;
        if(!empty($social_feeds)){
            //debug($tcf);
            foreach($tcf as $key=>$val){


                    $ctk=$ctk+3;
                    //if(($fk%3) && isset($val['created']) && isset($tcf[$ctk]))
                    if(isset($social_feeds[$ctk-1]))
                    {
                        $tcf[$key]['created']=$social_feeds[$ctk-1]['created']+1;
                        //$ctk++;
                    }
                $fk++;
                }
            $social_feeds=array_merge($tcf,$social_feeds);
            $social_feeds = Set::sort($social_feeds, '{n}.created', 'desc');
            }else
            $social_feeds=array_merge($tcf,$social_feeds);
            unset($tc);
        }
        $this->loadModel('WidgetIcon');
        $widget_icons=$this->WidgetIcon->find('list');

        $this->set(compact('design','weather','members','news','social_feeds','widget_icons'));
    }
    else{
        $this->Session->setFlash(__('Please configure your digital sign content before launching.'));
            $this->redirect(array('action' => 'index'));
    }

    $this->theme=$theme;//$template['Template']['folder_name'];
    $this->layout='default';


    }

    private function TwitterFeedByHash($hashtags=null,$last_id=0){

        if(!isset($this->user['UserConnect']['value']) || $this->user['UserConnect']['value']=='')
            return false;

        $this->loadModel('Hashtag');
        $this->Hashtag->recursive=-1;
        $hashTags = $this->Hashtag->find('list',array('conditions'=>array('user_connect_id'=>$this->user['UserConnect']['id'])));
        if(!$hashTags || empty($hashTags))
            return false;
        $hashTags='#'.implode('+OR+#',$hashTags);
        $hashTags=str_replace('##','#',$hashTags);

        $twitterUser = unserialize($this->user['UserConnect']['value']);
        // CakeSession::write('Twitter.User.oauth_token', $twitterUser['oauth_token']);
        // CakeSession::write('Twitter.User.oauth_token_secret', $twitterUser['oauth_token_secret']);

        $feed=$this->Twitter->apiRequest('GET','/1.1/search/tweets.json',array('q'=>urlencode($hashTags),'include_entities'=>true,'since_id'=>$last_id));
        //debug($feed);
if(!isset($feed->body) || empty($feed->body))
    return false;

        $feed=json_decode($feed->body);
        if(isset($feed->statuses))
        $feed=$feed->statuses;
    else
        return false;

        return $feed;

    }

    private function TwitterFeed($screen_name,$last_id=0){
        if(!isset($this->user['UserConnect']['value']) || $this->user['UserConnect']['value']=='')
            return false;
        $twitterUser = unserialize($this->user['UserConnect']['value']);

        $feed=$this->Twitter->homeTimeline($screen_name,$last_id);
        //debug($screen_name);
        if($feed!='null')
        return $feed;
    else
        return array();


    }

    private function getFacebookFeed($page_id=null){
if(!isset($this->user['FConnect']['permissions']) || $this->user['FConnect']['permissions']==null)
    return false;
        //$this->loadModel('UserConnect');
$fuser=unserialize($this->user['FConnect']['value']);
//debug($fuser);

        $Facebook = new FB();




if(!$page_id)
    $page_id=$fuser['id'];

        $access_token = $Facebook->getAccessToken();
        $fuser['access_token']=$access_token;
        $fuser['wall_id']=$page_id;
        $this->set('fuser',$fuser);


           $feed= $Facebook->api($page_id.'/feed');
           //debug($feed);
return $feed;

    }

    private function formatFacebookFeed($facebookFeed=null){
        $feed=array();
        if(!$facebookFeed)
            return $feed;


        $i = 0;
        if($facebookFeed['data'] && !empty($facebookFeed['data'])){
            $feed=array();
        foreach($facebookFeed['data'] as $post) {

    if ($post['type'] == 'status' || $post['type'] == 'link' || $post['type'] == 'photo') {

        // open up an fb-update div
        $phtml="<span class=\"fb-update\">";

            // post the time
        if(isset($post['picture']))
            $phtml .='<span class="feed-img fimage"> <img src="'.$post['picture'].'" class="">
        <img src="'.Router::url('/', true).'img/ficon.png" class="ficon" /></span>';
        else
           $phtml .='<span class="feed-img fimage"><img src="https://graph.facebook.com/'.$post['from']['id'].'/picture" class=""><img src="'.Router::url('/', true).'img/ficon.png" class="ficon" /></span>';

            // check if post type is a status
            if ($post['type'] == 'status') {
                $phtml .="<small  class='date'>" . date("jS M, Y", (strtotime($post['created_time']))) . "</small>";
                 if (isset($post['story']) && empty($post['story']) === false) {
                    $phtml .="<p>" . $post['story'] . "</p>";
                } elseif (empty($post['message']) === false) {
                    $phtml .="<p>" . $post['message'] . "</p>";
                }
            }

            // check if post type is a link
            if ($post['type'] == 'link') {
                $phtml .="<small class='date'>" . date("jS M, Y", (strtotime($post['created_time']))) . "</small>";
                $phtml .="<p>" . $post['from']['name'] . "</p>";
                  if (isset($post['story']) && empty($post['story']) === false) {
                    $phtml .="<p>" . $post['story'] . "</p>";
                } elseif (empty($post['message']) === false) {
                    $phtml .="<p>" . $post['message'] . "</p>";
                }
               // echo "<p><a href=\"" . $post['link'] . "\" target=\"_blank\">" . $post['link'] . "</a></p>";
            }

            // check if post type is a photo
            if ($post['type'] == 'photo') {
                $phtml .="<small class='date'>" . date("jS M, Y", (strtotime($post['created_time']))) . "</small>";
                if (empty($post['story']) === false) {
                    $phtml .="<p>" . $post['story'] . "</p>";
                } elseif (empty($post['message']) === false) {
                    $phtml .="<p>" . $post['message'] . "</p>";
                }
              //  echo "<p><a href=\"" . $post['link'] . "\" target=\"_blank\">View photo &rarr;</a></p>";
            }

        $phtml .="</span>"; // close fb-update div

        $created=strtotime($post['created_time']);
        $feed[$i]=array('content'=>$phtml,'created'=>$created);


        $i++; // add 1 to the counter if our condition for $post['type'] is met
    }

    //  break out of the loop if counter has reached 10
    if ($i == 10) {
       // break;
    }

} // end the foreach statement

  //debug($feed);
    return $feed;
}


    }

    private function formatTwitterFeed($twitter_feed=null){


        if(!$twitter_feed)
            return array();

    $i = 0;
    if($twitter_feed && !empty($twitter_feed)){
        $feed=array();


    foreach($twitter_feed as $post) {
if(is_object($post))
        $post=Set::reverse ($post);

    //debug($post);

 //   if ($post['type'] == 'status' || $post['type'] == 'link' || $post['type'] == 'photo')
    {
//debug($post);
        // open up an fb-update div
        if(!isset($post['user']))
            continue;
        $phtml='<span class="fb-update tweet" id="'.$post['id_str'].'">';
$post['user']=(array) $post['user'];
            // post the time
$handler=$post['user']['name'];
if($post['in_reply_to_screen_name'])
    $handler .=' | '.$post['in_reply_to_screen_name'];

        if(isset($post['picture']))
            $phtml .='<span class="feed-img fimage"><img src="'.$post['picture'].'" class="ficon" /><span class="handler"><img src="'.Router::url('/', true).'img/ticon.png" class="ficon" />'.$handler.'</span></span>';
        else
           $phtml .='<span class="feed-img fimage"><img src="'.$post['user']['profile_image_url'].'" class=""><span class="handler"><img src="'.Router::url('/', true).'img/ticon.png" class="ficon" />'.$handler.'</span></span>';

            // check if post type is a status
           // if ($post['type'] == 'status')
            {
                $view = new View($this, false);
                $params=array('time'=>strtotime($post['created_at']));
                $asago = $view->element('timeago', $params);


                $phtml .="<small  class='date'>" .$asago . "</small>";
                 if (isset($post['text']) && empty($post['text']) === false) {
                    $phtml .="<p>" . $post['text'] . "</p>";
                   // echo $post['text'].'<br>';
                }
            }


//debug($id_str);
        $phtml .="</span>"; // close fb-update div
        $created=strtotime($post['created_at']);
        $feed[]=array('content'=>$phtml,'created'=>$created,'feed_id'=>$post['id_str']);
        $i++; // add 1 to the counter if our condition for $post['type'] is met
    }

    //  break out of the loop if counter has reached 10
    if ($i == 20) {
        break;
    }


    } // end the foreach statement

    //debug($feed);
    return $feed;
    }
    }

    public function loadTicker($last_id=0){
        //$last_id=0;

        $social_feeds=array();
        $tfeed=array();
        $tFeed=array();
        $tfeedHashtags=array();


        $facebookFeed=false;//$this->getFacebookFeed();

        //debug($facebookFeed);
        if($facebookFeed && !empty($facebookFeed))
        $social_feeds=$this->formatFacebookFeed($facebookFeed);



        if(isset($this->user['UserConnect']['value']) && $this->user['UserConnect']['value']!=''){
        $tuser=unserialize($this->user['UserConnect']['value']);
        $screen_name=$tuser['profile']['screen_name'];
        $tfeed=$this->TwitterFeed($screen_name,$last_id);


        $tfeedHashtags=$this->TwitterFeedByHash($last_id);

        if($tfeedHashtags && !empty($tfeedHashtags))
        $tfeed=array_merge($tfeed,$tfeedHashtags);
    if(!empty($tfeed))
        $tFeed=$this->formatTwitterFeed($tfeed);

        $social_feeds=array_merge($tFeed,$social_feeds);

        }

//debug($social_feeds);
        if(isset($social_feeds) && !empty($social_feeds))
        $social_feeds = Set::sort($social_feeds, '{n}.created', 'desc');
//debug($social_feeds);
if(!empty($social_feeds)){
  foreach($social_feeds as $feed){
    echo $feed['content'];
  }
}
die('');
    }

    public function loadElement($element){
        $this->layout=false;
        switch ($element) {
            case 'news':
                $news=$this->getNews();
                $widget=$this->getWidgets('News');
                $this->set('news',$news);
                $this->render('/Elements/'.$element, 'ajax');
                break;
            case 'event':
                $events=$this->getEvents();
                $widget=$this->getWidgets('Event');
                $this->set('events',$events);
                $this->render('/Elements/'.$element, 'ajax');
                break;
            case 'slideshow':
                $data=$this->getSlideshow();
                $widget=$this->getWidgets('Slideshow');
                $this->set('slideshows',$data);
                $this->render('/Elements/'.$element, 'ajax');
                break;
            case 'team_member':
                $data=$this->getMembers();
                $widget=$this->getWidgets('TeamMember');
                $this->set('members',$data);
                $this->render('/Elements/'.$element, 'ajax');
                break;
            case 'promotion':
                $data=$this->getPromotions();
                $widget=$this->getWidgets('Promotion');
                $this->set('promotions',$data);
                $this->render('/Elements/'.$element, 'ajax');
                break;
            case 'checkDesign':
                die($this->checkDesign($_POST['modified']));
                break;

            default:
                # code...
                break;
        }

    }

    private function getWidgets($model){
        $this->loadModel('UserLayout');
        $widget=$this->UserLayout->Widget->find('first',
            array('conditions'=>array('UserLayout.user_id'=>$this->user['id'],
                'UserLayout.default'=>1,'Widget.name'=>$model))
            );

        if(!empty($widget)){
            $this->set(array('widget'=>$widget['Widget'],
                            'widget_icons'=>array($widget['Widget']['widget_icon_id']=>$widget['WidgetIcon']['image'])));
        }
        unset($widget);

    }

    private function checkDesign($modified){
//die();
        $this->Design->recursive=-1;
        $design=$this->Design->find('first',array('fields'=>array('Design.modified'),
                'conditions'=>array('Design.user_id'=>$this->user['User']['id'],'Design.modified >'=>$modified)));



            if($design)
                return $design['Design']['modified'];
            else
                return false;

    }


}