<?php
App::uses('AppController', 'Controller');
/**
 * NewsFeeds Controller
 *
 * @property NewsFeed $NewsFeed
 */
class NewsFeedsController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->NewsFeed->recursive = 0;
		$this->set('newsFeeds', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->NewsFeed->exists($id)) {
			throw new NotFoundException(__('Invalid news feed'));
		}
		$options = array('conditions' => array('NewsFeed.' . $this->NewsFeed->primaryKey => $id));
		$this->set('newsFeed', $this->NewsFeed->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->NewsFeed->create();
			if ($this->NewsFeed->save($this->request->data)) {
				$this->Session->setFlash(__('The news feed has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The news feed could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$newsCategories = $this->NewsFeed->NewsCategory->find('list');
		$this->set(compact('newsCategories'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->NewsFeed->exists($id)) {
			throw new NotFoundException(__('Invalid news feed'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->NewsFeed->save($this->request->data)) {
				$this->Session->setFlash(__('The news feed has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The news feed could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('NewsFeed.' . $this->NewsFeed->primaryKey => $id));
			$this->request->data = $this->NewsFeed->find('first', $options);
		}
		$newsCategories = $this->NewsFeed->NewsCategory->find('list');
		$this->set(compact('newsCategories'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->NewsFeed->id = $id;
		if (!$this->NewsFeed->exists()) {
			throw new NotFoundException(__('Invalid news feed'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->NewsFeed->delete()) {
			$this->Session->setFlash(__('News feed deleted'), 'flash/success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('News feed was not deleted'), 'flash/error');
		return $this->redirect(array('action' => 'index'));
	}
}
