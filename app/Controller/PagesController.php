<?php
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public function beforeFilter()
    {
        parent::beforeFilter();

        $this->Auth->allow('display');
    }

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Pages';

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    /**
     * Displays a view
     *
     * @param
     *            mixed What page to display
     * @return void
     */
    public function display()
    {
        $path = func_get_args();
        if ($this->Auth->login() && $path[0] == 'home') {
            $user = $this->Auth->User();
            if ($user['group_id'] == 1) {
                $this->redirect(array( 'controller' => 'admin', 'action' => 'index', 'admin' => false));
            }
        }
        $count = count($path);
        if (! $count) {
            $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (! empty($path[0])) {
            $page = $path[0];
        }
        if (! empty($path[1])) {
            $subpage = $path[1];
        }
        if (! empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));
        $this->render(implode('/', $path));
    }
}
