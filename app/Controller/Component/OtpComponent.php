<?php
App::uses('Component', 'Controller');
class OtpComponent extends Component{
    var $components = array('Auth');
    public function initialize(Controller $controller) {
		//parent::initialize();
	}
	public function startup(Controller $controller) {
		//parent::startup();
	}
    function createOTP($parameters){
           return  $this->Auth->password(implode("", $parameters));
    }

    function authenticateOTP($otp,$parameters ){
        return $otp== $this->Auth->password(implode("", $parameters));
    }

}

?>
