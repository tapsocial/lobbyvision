<?php
App::uses('AppController', 'Controller');
/**
 * Hashtags Controller
 *
 * @property Hashtag $Hashtag
 */
class HashtagsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Hashtag->recursive = 0;
		$this->set('hashtags', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Hashtag->exists($id)) {
			throw new NotFoundException(__('Invalid hashtag'));
		}
		$options = array('conditions' => array('Hashtag.' . $this->Hashtag->primaryKey => $id));
		$this->set('hashtag', $this->Hashtag->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Hashtag->create();
			if ($this->Hashtag->save($this->request->data)) {
				$this->Session->setFlash(__('The hashtag has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The hashtag could not be saved. Please, try again.'));
			}
		}
		$userConnects = $this->Hashtag->UserConnect->find('list');
		$this->set(compact('userConnects'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Hashtag->exists($id)) {
			throw new NotFoundException(__('Invalid hashtag'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Hashtag->save($this->request->data)) {
				$this->Session->setFlash(__('The hashtag has been saved'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The hashtag could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Hashtag.' . $this->Hashtag->primaryKey => $id));
			$this->request->data = $this->Hashtag->find('first', $options);
		}
		$userConnects = $this->Hashtag->UserConnect->find('list');
		$this->set(compact('userConnects'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Hashtag->id = $id;
		if (!$this->Hashtag->exists()) {
			throw new NotFoundException(__('Invalid hashtag'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Hashtag->delete()) {
			$this->Session->setFlash(__('Hashtag deleted'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Hashtag was not deleted'));
		return $this->redirect(array('action' => 'index'));
	}

}
