<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {



    public $components = array(
        'Acl',
        'Auth' => array(
            'authenticate' => array('Form' => array( 'userModel' => 'User',
                                    'fields' => array(
                                                        'username' => 'username',
                                                        'password' => 'password'
                                                        ),

                                                )
                            ),
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            )
        ),
        'Session',
        'Email',
        'Facebook.Connect',
        //'Facebook.Api',
        //'DebugKit.Toolbar'
    );
    public $user;

    public $helpers = array('Html', 'Form', 'Session','Menubuilder.Menu','Custom','Facebook.Facebook');


    public function beforeFilter() {

    $this->user=$this->Auth->user();

    /*            $Email = new CakeEmail('smtp');
                $Email->template('welcome')
                ->emailFormat('html')
                ->subject('Welcome to TapSocial Digital Signage')
                ->to('sanjitbauli@gmail.com')
                ->from('accounts@tapsocial.net')
                ->send();*/

    if($this->Auth->user() && $this->user['group_id']==2){
  $this->loadModel('User');
    $this->User->recursive=0;
    $this->user=array_merge($this->user,$this->User->read(null,$this->Auth->user('id')));
$this->layout = 'dashboard';
//debug($this->Connect->role);

if($user = $this->Connect->registrationData()){
    debug($user);
}
 $Facebook = new FB();
   $u= $Facebook->api('/me');

   //$permissions = $Facebook->getLoginStatus("/me/permissions");
  // debug($permissions);
    //debug($u);

 //Router::url( $this->request->here, true );

{
    $this->set('fconnect',$this->Connect->user());
}
    }

    $this->set('authUser', $this->user);

    Configure::load('custom');
    Configure::load('timezone');

        $this->theme='Cakestrap';

        // For CakePHP 2.1 and up
        $this->Auth->allow('index');
        //Configure AuthComponent

        $this->Auth->loginAction = array('admin'=>false,'plugin'=>false,'controller' => 'users', 'action' => 'login');
        $this->Auth->logoutRedirect = array('admin'=>false,'plugin'=>false,'controller' => 'users', 'action' => 'login');

        $this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'index','admin'=>true);

        if($this->user['group_id']==1)
        {
        $this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'index');
        }
        if($this->user['group_id']==2)
        {
        $this->Auth->loginRedirect = array('controller' => 'villas', 'action' => 'index');
        }
    }
function afterFacebookLogin(){
    //Logic to happen after successful facebook login.
    $this->redirect('/custom_facebook_redirect');
}



}
