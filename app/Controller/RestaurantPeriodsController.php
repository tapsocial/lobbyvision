<?php
App::uses('AppController', 'Controller');
/**
 * RestaurantPeriods Controller
 *
 * @property RestaurantPeriod $RestaurantPeriod
 */
class RestaurantPeriodsController extends AppController {

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->RestaurantPeriod->recursive = 0;
        $userId  = $this->user['id'];
        $this->set('restaurantPeriods', $this->paginate(null, ['RestaurantPeriod.user_id' => $userId]));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        $userId  = $this->user['id'];
        if ($this->request->is('post')) {
            $this->RestaurantPeriod->create();
            $this->request->data['RestaurantPeriod']['user_id'] = $userId;
            if ($this->RestaurantPeriod->save($this->request->data)) {
                $this->Session->setFlash(__('The restaurant period has been saved'), 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The restaurant period could not be saved. Please, try again.'), 'flash/error');
            }
        }
        $restaurantCategories = $this->RestaurantPeriod->RestaurantCategory->find('list');
        $this->set(compact('users', 'restaurantCategories'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->RestaurantPeriod->exists($id)) {
            throw new NotFoundException(__('Invalid restaurant period'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->RestaurantPeriod->save($this->request->data)) {
                $this->Session->setFlash(__('The restaurant period has been saved'), 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The restaurant period could not be saved. Please, try again.'), 'flash/error');
            }
        } else {
            $options = array('conditions' => array('RestaurantPeriod.' . $this->RestaurantPeriod->primaryKey => $id));
            $this->request->data = $this->RestaurantPeriod->find('first', $options);
        }
        $users = $this->RestaurantPeriod->User->find('list');
        $restaurantCategories = $this->RestaurantPeriod->RestaurantCategory->find('list');
        $this->set(compact('users', 'restaurantCategories'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->RestaurantPeriod->id = $id;
        if (!$this->RestaurantPeriod->exists()) {
            throw new NotFoundException(__('Invalid restaurant period'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->RestaurantPeriod->delete()) {
            $this->Session->setFlash(__('Restaurant period deleted'), 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Restaurant period was not deleted'), 'flash/error');
        $this->redirect(array('action' => 'index'));
    }
}
