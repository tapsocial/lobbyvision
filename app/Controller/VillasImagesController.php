<?php
App::uses('AppController', 'Controller');
/**
 * VillasImages Controller
 *
 * @property VillasImage $VillasImage
 */
class VillasImagesController extends AppController {



/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->VillasImage->recursive = 0;
		$this->set('villasImages', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->VillasImage->exists($id)) {
			throw new NotFoundException(__('Invalid villas image'));
		}
		$options = array('conditions' => array('VillasImage.' . $this->VillasImage->primaryKey => $id));
		$this->set('villasImage', $this->VillasImage->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->VillasImage->create();
			if ($this->VillasImage->save($this->request->data)) {
				$this->Session->setFlash(__('The villas image has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The villas image could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$villas = $this->VillasImage->Villa->find('list');
		$this->set(compact('villas'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null,$villa_image_id=null) {
		$this->VillasImage->regenerateThumbnails();

		
		if ($this->request->is('post') || $this->request->is('put')) {
			
			$this->request->data['VillasImage']['dir']='villaimage';
			if ($this->VillasImage->save($this->request->data)) {
				$this->Session->setFlash(__('The villas image has been saved'), 'flash/success');
				//$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The villas image could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			if($villa_image_id){
				$options = array('conditions' => array('VillasImage.' . $this->VillasImage->primaryKey => $villa_image_id));
			$this->request->data = $this->VillasImage->find('first', $options);
			}
			
		}

		
		
		$this->set(compact('VillasImages'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->VillasImage->id = $id;
		if (!$this->VillasImage->exists()) {
			throw new NotFoundException(__('Invalid villas image'));
		}
		$this->request->allowMethod('post', 'delete');
		//$this->request->data['VillasImage']['filename']['remove'];
		if ($this->VillasImage->delete()) {
			$this->Session->setFlash(__('Villas image deleted'), 'flash/success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Villas image was not deleted'), 'flash/error');
		return $this->redirect(array('action' => 'index'));
	}
}
