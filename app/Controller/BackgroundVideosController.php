<?php
App::uses('AppController', 'Controller');
/**
 * BackgroundVideos Controller
 *
 * @property BackgroundVideo $BackgroundVideo
 */
class BackgroundVideosController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
    public function admin_index() {
        $this->BackgroundVideo->recursive = 0;
        $this->set('backgroundVideos', $this->paginate());
    }

/**
 * admin_add method
 *
 * @return void
 */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->BackgroundVideo->create();
            if ($this->BackgroundVideo->save($this->request->data)) {
                $this->Session->setFlash(__('The background video has been saved'), 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The background video could not be saved. Please, try again.'), 'flash/error');
            }
        }
    }

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_edit($id = null) {
        if (!$this->BackgroundVideo->exists($id)) {
            throw new NotFoundException(__('Invalid background video'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->BackgroundVideo->save($this->request->data)) {
                $this->Session->setFlash(__('The background video has been saved'), 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The background video could not be saved. Please, try again.'), 'flash/error');
            }
        } else {
            $options = array('conditions' => array('BackgroundVideo.' . $this->BackgroundVideo->primaryKey => $id));
            $this->request->data = $this->BackgroundVideo->find('first', $options);
        }
    }

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null) {
        $this->BackgroundVideo->id = $id;
        if (!$this->BackgroundVideo->exists()) {
            throw new NotFoundException(__('Invalid background video'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->BackgroundVideo->delete()) {
            $this->Session->setFlash(__('Background video deleted'), 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Background video was not deleted'), 'flash/error');
        $this->redirect(array('action' => 'index'));
    }
}
