<?php
App::uses('AppController', 'Controller');
/**
 * RestaurantMenuItems Controller
 *
 * @property RestaurantMenuItem $RestaurantMenuItem
 */
class RestaurantMenuItemsController extends AppController
{
    private function getCategoriesList()
    {
        $userId               = $this->user['id'];
        $restaurantCategories = [];
        $rs                   = $this->RestaurantMenuItem->RestaurantCategory->find('all', [
                'conditions' => ['RestaurantCategory.user_id' => $userId],
                'contain'    => ['RestaurantPeriod'],
        ]);
        foreach ($rs as $category) {
            $periods = [];
            foreach ($category['RestaurantPeriod'] as $period) {
                $periods[] = $period['name'];
            }
            sort($periods);
            $periods = implode(', ', $periods);
            $id      = $category['RestaurantCategory']['id'];
            $restaurantCategories[$id] = "{$category['RestaurantCategory']['name']} ($periods)";
        }
        return $restaurantCategories;
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {0;
        $userId  = $this->user['id'];
        $this->RestaurantMenuItem->contain('RestaurantCategory');
        $restaurantMenuItems = $this->paginate(null, ['RestaurantCategory.user_id' => $userId]);
        $this->set(compact('restaurantMenuItems'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add()
    {
        if ($this->request->is('post')) {

            foreach($this->request->data['RestaurantMenuItemPrice'] as $i => $val) {
                if (!$val['price']) {
                    unset ($this->request->data['RestaurantMenuItemPrice'][$i]);
                }
            }
            $this->RestaurantMenuItem->create();
            if ($this->RestaurantMenuItem->saveAll($this->request->data)) {
                $this->Session->setFlash(__('The restaurant menu item has been saved'), 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The restaurant menu item could not be saved. Please, try again.'), 'flash/error');
            }
        }
        $restaurantCategories = $this->getCategoriesList();
        $this->set(compact('restaurantCategories'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null)
    {
        if (!$this->RestaurantMenuItem->exists($id)) {
            throw new NotFoundException(__('Invalid restaurant menu item'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            foreach($this->request->data['RestaurantMenuItemPrice'] as $i => $val) {
                if (!$val['price']) {
                    unset ($this->request->data['RestaurantMenuItemPrice'][$i]);
                }
            }
            $this->RestaurantMenuItem->RestaurantMenuItemPrice->deleteAll([
                'restaurant_menu_item_id'  => $this->request->data['RestaurantMenuItem']['id'],
            ]);
            if ($this->RestaurantMenuItem->saveAll($this->request->data)) {
                $this->Session->setFlash(__('The restaurant menu item has been saved'), 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The restaurant menu item could not be saved. Please, try again.'), 'flash/error');
            }
        } else {
            $options = array(
                'conditions' => array('RestaurantMenuItem.' . $this->RestaurantMenuItem->primaryKey => $id),
                'contain'    => ['RestaurantMenuItemPrice'],
            );
            $this->request->data = $this->RestaurantMenuItem->find('first', $options);
        }
        $restaurantCategories = $this->getCategoriesList();
        $this->set(compact('restaurantCategories'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->RestaurantMenuItem->id = $id;
        if (!$this->RestaurantMenuItem->exists()) {
            throw new NotFoundException(__('Invalid restaurant menu item'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->RestaurantMenuItem->delete()) {
            $this->Session->setFlash(__('Restaurant menu item deleted'), 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Restaurant menu item was not deleted'), 'flash/error');
        $this->redirect(array('action' => 'index'));
    }
}
