<?php
App::uses('AppController', 'Controller');
/**
 * Layouts Controller
 *
 * @property Layout $Layout
 */
class LayoutsController extends AppController {


	function beforeFilter() {
 		parent::beforeFilter();
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Layout->recursive = 0;
		$this->set('layouts', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Layout->exists($id)) {
			throw new NotFoundException(__('Invalid layout'));
		}
		$this->Layout->validate['image'] = array(
			 	'Empty' => array('check' => true)			        
			    );

		$options = array('conditions' => array('Layout.' . $this->Layout->primaryKey => $id));
		$this->set('layout', $this->Layout->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Layout->create();
			if ($this->Layout->save($this->request->data)) {
				$this->Session->setFlash(__('The layout has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The layout could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Layout->exists($id)) {
			throw new NotFoundException(__('Invalid layout'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Layout->save($this->request->data)) {
				$this->Session->setFlash(__('The layout has been saved'), 'flash/success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The layout could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Layout.' . $this->Layout->primaryKey => $id));
			$this->request->data = $this->Layout->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Layout->id = $id;
		if (!$this->Layout->exists()) {
			throw new NotFoundException(__('Invalid layout'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Layout->delete()) {
			$this->Session->setFlash(__('Layout deleted'), 'flash/success');
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Layout was not deleted'), 'flash/error');
		return $this->redirect(array('action' => 'index'));
	}
}
