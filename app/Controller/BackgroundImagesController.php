<?php
App::uses('AppController', 'Controller');
/**
 * BackgroundImages Controller
 *
 * @property BackgroundImage $BackgroundImage
 */
class BackgroundImagesController extends AppController {


    public function admin_index() {
        $this->BackgroundImage->recursive = 0;
        $this->set('backgroundImages', $this->paginate());
    }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_view($id = null) {
        if (!$this->BackgroundImage->exists($id)) {
            throw new NotFoundException(__('Invalid background image'));
        }
        $options = array('conditions' => array('BackgroundImage.' . $this->BackgroundImage->primaryKey => $id));
        $this->set('backgroundImage', $this->BackgroundImage->find('first', $options));
    }

/**
 * admin_add method
 *
 * @return void
 */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->BackgroundImage->create();
            if ($this->BackgroundImage->save($this->request->data)) {
                $this->Session->setFlash(__('The background image has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The background image could not be saved. Please, try again.'));
            }
        }
    }

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_edit($id = null) {
        if (!$this->BackgroundImage->exists($id)) {
            throw new NotFoundException(__('Invalid background image'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->BackgroundImage->save($this->request->data)) {
                $this->Session->setFlash(__('The background image has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The background image could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('BackgroundImage.' . $this->BackgroundImage->primaryKey => $id));
            $this->request->data = $this->BackgroundImage->find('first', $options);
        }
    }

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null) {
        $this->BackgroundImage->id = $id;
        if (!$this->BackgroundImage->exists()) {
            throw new NotFoundException(__('Invalid background image'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->BackgroundImage->delete()) {
            $this->Session->setFlash(__('Background image deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Background image was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
