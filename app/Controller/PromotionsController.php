<?php
App::uses('AppController', 'Controller');
/**
 * Promotions Controller
 *
 * @property Promotion $Promotion
 */
class PromotionsController extends AppController {
public $paginate = array(
        'order' => array(
            'Promotion.start_displaying' => 'desc'
        )
    );
  function beforeFilter() {
     parent::beforeFilter();
    }
/**
 * index method
 *
 * @return void
 */
    public function index() {
        //$this->Promotion->regenerateThumbnails();
        $this->Promotion->recursive = 0;
        $options=array('Promotion.user_id'=>$this->user['id']);
        $this->set('promotions', $this->paginate($options));
    }

/**
 * add method
 *
 * @return void
 */
    public function add() {
        if ($this->request->is('post')) {
            $this->Promotion->create();
            $this->request->data['Promotion']['user_id']=$this->user['id'];

            $this->Promotion->validate['image'] = array(
                 'Empty' => array('check' => false)
                );
            $this->request->data['Promotion']['recurrence_id'] = $this->Promotion->Recurrence->getForModel($this->Promotion, $this->request->data);
            if ($this->Promotion->save($this->request->data)) {
                $this->Session->setFlash(__('Your Promotion/Sale event has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Your Promotion/Sale event could not be saved. Please, try again.'));
            }
        }
        $recurrences = $this->Promotion->Recurrence->getTypes();
        $recurrence  = 'never';
        $this->set(compact('recurrences', 'recurrence'));
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit($id = null) {
        $this->Promotion->id = $id;
        if (!$this->Promotion->exists()) {
            throw new NotFoundException(__('Invalid promotion'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Promotion']['recurrence_id'] = $this->Promotion->Recurrence->getForModel($this->Promotion, $this->request->data);
            if ($this->Promotion->save($this->request->data)) {
                $this->Session->setFlash(__('Your Promotion/Sale event has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Your Promotion/Sale event could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Promotion->find('first', [
                    'conditions' => ['Promotion.id' => $id],
                    'contain'    => ['Recurrence'],
            ]);
            $recurrences = $this->Promotion->Recurrence->getTypes();
            $recurrence  = $this->Promotion->Recurrence->detectType($this->request->data);
            $this->set(compact('recurrences', 'recurrence'));
        }
        $users = $this->Promotion->User->find('list');
        $this->set(compact('users'));
    }

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Promotion->id = $id;
        if (!$this->Promotion->exists()) {
            throw new NotFoundException(__('Invalid promotion'));
        }
        if ($this->Promotion->delete()) {
            $this->Session->setFlash(__('Promotion deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Promotion was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}
