<?php
App::uses('AppModel', 'Model');
/**
 * Layout Model
 *
 */
class Layout extends AppModel {

public $actsAs = array(
        'MeioUpload.MeioUpload' => array(
            'image' => array(
                'fields' => array(
                    'dir' => 'layout'
                )
            )
        )
    );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
	);
}
