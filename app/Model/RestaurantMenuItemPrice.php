<?php
App::uses('AppModel', 'Model');
/**
 * RestaurantMenuItemPrice Model
 *
 * @property RestaurantMenuItem $RestaurantMenuItem
 */
class RestaurantMenuItemPrice extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'price';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'restaurant_menu_item_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'price' => array(
            'money' => array(
                'rule' => array('money'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'RestaurantMenuItem' => array(
            'className' => 'RestaurantMenuItem',
            'foreignKey' => 'restaurant_menu_item_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
