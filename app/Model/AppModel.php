<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model
{
    /**
     * Transaction flag
     *
     * @var boolean
     */
    private $transactionBegun = false;

    public $actsAs = array('Containable');

    /**
     * All find calls should not fetch their child elements
     *
     * @param array $fields
     * @return array
     *
     * @todo test the case where find=all with no conditions
     */
    private function _forceEmptyContanable($fields, $conditions)
    {
        if (!is_array($conditions) && in_array($conditions, ['first', 'all'])
            && is_array($fields) && !empty($fields['conditions']) && is_array($fields['conditions'])
            && !isset($fields['contain'])
        ) {
            $fields['contain'] = false;
        }
        return $fields;
    }

    function find($conditions = NULL, $fields = array (), $order = NULL, $recursive = NULL) {
        if ($conditions == 'list' && is_array($this->displayField)) {
            return Set::combine($this->find('all', $fields, $order, $recursive), "{n}.{$this->name}.{$this->primaryKey}", $this->displayField);
        } else {
            if (isset($this->actsAs['Containable']) && ($this->actsAs['Containable'] === false)) {
                $this->Behaviors->unload('Containable');
            } else {
                $fields = $this->_forceEmptyContanable($fields, $conditions);
            }
            return parent::find($conditions, $fields, $order, $recursive);
        }
    }

       function unbindModelType($type) {
           $related=array(
                        'hasOne' => array_keys($this->hasOne),
                        'hasMany' => array_keys($this->hasMany),
                        'belongsTo' => array_keys($this->belongsTo),
                        'hasAndBelongsToMany' => array_keys($this->hasAndBelongsToMany));
            echo $type;
                foreach( $related as $relation => $model) {
                    if($relation==$type)
                        $this->unbindModel(array($relation => $model));
                }
        }

    public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
    $parameters = compact('conditions', 'recursive');
    if (isset($extra['group'])) {
        $parameters['fields'] = $extra['group'];
        if (is_string($parameters['fields'])) {
            // pagination with single GROUP BY field
            if (substr($parameters['fields'], 0, 9) != 'DISTINCT ') {
                $parameters['fields'] = 'DISTINCT ' . $parameters['fields'];
            }
            unset($extra['group']);
            $count = $this->find('count', array_merge($parameters, $extra));
        } else {
            // resort to inefficient method for multiple GROUP BY fields
            $count = $this->find('count', array_merge($parameters, $extra));
            $count = $this->getAffectedRows();
        }
    } else {
        // regular pagination
        $count = $this->find('count', array_merge($parameters, $extra));
    }
    return $count;
}


    /**
     * custom rule
     *
     * WTF??
     *
     * @param array $field
     * @param unknown $compare_field
     * @return boolean
     */
    function startBeforeEnd( $field=array(), $compare_field=null ) {
                foreach( $field as $key => $value ){
                        $v1 = $value;
                        $v2 = $this->data[$this->name][ $compare_field ];
                        if($v1 < $v2) {
                                return FALSE;
                        } else {
                                continue;
                        }
                }
                return TRUE;
        }

    protected function transactionBegin()
    {
        $db = $this->getDataSource();
        $this->transactionBegun = $db->begin();
    }

    protected function transactionCommit()
    {
        $db = $this->getDataSource();
        if ($this->transactionBegun) {
            return $db->commit() !== false;
        }
        return true;
    }

    protected function transactionRollback()
    {
        $db = $this->getDataSource();
        return $db->rollback();
    }

}
