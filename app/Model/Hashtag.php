<?php
App::uses('AppModel', 'Model');
/**
 * Hashtag Model
 *
 * @property UserConnect $UserConnect
 */
class Hashtag extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'hashtag';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_connect_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'hashtag' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'UserConnect' => array(
			'className' => 'UserConnect',
			'foreignKey' => 'user_connect_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
