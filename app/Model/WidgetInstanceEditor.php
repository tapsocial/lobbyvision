<?php
App::uses('WidgetInstanceAbstract', 'Model');
/**
 * WidgetInstanceEditor Model
 *
 * @property WidgetInstance $WidgetInstance
 */
class WidgetInstanceEditor extends WidgetInstanceAbstract
{
    public $singleton = false;
    public $icon      = 'fa-file-text-o';

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'widget_instance_id';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'widget_instance_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'WidgetInstance' => array(
            'className' => 'WidgetInstance',
            'foreignKey' => 'widget_instance_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );


    public function getWidgetContent($widget)
    {
        return null;
    }

}
