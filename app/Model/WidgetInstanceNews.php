<?php
App::uses('WidgetInstanceAbstract', 'Model');
/**
 * WidgetInstanceNews Model
 *
 * Does not use db table
 *
 * @property WidgetInstance $WidgetInstance
 */
class WidgetInstanceNews extends WidgetInstanceAbstract
{
    public $useTable = false;

    public $icon = 'fa-newspaper-o';

    /**
     * Loads all news stories for all feeds/categories if the category was not updated within last hour
     */
    private function loadNews()
    {
        App::uses('News', 'Model');
        App::uses('NewsFeed', 'Model');

        $this->News     = new News();
        $this->NewsFeed = new NewsFeed();
        $Newfeeds = $this->NewsFeed->find('all', array('fields'=>array('id','feed_url','last_updated')));

        foreach ($Newfeeds as $feed) {
            $feed_id  = $feed['NewsFeed']['id'];
            $feed_url = $feed['NewsFeed']['feed_url'];
            if(strtotime($feed['NewsFeed']['last_updated']) > (time()-3600)) {
                continue;
            }

            $data = array('News' => array());
            if($feed_url && $feed_url!='') {
                try {
                    // New method using SimpleXML
                    $news = Xml::build($feed_url);
                    foreach($news->channel->item as $item){
                        $data['News'][] = array(
                            'name'         => $item->title->__toString(),
                            'news_feed_id' => $feed_id,
                            'news'         => json_encode($item),
                            'publish_date' => date('Y-m-d h:i:s', strtotime($item->pubDate)),
                        );
                    }
                    unset($news->channel->item);
                    $this->NewsFeed->id=$feed_id;
                    $this->NewsFeed->saveField('last_updated',date('Y-m-d H:i:s'));
                } catch (Exception $e) {
                    //echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
                if(!empty($data['News'])) {
                    $this->News->deleteAll(array('News.news_feed_id' => $feed_id), false);
                }
                if(isset($data)) {
                    foreach($data['News'] as $news) {
                        try  {
                            $this->News->create($news);
                            $this->News->save();
                        } catch (Exception $e) {
                            // NOOP
                        }
                    }

                }
            }
        }
    }

    /**
     * Format date based on the requirements
     *
     * @link http://stackoverflow.com/a/501415/613408
     * @link https://gitlab.com/tapsocial/lobbyvision/issues/25
     *
     * @param  string $time DB time
     * @return string
     */
    private function friendlyTime($date)
    {
        $time   = strtotime($date);
        $second = 1;
        $minute = 60 * $second;
        $hour   = 60 * $minute;
        $day    = 24 * $hour;
        $month  = 30 * $day;

        $delta = time() - $time;
        if ($delta < 2 * $minute) {
            return "1m";
        }
        if ($delta < 60 * $minute) {
            return floor($delta / $minute) . "m";
        }
        if ($delta < 24 * $hour) {
            $hours   = floor($delta / $hour);
            $minutes = floor(($delta - ($hours * $hour)) / $minute);
            return "{$hours}h";
        }
        if ($delta < 48 * $hour) {
            return "24h";
        }
        if ($delta < 30 * $day) {
            return floor($delta / $day) . "d";
        }
        if ($delta < 12 * $month) {
            $months = floor($delta / $day / 30);
            return $months <= 1 ? "month" : $months . "month";
        } else {
            $years = floor($delta / $day / 365);
            return $years <= 1 ? "y" : $years . "y";
        }
    }

    public function getWidgetContent($widget)
    {
        $this->loadNews();
        $user_id = $widget['WidgetInstance']['user_id'];
        App::uses('News',              'Model');
        App::uses('UsersNewsCategory', 'Model');
        $this->News              = new News();
        $this->UsersNewsCategory = new UsersNewsCategory();
        $this->UsersNewsCategory->recursive=-1;

        $UsersNewsCategories = $this->UsersNewsCategory->find('list', array(
            'fields'     => array('id','news_category_id'),
            'conditions' => array('user_id'=>$user_id))
        );
        $news = $this->News->find('all',array(
            'fields'     => array('News.*','NewsFeed.name'),
            'contain'    => array('NewsFeed'),
            'conditions' => array('news_category_id' => $UsersNewsCategories),
            'limit'      => 30,
            'order'      => 'News.publish_date DESC'
        ));

        foreach ($news as $key => $new) {
            $story = json_decode($new['News']['news'], true);
            $date  = $this->friendlyTime($new['News']['publish_date']);
            $story['pubDate'] = $date;
            $news[$key]['News']['news'] = $story;
        }
        return $news;
    }
}
