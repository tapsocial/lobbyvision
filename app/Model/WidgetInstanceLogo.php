<?php
App::uses('WidgetInstanceAbstract', 'Model');
/**
 * WidgetInstanceLogo Model
 *
 * Does not use db table
 *
 * @property WidgetInstance $WidgetInstance
 */
class WidgetInstanceLogo extends WidgetInstanceAbstract
{
    public $useTable = false;

    public $icon = 'fa-cube';

    public function getWidgetContent($widget)
    {
        App::uses('Design', 'Model');
        $design = new Design();
        $userId = $widget['WidgetInstance']['user_id'];
        $design = $design->find('first', array('conditions'=>array('user_id'=>$userId)));

        $path = '/img/TapSocial_top.png';
        if (isset($design['Design']['logo'])) {
            $path = '/uploads/design/logo/' . $design['Design']['logo'];
        }
        $css = "url('$path') no-repeat center center ;";
        return $css;
    }
}
