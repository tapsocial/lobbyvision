<?php
App::uses('AppModel', 'Model');
/**
 * Slideshow Model
 *
 * @property User $User
 */
class Slideshow extends AppModel {


    public $actsAs = array(
        'MeioUpload.MeioUpload' => array(
            'image' => array(
                'fields' => array(
                    'dir' => 'slideshows'
                )
            )
        )
    );

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'image' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                'allowEmpty' => true,
                'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'user_id' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'order' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * Resize image after upload
     *
     * {@inheritDoc}
     * @see Model::afterSave()
     */
    public function afterSave($created, $options = array())
    {
        $data = $this->find('first', ['conditions' => ['id' => $this->id]]);
        $path = "uploads/slideshow/image/{$data['Slideshow']['image']}";
        $path = realpath($path);
        exec("convert '$path' -auto-orient -resize 1920x1920\> -quality 80 '$path'");
    }
}
