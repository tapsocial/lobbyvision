<?php
App::uses('WidgetInstanceAbstract', 'Model');
/**
 * WidgetInstanceWeather Model
 *
 * @property WidgetInstance $WidgetInstance
 */
class WidgetInstanceWeather extends WidgetInstanceAbstract {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'location';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'widget_instance_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'location' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'geocode' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'WidgetInstance' => array(
            'className' => 'WidgetInstance',
            'foreignKey' => 'widget_instance_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public $icon = 'fa-sun-o';

    /**
     * Loads the weather data from Wunderground API
     *
     * @param string $geocode
     */
    public function getWidgetContent($widget)
    {
        $key     = 'd7491273622b8189';
        $geocode = $widget['WidgetInstanceWeather']['geocode'];
        if (!$geocode) {
            $geocode = 'autoip';
        }
        $current_url = "http://api.wunderground.com/api/$key/conditions/q/$geocode.json";
        $forcast_url = "http://api.wunderground.com/api/$key/forecast10day/q/$geocode.json";
        // $hourly_url  = "http://api.wunderground.com/api/$key/hourly/q/$geocode.json";

        $json     = file_get_contents($forcast_url);
        $forecast = json_decode($json);

        $json     = file_get_contents($current_url);
        $current = json_decode($json);

        return compact('current', 'forecast');
    }

    public static function icon_url($theme, $url){
        $name   = str_replace('.gif','',str_replace('http://icons.wxug.com/i/c/k/', '', $url));
        $resurl = Router::url('/')."img/WidgetInstanceWeather/theme-$theme/$name.png";
        return $resurl;

    }
}
