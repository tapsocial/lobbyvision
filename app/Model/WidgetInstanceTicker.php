<?php
App::uses('WidgetInstanceAbstract', 'Model');

/**
 * WidgetInstanceTicker Model
 *
 * Does not use db table
 *
 * @property WidgetInstance $WidgetInstance
 */
class WidgetInstanceTicker extends WidgetInstanceAbstract
{

    public $useTable = false;

    public $hasMany = [
            'TickerContent'
    ];

    public $icon = 'fa-sticky-note-o';

    private function formatTC ($twitter_feed = null)
    {
        if (! $twitter_feed)
            return array();

        $i = 0;
        if ($twitter_feed && ! empty($twitter_feed)) {
            $feed = array();

            foreach ($twitter_feed as $post) {

                $phtml = '<span class="fb-update tc" id="' .
                         $post['TickerContent']['id'] . '">';

                if (isset($post['picture']))
                    $phtml .= '<span class="feed-img fimage"><img src="' .
                             $post['picture'] .
                             '" class="ficon" /><span class="handler"><img src="' .
                             Router::url('/', true) .
                             'img/ticon.png" class="ficon" />' . $handler .
                             '</span></span>';
                else
                    $phtml .= '<span class="feed-img fimage"><img src="' .
                             Router::url('/', true) .
                             'img/tp-small.png" class=""></span>';

                    // check if post type is a status
                    // if ($post['type'] == 'status')
                    // {
                    /*
                 * $view = new View($this, false);
                 * $params=array('time'=>strtotime($post['TickerContent']['created']));
                 * $asago = $view->element('timeago', $params);
                 */

                // $phtml .="<small class='date'>" .$asago . "</small>";
                if (isset($post['TickerContent']['content']) &&
                         empty($post['TickerContent']['content']) === false) {
                    $phtml .= "<p>" . $post['TickerContent']['content'] . "</p>";
                    // echo $post['text'].'<br>';
                }
                // }

                // debug($id_str);
                $phtml .= "</span>"; // close fb-update div
                $created = strtotime($post['TickerContent']['created']);
                $feed[] = array(
                        'content' => $phtml,
                        'created' => $created,
                        'feed_id' => $post['TickerContent']['id']
                );
                $i ++; // add 1 to the counter if our condition for
                       // $post['type'] is met
                           // break out of the loop if counter has reached 10
                           // if ($i == 20) {
                           // break;
                           // }
            }
        } // end the foreach statement

        // debug($feed);
        return $feed;
    }

    protected function getEditPath ($widget)
    {
        return '/user_connects/hashTags';
    }

    public function getWidgetContent($widget)
    {
        $userId = $widget['WidgetInstance']['user_id'];
        $tickerContents = $this->TickerContent->find('all',
                array(
                        'conditions' => array(
                                'user_id' => $userId
                        )
                ));
        $tickerContents = $this->formatTC($tickerContents);
        return $tickerContents;
    }
}
