<?php
App::uses('AppModel', 'Model');
/**
 * Forecast Model
 *
 * @property WeatherUnderground $WeatherUnderground
 */
class Forecast extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'forecast';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'weather_underground_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'forecast' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'url' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'WeatherUnderground' => array(
			'className' => 'WeatherUnderground',
			'foreignKey' => 'weather_underground_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
