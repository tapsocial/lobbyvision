<?php
App::uses('AppModel', 'Model');
/**
 * UsersNewsCategory Model
 *
 * @property NewsCategory $NewsCategory
 * @property User $User
 */
class UsersNewsCategory extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'news_category_id';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'news_category_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'NewsCategory' => array(
            'className' => 'NewsCategory',
            'foreignKey' => 'news_category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
