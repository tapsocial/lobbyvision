<?php
App::uses('WidgetInstanceAbstract', 'Model');
/**
 * WidgetInstanceLogo Model
 *
 * Does not use db table
 *
 * @property WidgetInstance $WidgetInstance
 * @property TeamMember     $TeamMember
 */
class WidgetInstanceTeam extends WidgetInstanceAbstract
{
    public $useTable = false;
    public $hasMany  = ['TeamMember'];

    public $icon = 'fa-users';

    public function getWidgetContent($widget)
    {
        $conditions = array('user_id'=>$widget['WidgetInstance']['user_id']);
        $members=$this->TeamMember->find('all',array('conditions'=>$conditions));
        return $members;
    }

}
