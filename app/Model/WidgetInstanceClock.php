<?php
App::uses('WidgetInstanceAbstract', 'Model');
/**
 * WidgetInstancClock Model
 *
 * @property WidgetInstance $WidgetInstance
 */
class WidgetInstanceClock extends WidgetInstanceAbstract
{
    const TIME_ONLY = 'time only';
    const DATE_ONLY = 'date only';
    const DATE_TIME = 'date and time';
    public $icon = 'fa-clock-o';

    public function getWidgetContent($widget)
    {
        return null;
    }
}
