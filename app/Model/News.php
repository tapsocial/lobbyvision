<?php
App::uses('AppModel', 'Model');
/**
 * News Model
 *
 * @property NewsFeed $NewsFeed
 */
class News extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
//public $primary_key='news_feed_id';

	public $validate = array(
		'news_feed_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'news' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'NewsFeed' => array(
			'className' => 'NewsFeed',
			'foreignKey' => 'news_feed_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
