<?php
App::uses('AppModel', 'Model');
/**
 * VillasImage Model
 *
 * @property Villa $Villa
 */
class VillasImage extends AppModel {
	var $name = 'VillasImage';
var $actsAs = array(
			'MeioUpload.MeioUpload' => array('path')
		);
var $thumbsizes =array('small'=>array('width'=>100,'height'=>100),
						'medium'=>array(300,300)
						);
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	
}
