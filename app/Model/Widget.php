<?php
App::uses('AppModel', 'Model');
/**
 * Widget Model
 *
 * @property UserLayout $UserLayout
 * @deprecated
 */
class Widget extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'user_layout_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'order' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'UserLayout' => array(
            'className' => 'UserLayout',
            'foreignKey' => 'user_layout_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'WidgetIcon' => array(
            'className' => 'WidgetIcon',
            'foreignKey' => 'widget_icon_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

}