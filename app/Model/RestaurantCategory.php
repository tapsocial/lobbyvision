<?php
App::uses('AppModel', 'Model');
/**
 * RestaurantCategory Model
 *
 * @property User $User
 * @property RestaurantMenuItem $RestaurantMenuItem
 */
class RestaurantCategory extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'RestaurantMenuItem' => array(
            'className' => 'RestaurantMenuItem',
            'foreignKey' => 'restaurant_category_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public $hasAndBelongsToMany = array(
        'RestaurantPeriod' => array(
            'joinTable'             => 'restaurant_category_period',
            'foreignKey'            => 'restaurant_category_id',
            'associationForeignKey' => 'restaurant_period_id',
        )
    );

    public function beforeValidate($options = array())
    {
        if (!is_array($this->data['RestaurantPeriod']['RestaurantPeriod']) || !count($this->data['RestaurantPeriod']['RestaurantPeriod'])) {
            $errorMessage = 'Category must belong to at least one period.';
            $this->invalidate('RestaurantPeriod', $errorMessage, false);
            $this->RestaurantPeriod->invalidate('RestaurantPeriod', $errorMessage, false);
            return  false;
        }
        return parent::beforeValidate($options);
    }

}
