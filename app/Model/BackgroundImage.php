<?php
App::uses('AppModel', 'Model');
/**
 * BackgroundImage Model
 *
 */
class BackgroundImage extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


var $actsAs = array('MeioUpload.MeioUpload' => array('src'));


/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
	);
}
