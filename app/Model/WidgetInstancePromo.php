<?php
App::uses('WidgetInstanceAbstract', 'Model');
/**
 * WidgetInstancePromo Model
 *
 * Does not use db table
 *
 * @property WidgetInstance $WidgetInstance
 * @property Promotion      $Promotion
 */
class WidgetInstancePromo extends WidgetInstanceAbstract
{
    public $useTable = false;
    public $hasMany  = ['Promotion', 'Recurrence'];

    public $icon = 'fa-usd';

    public function getWidgetContent($widget)
    {
        $userId = $widget['WidgetInstance']['user_id'];
        $promotions = $this->Promotion->find('all', [
            'conditions' => [
                'Promotion.user_id' => $userId,
                'Promotion.start    <= now()',
                'Promotion.end      >= now()',
            ],
        ]);
        $recurrences = $this->Recurrence->getMatches();
        if ($recurrences) {
            $promotions = array_merge($promotions, $this->Promotion->find('all', [
                'conditions' => [
                    'Promotion.user_id'       => $userId,
                    'Promotion.recurrence_id' => $recurrences,
                ],
            ]));
        }
        foreach ($promotions as $key => $promotion) {
            $path = "uploads/promotion/image/{$promotion['Promotion']['image']}";
            if (!file_exists($path)) {
                $promotions[$key]['Promotion']['image'] = false;
            }
        }
        return $promotions;
    }
}
