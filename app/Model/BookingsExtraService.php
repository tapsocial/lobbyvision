<?php
App::uses('AppModel', 'Model');
/**
 * BookingsExtraService Model
 *
 * @property Booking $Booking
 * @property PaymentType $PaymentType
 * @property PaymentStatus $PaymentStatus
 */
class BookingsExtraService extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'booking_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'booking_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'payment_type_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'payment_status_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Booking' => array(
			'className' => 'Booking',
			'foreignKey' => 'booking_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PaymentType' => array(
			'className' => 'PaymentType',
			'foreignKey' => 'payment_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PaymentStatus' => array(
			'className' => 'PaymentStatus',
			'foreignKey' => 'payment_status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
