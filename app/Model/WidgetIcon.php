<?php
App::uses('AppModel', 'Model');
/**
 * WidgetIcon Model
 *
 */
class WidgetIcon extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'image';


	var $actsAs = array(
				'MeioUpload.MeioUpload' => array(
					'image'=>array(
						'Emptyasda' => array('check' => false),
						'thumbsizes' => array(
		                'normal' => array('width'=>200, 'height'=>200),
		            	),
					)
				)
				);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'image' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
