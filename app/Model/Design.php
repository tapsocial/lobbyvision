<?php
App::uses('AppModel', 'Model');
/**
 * Design Model
 *
 * @property User $User
 */
class Design extends AppModel {
    var $actsAs = array(
        'MeioUpload.MeioUpload' => array('logo'=>array(
            'thumbnailSizes' => array(
                'small'  => '150x84',
                'medium' => '300x170',
                'large'  => '400x220'
            )
        ))
    );

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'display_name';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'display_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Required',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'maxLeght'=>array(
                'rule' => array('maxLength', 50),
                'message' => 'Maximum 50 char allowed.',

        )
            ),
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Template' => array(
            'className' => 'Template',
            'foreignKey' => 'template_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
