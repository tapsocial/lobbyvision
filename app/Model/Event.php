<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property User $User
 */
class Event extends AppModel
{

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'event_name';

    public $actsAs = array(
        'MeioUpload.MeioUpload' => array(
                'image'=>array(
                    'Emptyasda' => array('check' => false),
                    'thumbsizes' => array(
                    'normal' => array('width'=>200, 'height'=>200),
                    ),
                )
            )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'event_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Required',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'start' => array(
            'datetime' => array(
                'rule' => array('datetime'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'end' => array(
             'startBeforeEnd' => array(
                                'rule' => array('startBeforeEnd', 'start' ),
                'message' => 'The end time must be after the start time.',
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'description' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Required',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'image' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Required',
                'allowEmpty' => true,
                'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = ['User', 'Recurrence'];

    public function checkFutureDate($check) {
        $value = array_values($check);
        return CakeTime::fromString($value['0']) < CakeTime::fromString(date('Y-m-d'));
    }
}