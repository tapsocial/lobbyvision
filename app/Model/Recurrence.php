<?php
App::uses('AppModel', 'Model');
/**
 * Recurrence Model
 *
 * @property Event $Event
 *
 * @link http://stackoverflow.com/questions/5183630/calendar-recurring-repeating-events-best-storage-method
 */
class Recurrence extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'repeat_start';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'repeat_start' => array(
            'date' => array(
                'rule' => array('date'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'repeat_interval' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'repeat_year' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'repeat_month' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'repeat_day' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'repeat_week' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'repeat_weekday' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Event' => array(
            'className' => 'Event',
            'foreignKey' => 'recurrence_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function getTypes()
    {
        $recurrences = [
            'never'   => 'never',
            'daily'   => 'daily',
            'weekly'  => 'weekly',
            'monthly' => 'monthly',
            'yearly'  => 'yearly',
        ];
        return $recurrences;
    }

    public function detectType($recurrenceData)
    {
        $recurrence = 'never';
        if ($recurrenceData['Recurrence']['id']) {
            $recurrence = $recurrenceData['Recurrence'];
            switch (true) {
                case ($recurrence['repeat_interval'] == 1):
                    $recurrence = 'daily';
                    break;
                case ($recurrence['repeat_interval'] == 7):
                    $recurrence = 'weekly';
                    break;
                case (($recurrence['repeat_month'] == '*') && ((int) $recurrence['repeat_day'])):
                    $recurrence = 'monthly';
                    break;
                case (($recurrence['repeat_year'] == '*') && ((int) $recurrence['repeat_month']) && ((int) $recurrence['repeat_day'])):
                    $recurrence = 'yearly';
                    break;
                default:
                    $recurrence = 'never';
            }
        }
        return $recurrence;
    }

    public function getForModel(AppModel $model, $data)
    {
        if (!empty($data[$model->alias]['id'])) {
            $modelData = $model->find('first', [
                'conditions' => ["{$model->alias}.id" => $data[$model->alias]['id']],
                'contain'    => ['Recurrence'],
            ]);
        } else {
            $modelData = ['Recurrence' => []];
        }

        $start = new DateTime($data['Recurrence']['repeat_start']);
        switch ($data['Recurrence']['repeat_type']) {
            case 'never':
                if (!empty($modelData['Recurrence']['id'])) {
                    $this->delete($modelData['Recurrence']['id']);
                }
                return null;
            case 'daily':
                $modelData['Recurrence'] = array_merge($modelData['Recurrence'], [
                    'repeat_start'    => $data['Recurrence']['repeat_start'],
                    'repeat_interval' => '1',
                    'repeat_year'     => 'NULL',
                    'repeat_month'    => 'NULL',
                    'repeat_day'      => 'NULL',
                    'repeat_week'     => 'NULL',
                    'repeat_weekday'  => 'NULL',
                ]);
                break;
            case 'weekly':
                $modelData['Recurrence'] = array_merge($modelData['Recurrence'], [
                    'repeat_start'    => $data['Recurrence']['repeat_start'],
                    'repeat_interval' => '7',
                    'repeat_year'     => 'NULL',
                    'repeat_month'    => 'NULL',
                    'repeat_day'      => 'NULL',
                    'repeat_week'     => 'NULL',
                    'repeat_weekday'  => 'NULL',
                ]);
                break;
            case 'monthly':

                $modelData['Recurrence'] = array_merge($modelData['Recurrence'], [
                    'repeat_start'    => $data['Recurrence']['repeat_start'],
                    'repeat_interval' => 'NULL',
                    'repeat_year'     => '*',
                    'repeat_month'    => '*',
                    'repeat_day'      => $start->format('d'),
                    'repeat_week'     => '*',
                    'repeat_weekday'  => '*',
                    'foo' => 'bar',
                ]);
                break;
            case 'yearly':
                $modelData['Recurrence'] = array_merge($modelData['Recurrence'], [
                    'repeat_start'    => $data['Recurrence']['repeat_start'],
                    'repeat_interval' => 'NULL',
                    'repeat_year'     => '*',
                    'repeat_month'    => $start->format('m'),
                    'repeat_day'      => $start->format('d'),
                    'repeat_week'     => '*',
                    'repeat_weekday'  => '*',
                ]);
                break;
            default:
                throw new Exception('Recurrence type not defined');
        }
        $this->save($modelData['Recurrence']);
        return $this->id;
    }

    public function getMatches()
    {
        $today = new DateTime();
        $week  = (int) (($today->format('d') - 1) / 7) + 1;

        $sql = "SELECT * FROM recurrences Recurrence
        WHERE (
            DATEDIFF( '{$today->format('Y-m-d')}', repeat_start ) % repeat_interval = 0 )
            OR (
                     repeat_start  <= DATE('{$today->format('Y-m-d')}')
                AND (repeat_year    = {$today->format('Y')} OR repeat_year  = '*' )
                AND (repeat_month   = {$today->format('m')} OR repeat_month = '*' )
                AND (repeat_day     = {$today->format('d')} OR repeat_day   = '*' )
                AND (repeat_week    = $week    OR repeat_week  = '*' )
                AND (repeat_weekday = {$today->format('N')} OR repeat_weekday = '*' )
            )";
        $rs      = $this->query($sql);
        $matches = [];
        foreach ($rs as $record) {
            $matches[] = $record['Recurrence']['id'];
        }
        return $matches;
    }
}
