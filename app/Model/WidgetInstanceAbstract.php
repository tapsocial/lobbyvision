<?php
App::uses('AppModel', 'Model');
abstract class WidgetInstanceAbstract extends AppModel
{
    public $icon      = '';
    public $singleton = true;

    final public function getEditData($widget)
    {
        return [
            'editPath' => '/widget_instances/edit/' . $widget['WidgetInstance']['id'],
            'icon'     => $this->icon,
            'singleton' => $this->singleton,
        ];
    }

    abstract public function getWidgetContent($widget);

    public function getDesign(View $view)
    {
        $design = ['Design' => $view->viewVars['authUser']['Design']];
        return $design;
    }
}
