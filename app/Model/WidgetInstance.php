<?php
App::uses('AppModel', 'Model');
/**
 * WidgetInstance Model
 *
 * @property User         $User
 * @property WidgetMaster $WidgetMaster
 */
class WidgetInstance extends AppModel
{
    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'widget_master_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'gridster_col' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'gridster_row' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'gridster_size_x' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'gridster_size_y' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array('User', 'WidgetMaster');

    public function __construct($id = false, $table = null, $ds = null)
    {
        App::uses('WidgetMaster', 'Model');
        $masters = new WidgetMaster();
        $masters = $masters->find('all', ['contain' => false]);
        foreach ($masters as $master) {
            $widgetType     = "WidgetInstance".ucfirst($master['WidgetMaster']['name']);
            $this->hasOne[] = $widgetType;
        }
        parent::__construct($id = false, $table = null, $ds = null);
    }

    /**
     * Get the widgets for the user
     *
     * @param int $userId
     *
     * @todo confirm if we need to sort_by_row_and_col_asc
     */
    public function getByUser($userId)
    {
        $rs = $this->find('all', [
            'conditions' => ['user_id' => $userId],
            'contain'    => ['WidgetMaster'],
            'order'      => ['row', 'col'],
        ]);

        foreach($rs as $key => $widget) {
            $subWidget = 'WidgetInstance'.ucfirst($widget['WidgetMaster']['name']);
            $editData  = $this->$subWidget->getEditData($widget);

            if ($this->$subWidget->useTable) {
                $subData   = $this->$subWidget->find('first', [
                    'conditions' => ['widget_instance_id' => $widget['WidgetInstance']['id']],
                ]);
                $rs[$key] += $subData;
            }
            $rs[$key]['WidgetInstance'] += $editData;
        }
        return $rs;
    }

    /**
     * Delete from databsae removed widgets from the display
     *
     * @param  array $widgets
     * @return int
     */
    public function deleteRemoved($userId, $widgets)
    {
        $existing = [-1, 0];
        foreach ($widgets as $widget) {
            if (!empty($widget['id']))
                $existing[] = $widget['id'];
        }
        if (!$existing) return false;
        $toDelete = array_keys($this->find('list', [
            'conditions' => [
                'user_id'   => $userId,
                'id not in' => $existing,
            ],
        ]));
        if ($toDelete) $this->deleteAll(['id' => $toDelete]);
        return count($toDelete);
    }

    /**
     *
     * {@inheritDoc}
     * @see Model::save()
     *
     * @todo set widget name
     */
    public function save($data = null, $validate = true, $fieldList = array())
    {
        $this->set($data);
        if (empty($this->data['WidgetInstance']['id'])) {
            $this->transactionBegin();
            $master = $this->WidgetMaster->find('first', [
                'conditions' => [
                    'name' => $this->data['WidgetInstance']['type'],
                ],
            ]);
            $this->set([
                'name'             => $this->data['WidgetInstance']['type'],
                'widget_master_id' => $master['WidgetMaster']['id'],
                'size_x'           => $master['WidgetMaster']['width_def'],
                'size_y'           => $master['WidgetMaster']['height_def'],
            ]);
            $data = parent::save($data, $validate, $fieldList);
            $subWidget = 'WidgetInstance'.ucfirst($data['WidgetInstance']['type']);

            if (!$this->$subWidget->useTable) {
                $this->transactionCommit();
                return true;
            } else {
                $this->$subWidget->create([
                    'widget_instance_id' => $this->id,
                ]);
                $subWidget = $this->$subWidget->save();
                if ($subWidget) {
                    $this->transactionCommit();
                    return $subWidget;
                } else {
                    $this->transactionRollback();
                    throw new \Exception('Subwidget not able to be stored');
                }
            }
        } else {
            return parent::save($data, $validate, $fieldList);
        }

    }

    public function getData($id)
    {
        $options = array(
            'conditions' => array('WidgetInstance.' . $this->primaryKey => $id),
            'contain'    => ['WidgetMaster'],
        );
        $widget    = $this->find('first', $options);
        $subWidget = 'WidgetInstance' . ucfirst($widget['WidgetMaster']['name']);
        if ($this->$subWidget->useTable === false) {
            $subWidget = [];
        } else {
            $subWidget = $this->$subWidget->find('first', [
                    'conditions' => ["$subWidget.widget_instance_id" => $widget['WidgetInstance']['id']]
            ]);
        }
        $widget += $subWidget;
        return $widget;
    }

}
