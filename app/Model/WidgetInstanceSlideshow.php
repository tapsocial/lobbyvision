<?php
App::uses('WidgetInstanceAbstract', 'Model');
/**
 * WidgetInstanceLogo Model
 *
 * Does not use db table
 *
 * @property WidgetInstance $WidgetInstance
 */
class WidgetInstanceSlideshow extends WidgetInstanceAbstract
{
    public $useTable = false;

    public $icon = 'fa-picture-o';

    public function getWidgetContent($widget)
    {

        App::uses('Slideshow', 'Model');

        $this->Slideshow = new Slideshow();

        $user_id    = $widget['WidgetInstance']['user_id'];
        $slideshows = $this->Slideshow->find('all', array(
            'conditions' => array('user_id'=>$user_id),
            'order'      => 'order DESC',
        ));
        return $slideshows;

    }

}
