<?php
App::uses('WidgetInstanceAbstract', 'Model');
/**
 * WidgetInstancClock Model
 *
 * @property WidgetInstance                   $WidgetInstance
 * @property WidgetInstanceRestaurantCategory $WidgetInstanceRestaurantCategory
 */
class WidgetInstanceRestaurant extends WidgetInstanceAbstract
{
    public $icon      = 'fa-cutlery';
    public $singleton = false;
    public $hasMany   = ['WidgetInstanceRestaurantCategory'];

    public function getPossibleCategoriesGroupByPeriod($widget)
    {
        $userId  = $widget['WidgetInstance']['user_id'];
        $periods = $this->WidgetInstanceRestaurantCategory->RestaurantCategory->RestaurantPeriod->find('all', [
            'conditions' => ['user_id' => $userId],
            'contain'    => ['RestaurantCategory'],
        ]);

        $preSelected = $this->WidgetInstanceRestaurantCategory->find('list', [
            'conditions' => ['widget_instance_restaurant_id' => $widget['WidgetInstanceRestaurant']['id']],
        ]);
        foreach ($periods as $key  => $period) {
            $categories = [];
            $selected   = false;
            foreach ($period['RestaurantCategory'] as $category) {
                $categories[$category['id']] = $category['name'];
                if (in_array($category['id'], $preSelected)) {
                    $selected = $category['id'];
                }
                if (!$selected)  {
                    $selected = $category['id'];
                }
            }
            $periods[$key]['RestaurantCategory']         = $categories;
            $periods[$key]['RestaurantSelectedCategory'] = $selected;
        }
        return $periods;
    }

    /**
     * Returns RestaurantMenuItem[] with it's prices
     *
     * {@inheritDoc}
     *
     * @see WidgetInstanceAbstract::getWidgetContent()
     */
    public function getWidgetContent($widget)
    {
        $userId = $widget['WidgetInstance']['user_id'];
        $time   = new DateTime();
        $time   = $time->format('H');

        $period = $this->WidgetInstanceRestaurantCategory->RestaurantCategory->RestaurantPeriod->find('first', [
            'conditions' => [
                'user_id'   => $userId,
                'starts <=' => $time,
                'ends >'    => $time
            ]
        ]);

        $categories = implode(', ', $this->WidgetInstanceRestaurantCategory->find('list', [
            'conditions' => ['widget_instance_restaurant_id' => $widget['WidgetInstanceRestaurant']['id']],
        ]));
        if (!$categories) {
            return [];
        }
        $categories = $this->query("SELECT RestaurantCategory.* FROM restaurant_categories RestaurantCategory
                JOIN restaurant_category_period hab2m            ON (RestaurantCategory.id = hab2m.restaurant_category_id)
                JOIN restaurant_periods         RestaurantPeriod ON (RestaurantPeriod.id   = hab2m.restaurant_period_id)
                WHERE   RestaurantCategory.id IN ($categories)
                    AND RestaurantPeriod.id   = {$period['RestaurantPeriod']['id']}
        ");

        $tmp = [];
        foreach ($categories  as $category) {
            $tmp[] = (int) $category['RestaurantCategory']['id'];
        }
        $categories = $tmp;
        $widget = [];
        App::uses('RestaurantMenuItem', 'Model');
        $this->RestaurantMenuItem = new RestaurantMenuItem();
        $items = $this->RestaurantMenuItem->find('all', [
            'contain'    => ['RestaurantMenuItemPrice'],
            'conditions' => ['restaurant_category_id' => $categories],
            'order'      => ['sort_order'],
        ]);
        foreach ($items as $key => $value) {
            if (empty($value['RestaurantMenuItemPrice'])) {
                unset($items[$key]);
            }
        }
        return $items;
    }

    public function save($data = null, $validate = true, $fieldList = array())
    {
        if (isset($data['WidgetInstanceRestaurantCategory'])) {
            $this->WidgetInstanceRestaurantCategory->deleteAll(['widget_instance_restaurant_id' => $data['id']]);
        }
        return parent::save($data, $validate, $fieldList);
    }

}