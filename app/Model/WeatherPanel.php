<?php
App::uses('AppModel', 'Model');
/**
 * WeatherPanel Model
 *
 * @property WeatherUnderground $WeatherUnderground
 */
class WeatherPanel extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'panel_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'panel_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'code' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'WeatherUnderground' => array(
			'className' => 'WeatherUnderground',
			'joinTable' => 'weather_undergrounds_weather_panels',
			'foreignKey' => 'weather_panel_id',
			'associationForeignKey' => 'weather_underground_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
