<?php
App::uses('AppModel', 'Model');
/**
 * WeatherUndergroundsWeatherPanel Model
 *
 * @property WeatherUnderground $WeatherUnderground
 * @property WeatherPanel $WeatherPanel
 */
class WeatherUndergroundsWeatherPanel extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'weather_underground_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'weather_underground_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'weather_panel_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'WeatherUnderground' => array(
			'className' => 'WeatherUnderground',
			'foreignKey' => 'weather_underground_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'WeatherPanel' => array(
			'className' => 'WeatherPanel',
			'foreignKey' => 'weather_panel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
