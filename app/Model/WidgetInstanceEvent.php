<?php
App::uses('WidgetInstanceAbstract', 'Model');
/**
 * WidgetInstancEvent Model
 *
 * Does not use db table
 *
 * @property WidgetInstance $WidgetInstance
 * @property Event          $Event
 */
class WidgetInstanceEvent extends WidgetInstanceAbstract
{
    public $useTable = false;
    public $hasMany  = ['Event', 'Recurrence'];

    public $icon = 'fa-calendar';

    public function getWidgetContent($widget)
    {
        $userId = $widget['WidgetInstance']['user_id'];
        $events = $this->Event->find('all', [
            'conditions' => [
                'Event.user_id' => $userId,
                'Event.start    <= now()',
                'Event.end      >= now()',
            ],
        ]);
        $recurrences = $this->Recurrence->getMatches();
        if ($recurrences) {
            $events = array_merge($events, $this->Event->find('all', [
                'conditions' => [
                    'Event.user_id'       => $userId,
                    'Event.recurrence_id' => $recurrences,
                ],
            ]));
        }
        foreach ($events as $key => $event) {
            $path = "uploads/event/image/{$event['Event']['image']}";
            if (!file_exists($path)) {
                $events[$key]['Event']['image'] = false;
            }
        }
        return $events;
    }
}
