<?php
App::import('Model', 'Menubuilder.Menu');
App::uses('Sanitize', 'Utility');

App::uses('AppHelper', 'View/Helper');

class MenuHelper extends AppHelper{

    var $Menu;
    var $Acl;

    var $helpers = array(
        'Html',
        'Session'
    );

    public function __construct(View $View, $settings = array()) {
        parent::__construct($View, $settings);
        $this->Menu = new Menu();
    }

    /*
     * Outputs an html structure of a <ul> and nested <li> elements
     * that make the current users navigation
     *
     * @params $slug lowercase menu slug
     **/
    function display( $slug = null, $options = array() ){
        if ($slug == null) return '';
        $slug = Sanitize::clean($slug);

        $menuarray=$this->get($slug);

        if(empty($menuarray)){
            echo "menu with slug ' ".$slug." ' was not found!";
        }else{
        //	debug($menuarray);
            $menus=$this->createMenuTree2($menuarray);
            //debug($menus);

            echo $this->make_nav_lists_2(json_encode($menus), $options );
            //echo $this->make_nav_lists(json_encode($menuarray), $options ); //convert Menuitems array to json object
        }
    }

  /**
   * Creates a menu tree
   * @param  array $menu_items
   * @return array
   */
  public function createMenuTree2($menu_items, $parentid = null){

        $menu_items = Set::sort($menu_items, '{n}.order', 'ASC');
    $menu_structure = array();
    foreach($menu_items as $item) {
      if( $item['nested'] == 0 && is_null($parentid) ) {
        //find children for this menu
        $kids = $this->createMenuTree2($menu_items, $item['id']);
        if( ! empty($kids) ) $item['children'] = $kids ;

        $menu_structure[] = $item;
      } else{

        // this is a child,
        if( $item['parent_id'] == $parentid ) {
          //does the child have nested children
          $kids = $this->createMenuTree2($menu_items, $item['id']);
          //unset this menu
          if( ! empty($kids) ) $item['children'] = $kids ;

          $menu_structure[] = $item;
        }

      }
    }

    return $menu_structure;

  }


    /*
     * Returns an associative array containing
     * menu items,accepts a slug of the menu
     *
     * @params $slug lowercase menu slug
     **/
    function get($slug = null){
        if ($slug == null) return '';

        $slug = Sanitize::clean($slug);

        $current_controller=$this->params['controller'];
        $current_action=$this->params['action'];


        //fetch the menu contains: menu,Menuitems
        $menu = $this->Menu->find('first',
            array(
                'contain' => ['Menuitem'],
                'conditions' => array(
                    'Menu.slug' => $slug,
                ),
                //'order' => array('Menuitem.order', 'Menuitem.id DESC'),
            )
        );
        $menu['Menuitem'] = Set::sort($menu['Menuitem'], '{n}.Menuitem.order', 'ASC');
        if( isset($menu['Menuitem']) )
            return $menu['Menuitem'];

    }


    /**
     * Make Navigation list elements
     *
     * @params $Menuitems array() an array pf menu items
     *
     * generate list elements,recurse if menu has a second level
     */	private function make_nav_lists($menuobj, $options = array()){
        $Menuitems=json_decode($menuobj); //decode json string of children

 {
        if( array_key_exists('menu_class', $options ) ) {
            $navlists = '<ul class="'. $options['menu_class'] . '">';
        } else {
            $navlists="<ul>";
        }
}
//$navlists='';
        foreach($Menuitems as $menu){
//debug($menu);
            $navlists.='<li '.$this->is_current_controller($menu->controller).'>';
            $navlists.='<a href="'. $this->webroot. $menu->url.'" '. $this->is_current_action($menu->controller,$menu->action) .'><span>'.$menu->label.'</span></a>';

            //if menu element has a children note: $menu->children is stored as a json string
            if(isset($menu->children) && $menu->children !='' ){
                $navlists.=$this->make_nav_lists($menu->children);
            }
            $navlists.="</li>";
        }

        $navlists .= '</ul>';

        return $navlists;
    }

    /**
     * Make Navigation list elements
     *
     * @params $menuitems array() an array pf menu items
     *
     * generate list elements,recurse if menu has a second level
     */
    function make_nav_lists_2($menuobj,$options=array()){
        if(is_array($menuobj))
            $menuitems=$menuobj;
        else
        $menuitems=json_decode($menuobj); //decode json string of children


if(isset($options['class']) && $options['class']) {

            $navlists="<ul class='".$options['class']."'>";

        }else {
        $navlists="<ul class='nav'>";
        }

        $user=$this->Session->read('Auth.User.email'); //get current user

if(isset($options['home']) && $options['home'])
{

    $navlists.='<li class="'.$this->is_home().'">';
            $navlists.='<a  href="'.$this->webroot.'"  class="'.$this->is_home().'"><span>Home</span></a>';
    $navlists.='</li>';
    }

        foreach($menuitems as $menu){

            //if menu element has a children note: $menu->children is stored as a json string
            if(isset($menu->children) && $menu->children !='' ){
        $navlists.='<li  class="dropdown '.$this->is_current_controller($menu->controller).' '.$this->is_current_action($menu->controller,$menu->action).'">';
            $navlists.='<a href="#" '. $this->is_current_action($menu->controller,$menu->action) .'
             class=dropdown-toggle"
data-toggle="dropdown"
>'.$menu->label.' <b class="caret"></b></a>';
                //$navlists.="<ul class='dropdown-menu'>";

                $navlists.=$this->make_nav_lists_2($menu->children,array('class'=>'dropdown-menu'));
            //	$navlists.="</ul>";
            }else {
                //.$this->is_current_controller($menu->controller).' '
            $navlists.='<li class="'. $this->is_current_action($menu->controller,$menu->action) .'">';
            $navlists.='<a href="'.Router::url('/', false).$menu->url.'"  class="'. $this->is_current_action($menu->controller,$menu->action) .'"><span>'.$menu->label.'</span></a>';

                }



            $navlists.="</li>";
        }

        $navlists .= '</ul>';

        return $navlists;
    }

    /**
     * Is Current Controller
     *
     * @params $urlcontroller
     * checks if controller for the current link matches current controller,
     * then add class current to highlight it
     */
    function is_current_controller($urlcontroller=""){
        $urlcontroller = strtolower($urlcontroller);

        if( $this->request->params['controller'] == $urlcontroller ){
            return "class='current'";
        }else{
            return "";
        }
    }


    /**
     * Is Current Action
     *
     * @params $urlcontroller
     * checks if controller for the current link matches current controller,
     * then add class current to highlight it
     */
    function is_current_action($urlcontroller="",$urlaction=""){
        $urlcontroller = strtolower($urlcontroller);
        $urlaction = strtolower($urlaction);
        if( $this->request->params['controller'] == $urlcontroller && $this->request->params['action']==$urlaction ){
            return "class='active'";
        }else{
            return "";
        }
    }



}


?>
