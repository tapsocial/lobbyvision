<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Design $Design
 * @property Address $MailingAddress
 * @property Address $BillingAddress
 * @property Group $Group
 * @property SubscriptionPackage $SubscriptionPackage
 * @property Address $Address
 * @property Agent $Agent
 * @property Contact $Contact
 * @property Design $Design
 * @property Event $Event
 * @property Promotion $Promotion
 * @property TeamMember $TeamMember
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'username';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'username' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'unique' => array(
	        'rule' => 'isUnique',
	        'message' => 'Already taken.',
	        'required' => 'create'
		    ),
		    
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'unique' => array(
	        'rule' => 'isUnique',
	        'message' => 'Already registered.',
	        'required' => 'create'
		    ),
		),
		'password' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'group_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'Contact' => array(
			'className' => 'Contact',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Design' => array(
			'className' => 'Design',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'UserConnect' => array(
			'className' => 'UserConnect',
			'foreignKey' => 'user_id',
			'conditions' => array('User.group_id'=>2,'UserConnect.type'=>'twitter'),
			'fields' => '',
			'order' => ''
		),
		'FConnect' => array(
			'className' => 'UserConnect',
			'foreignKey' => 'user_id',
			'conditions' => array('User.group_id'=>2,'FConnect.type'=>'facebook'),
			'fields' => '',
			'order' => ''
		),
		'MailingAddress' => array(
			'className' => 'Address',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',//array('MailingAddress.*','Country.name'),
			'order' => ''
		),
		'BillingAddress' => array(
			'className' => 'Address',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SubscriptionPackage' => array(
			'className' => 'SubscriptionPackage',
			'foreignKey' => 'subscription_package_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(		
		'Event' => array(
			'className' => 'Event',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Promotion' => array(
			'className' => 'Promotion',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'TeamMember' => array(
			'className' => 'TeamMember',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	public $actsAs = array('Acl' => array('type' => 'requester'));
    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['group_id'])) {
            $groupId = $this->data['User']['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        if (!$groupId) {
            return null;
        } else {
            return array('Group' => array('id' => $groupId));
        }
    }

	public function beforeSave($options = array()) {
		if(isset($this->data['User']['password']))
        $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        return true;
    }

     // this is used by the auth component to turn the password into its hash before comparing with the DB
    function hashPasswords($data) {

         $data['User']['password'] = crypt($data['User']['password'], substr($data['User']['username'], 0, 2));
         return $data;
    }
	function paginateCount($conditions = null, $recursive = 0, $extra = array())
    {
         $count = $this->find('count', array(
         'fields' => 'DISTINCT User.id',
            'conditions' => $conditions
        ));
        return $count;
    }
	

}
