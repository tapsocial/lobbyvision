<?php
App::uses('BackgroundVideo', 'Model');

/**
 * BackgroundVideo Test Case
 *
 */
class BackgroundVideoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.background_video'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BackgroundVideo = ClassRegistry::init('BackgroundVideo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BackgroundVideo);

		parent::tearDown();
	}

}
