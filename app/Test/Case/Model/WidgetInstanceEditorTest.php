<?php
App::uses('WidgetInstanceEditor', 'Model');

/**
 * WidgetInstanceEditor Test Case
 *
 */
class WidgetInstanceEditorTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.widget_instance_editor',
		'app.widget_instance',
		'app.user',
		'app.group',
		'app.subscription_package',
		'app.contact',
		'app.design',
		'app.template',
		'app.user_connect',
		'app.hashtag',
		'app.address',
		'app.country',
		'app.state',
		'app.event',
		'app.event_repeat',
		'app.event_type',
		'app.promotion',
		'app.team_member',
		'app.widget_master'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WidgetInstanceEditor = ClassRegistry::init('WidgetInstanceEditor');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WidgetInstanceEditor);

		parent::tearDown();
	}

}
