<?php
App::uses('WidgetInstanceRestaurantCategory', 'Model');

/**
 * WidgetInstanceRestaurantCategory Test Case
 *
 */
class WidgetInstanceRestaurantCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.widget_instance_restaurant_category',
		'app.widget_instance',
		'app.user',
		'app.group',
		'app.subscription_package',
		'app.contact',
		'app.design',
		'app.template',
		'app.user_connect',
		'app.hashtag',
		'app.address',
		'app.country',
		'app.state',
		'app.event',
		'app.event_repeat',
		'app.event_type',
		'app.promotion',
		'app.team_member',
		'app.widget_master',
		'app.widget_instance_weather',
		'app.widget_instance_editor',
		'app.widget_instance_logo',
		'app.widget_instance_news',
		'app.widget_instance_slideshow',
		'app.widget_instance_team',
		'app.widget_instance_event',
		'app.widget_instance_promo',
		'app.widget_instance_clock',
		'app.widget_instance_ticker',
		'app.ticker_content',
		'app.widget_instance_restaurant',
		'app.restaurant_category',
		'app.restaurant_menu_item',
		'app.restaurant_menu_item_price',
		'app.restaurant_period',
		'app.restaurant_category_period'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WidgetInstanceRestaurantCategory = ClassRegistry::init('WidgetInstanceRestaurantCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WidgetInstanceRestaurantCategory);

		parent::tearDown();
	}

}
