<?php
App::uses('WidgetMaster', 'Model');

/**
 * WidgetMaster Test Case
 *
 */
class WidgetMasterTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.widget_master',
		'app.widget_instance'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WidgetMaster = ClassRegistry::init('WidgetMaster');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WidgetMaster);

		parent::tearDown();
	}

}
