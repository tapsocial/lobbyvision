<?php
App::uses('PaymentStatus', 'Model');

/**
 * PaymentStatus Test Case
 *
 */
class PaymentStatusTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.payment_status',
		'app.booking',
		'app.villa',
		'app.agent',
		'app.user',
		'app.group',
		'app.extra_service',
		'app.bookings_extra_service',
		'app.country',
		'app.state',
		'app.states',
		'app.villa_seasons_price',
		'app.book_status',
		'app.payment_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PaymentStatus = ClassRegistry::init('PaymentStatus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PaymentStatus);

		parent::tearDown();
	}

}
