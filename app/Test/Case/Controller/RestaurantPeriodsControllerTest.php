<?php
App::uses('RestaurantPeriodsController', 'Controller');

/**
 * RestaurantPeriodsController Test Case
 *
 */
class RestaurantPeriodsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.restaurant_period',
		'app.user',
		'app.group',
		'app.subscription_package',
		'app.contact',
		'app.design',
		'app.template',
		'app.user_connect',
		'app.hashtag',
		'app.address',
		'app.country',
		'app.state',
		'app.event',
		'app.event_repeat',
		'app.event_type',
		'app.promotion',
		'app.team_member',
		'app.widget_instance',
		'app.widget_master',
		'app.widget_instance_weather',
		'app.widget_instance_editor',
		'app.widget_instance_logo',
		'app.widget_instance_news',
		'app.widget_instance_slideshow',
		'app.widget_instance_team',
		'app.widget_instance_event',
		'app.widget_instance_promo',
		'app.widget_instance_clock',
		'app.widget_instance_ticker',
		'app.ticker_content',
		'app.restaurant_category',
		'app.restaurant_menu_item',
		'app.restaurant_menu_item_price',
		'app.restaurant_category_period'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
