<?php
/**
 * WidgetMasterFixture
 *
 */
class WidgetMasterFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'length' => 128, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'description' => array('type' => 'string', 'null' => false, 'length' => 256, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'height_min' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 3),
		'height_max' => array('type' => 'integer', 'null' => false, 'default' => '3', 'length' => 3),
		'height_def' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 3),
		'width_min' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 3),
		'width_max' => array('type' => 'integer', 'null' => false, 'default' => '4', 'length' => 3),
		'width_def' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 3),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet',
			'height_min' => 1,
			'height_max' => 1,
			'height_def' => 1,
			'width_min' => 1,
			'width_max' => 1,
			'width_def' => 1
		),
	);

}
