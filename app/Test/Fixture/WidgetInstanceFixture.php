<?php
/**
 * WidgetInstanceFixture
 *
 */
class WidgetInstanceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'length' => 128, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'widget_master_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'index'),
		'gridster_col' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 3),
		'gridster_row' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 3),
		'gridster_size_x' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 3),
		'gridster_size_y' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 3),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'user_id' => array('column' => 'user_id', 'unique' => 0),
			'widget_master_id' => array('column' => 'widget_master_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'user_id' => 1,
			'widget_master_id' => 1,
			'gridster_col' => 1,
			'gridster_row' => 1,
			'gridster_size_x' => 1,
			'gridster_size_y' => 1,
			'created' => '2016-02-04 09:41:25',
			'updated' => '2016-02-04 09:41:25'
		),
	);

}
