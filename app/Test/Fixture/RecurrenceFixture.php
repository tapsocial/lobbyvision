<?php
/**
 * RecurrenceFixture
 *
 */
class RecurrenceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'repeat_start' => array('type' => 'date', 'null' => false, 'default' => null),
		'repeat_interval' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'repeat_year' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'repeat_month' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'repeat_day' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'repeat_week' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'repeat_weekday' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'repeat_start' => '2016-03-30',
			'repeat_interval' => 'Lorem ipsum dolor sit amet',
			'repeat_year' => 'Lorem ipsum dolor sit amet',
			'repeat_month' => 'Lorem ipsum dolor sit amet',
			'repeat_day' => 'Lorem ipsum dolor sit amet',
			'repeat_week' => 'Lorem ipsum dolor sit amet',
			'repeat_weekday' => 'Lorem ipsum dolor sit amet'
		),
	);

}
